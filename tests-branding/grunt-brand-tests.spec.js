'use strict';
var path = require('path');
var should = require('should');
var expect = require('should');
var fs = require('fs');
var assert = require('assert');

describe('Addison Lee Brand Configuration', function() {

	it('should have application brand-configuration file defined', function() {
		expect(fs.existsSync(path.resolve('./config/config-brand.js')) === true);
	});

	it('should have application brand images directory defined', function() {
		expect(fs.existsSync(path.resolve('./config/branding/images')) === true);
	});

	it('should have brand images for addison-lee brand', function() {
		expect(fs.existsSync(path.resolve('./config/branding/images/addison-lee')) === true);
	});

	it('should have brand images for barclays brand', function() {
		expect(fs.existsSync(path.resolve('./config/branding/images/barclays')) === true);
	});

	it('should have application brand icon directory defined', function() {
		expect(fs.existsSync(path.resolve('./config/branding/icon')) === true);
	});

	it('should have brand icons for addison-lee brand', function() {
		expect(fs.existsSync(path.resolve('./config/branding/icon/addison-lee')) === true);
	});

	it('should have brand icons for barclays brand', function() {
		expect(fs.existsSync(path.resolve('./config/branding/icon/barclays')) === true);
	});

	it('should have application brand scss directory defined', function() {
		expect(fs.existsSync(path.resolve('./config/branding/scss')) === true);
	});

	it('should have brand scss for addison-lee brand', function() {
		expect(fs.existsSync(path.resolve('./config/branding/scss/addison-lee')) === true);
	});

	it('should have brand scss for barclays brand', function() {
		expect(fs.existsSync(path.resolve('./config/branding/scss/barclays')) === true);
	});

});

describe('Addison Lee Brand Tests', function() {

	it('should have brand logo image in assets/img', function() {
		expect(fs.existsSync(path.resolve('./assets/img/logo.svg')) === true);
	});

	it('should have brand favicon image in assets/icon', function() {
		expect(fs.existsSync(path.resolve('./assets/icon/favicon.ico')) === true);
	});

	it('should have brand scss in styles/brand-variables', function() {
		expect(fs.existsSync(path.resolve('./src/styles/_brand.scss')) === true);
	});

	it('should have colors scss in styles/brand-variables', function() {
		expect(fs.existsSync(path.resolve('./src/styles/_colors.scss')) === true);
	});

	it('should have fonts scss in styles/brand-variables', function() {
		expect(fs.existsSync(path.resolve('./src/styles/_fonts.scss')) === true);
	});

	it('should have forms scss in styles/brand-variables', function() {
		expect(fs.existsSync(path.resolve('./src/styles/_forms.scss')) === true);
	});

	it('should have global scss in styles/brand-variables', function() {
		expect(fs.existsSync(path.resolve('./src/styles/_global.scss')) === true);
	});

	it('should have buttons scss in styles/brand-variables', function() {
		expect(fs.existsSync(path.resolve('./src/styles/_buttons.scss')) === true);
	});

});
