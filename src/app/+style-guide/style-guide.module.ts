import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './style-guide.routes';
import { SharedModule } from '../+shared/shared.module';

import { StyleGuideComponent } from './style-guide.component';
import { CustomDatePickerComponent } from '../components/custom-date-picker/custom-date-picker.component';
import { SharedComponentsModule } from '../+shared-components/shared-components.module';

console.log('style guide bundle loaded asynchronously');

@NgModule({
	declarations: [
		// Components / Directives/ Pipes
		StyleGuideComponent,
		CustomDatePickerComponent
	],
	imports: [
		CommonModule,
		SharedModule,
		SharedComponentsModule,
		RouterModule.forChild(routes)
	],
})
export class StyleGuideModule {
	public static routes = routes;
}
