import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { CustomInputComponent } from '../components/custom-input/custom-input.component';

@Component({
	selector: 'style-guide',
 	templateUrl: './style-guide.component.html'
})
export class StyleGuideComponent implements OnInit {

	public options = ['Cash', 'Card'];
	@ViewChildren(CustomInputComponent) public allCustomInputs: QueryList<CustomInputComponent>;
    constructor() {
    	//
	}

    public ngOnInit() {
		// this.allCustomInputs.map((input) => {
		// 	console.log(input.model);
		// });
	}

	public clicking() {
		this.allCustomInputs.map((input) => {
			console.log(input.model);
		});
	}
}
