import { StyleGuideComponent } from './style-guide.component';

export const routes = [
	{ path: '', component: StyleGuideComponent }
];
