import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, RequestOptions, XHRBackend, Http } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
	NgModule,
	ApplicationRef, CUSTOM_ELEMENTS_SCHEMA
} from '@angular/core';

import {
	RouterModule,
	PreloadAllModules
} from '@angular/router';

/*
 * Platform and Environment providers/directives/pipes
 */
import { ROUTES } from './app.routes';

// App is our top level component
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login';
import { LoginFormComponent } from './components/login/login-form';
import { LoginLinksComponent } from './components/login/login-links';
import { NoContentComponent } from './components/no-content';
import { SharedModule } from './+shared';

import '../styles/styles.scss';
import { HttpService } from './services/http/http.service';
import { ServiceBooking } from './services/book/book.service';
import { LocalStorage } from './services/local-storage/local-storage.service';
import { PriceService } from './services/price/price.service';
import { AgmCoreModule, GoogleMapsAPIWrapper, MarkerManager } from '@agm/core';
import { CoreModule } from './+core/core.module';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import {
	CollapseModule, DatepickerModule, DropdownModule, ModalModule, PaginationModule,
	TypeaheadModule
} from 'ng2-bootstrap';
import { AddressService } from './services/address/address.service';
import { PersonalInfoService } from './services/profile/personal-info.service';
import { VerifyService } from './services/verify/verify.service';
import { CardsService } from './services/card/cards.service';
import { GeoService } from './services/global-geo/geo.service';
import { DragulaModule , DragulaService } from 'ng2-dragula/ng2-dragula';

export function createTranslateLoader(http: Http) {
	return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
	bootstrap: [ AppComponent ],
	declarations: [
		AppComponent,
		LoginComponent,
		LoginFormComponent,
		LoginLinksComponent,
		NoContentComponent
	],
	imports: [ // import Angular's modules
		BrowserModule,
		BrowserAnimationsModule,
		HttpModule,

		CoreModule,
		SharedModule,
		DragulaModule,
		CollapseModule.forRoot(),
		DatepickerModule.forRoot(),
		PaginationModule.forRoot(),
		TypeaheadModule.forRoot(),
		DropdownModule.forRoot(),
		ModalModule.forRoot(),

		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: (createTranslateLoader),
				deps: [Http]
			}
		}),

		AgmCoreModule.forRoot({
			apiKey: 'AIzaSyBos_5dnB8NDbbmiJkRin-bU1dirKzcMeU',
			libraries: ['places'],
		}),
		RouterModule.forRoot(ROUTES)
	],
	providers: [// expose our Services and Providers into Angular's dependency injection
		{
			provide: HttpService,
			useFactory: httpService,
			deps: [XHRBackend, RequestOptions]
		},
		LocalStorage,
		ServiceBooking,
		GoogleMapsAPIWrapper,
		TranslateService,
		PriceService,
		AddressService,
		MarkerManager,
		PersonalInfoService,
		VerifyService,
		CardsService,
		GeoService
	],
	schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
	constructor(
		public translate: TranslateService,
		public appRef: ApplicationRef,
	) {
		translate.setDefaultLang('en');
		translate.use('en');
	}
}

export function httpService (x: XHRBackend, o: RequestOptions) {
	return new HttpService(x, o);
}
