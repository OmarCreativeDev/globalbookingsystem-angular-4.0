import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './a-to-b.routes';
import { AtoBComponent } from './a-to-b.component';
import { SharedModule } from '../+shared/shared.module';
import { PassengerDetailsComponent } from './passenger-details/passenger-details.component';
import { SummaryWidgetComponent } from './summary-widget';
import { ServicesCatalogueComponent } from './service-catalogue/service-catalogue.component';
import { DirectionsMapDirective } from '../directives/directions.google-map.directive';
import { AgmCoreModule } from '@agm/core';
import { SharedComponentsModule } from '../+shared-components/shared-components.module';
import { BookingConfirmationComponent } from './booking-confirmation/booking-confirmation.component';
import { BookingConfirmationActionsComponent } from './booking-confirmation/booking-confirmation-buttons/booking-confirmation-buttons.component';
import { BookingConfirmationTitleComponent } from './booking-confirmation/booking-confirmation-title/booking-confirmation-title.component';
import { TranslateModule } from '@ngx-translate/core';
import { DragulaModule , DragulaService } from 'ng2-dragula/ng2-dragula';

@NgModule({
	declarations: [
		// Components / Directives/ Pipes
		AtoBComponent,
		ServicesCatalogueComponent,
		PassengerDetailsComponent,
		SummaryWidgetComponent,
		BookingConfirmationComponent,
		BookingConfirmationActionsComponent,
		BookingConfirmationTitleComponent,
		DirectionsMapDirective,

	],
	imports: [
		CommonModule,
		SharedModule, //
		SharedComponentsModule,
		AgmCoreModule,
		DragulaModule,
		TranslateModule,
		RouterModule.forChild(routes),
	],
	providers: [DragulaService]
})

export class AtoBModule {
	public static routes = routes;
}
