import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ServiceCatalogue } from '../../services/service-catalogue/service-catalogue.service';
import { ServiceCatalogueModel } from '../../models/service-catalogue/service-catalogue.model';
@Component({
	selector: 'global-services',
	templateUrl: './service-catalogue.component.html',
	styleUrls: ['./service-catalogue.component.scss'],
})
export class ServicesCatalogueComponent implements OnInit {
	public serviceCatalogList: ServiceCatalogue[];
	public selectedServiceCatalog = ServiceCatalogueModel;

	@Input() public index: any = 0;
	@Output() public onServiceSelect = new EventEmitter();

	constructor(
		public serviceCatalogue: ServiceCatalogue,
	) {}

	public ngOnInit() {
		this.serviceCatalogue.getServiceCatalogue()
			.subscribe((data) => {
				this.serviceCatalogList = data['records'];
				this.selectCatalogService(data['records'][0]['vehicles'][0]);
		});
	}

	public selectCatalogService(selected) {
		this.selectedServiceCatalog = selected;
		this.onServiceSelect.emit(this.selectedServiceCatalog);
	}
}
