import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable } from 'rxjs';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

// Load the implementations that should be tested
import { AtoBModule } from '../a-to-b.module';
import { ServicesCatalogueComponent } from './service-catalogue.component';
import { AppModule } from '../../app.module';

describe('A to B - ServicesCatalogueComponent', () => {
	let comp: ServicesCatalogueComponent;
	let fixture: ComponentFixture <ServicesCatalogueComponent>;

	// async beforeEach
	beforeEach(() => {
		localStorage.clear();

		TestBed.configureTestingModule({
			imports: [ AppModule, AtoBModule, NoopAnimationsModule, RouterTestingModule ],
			declarations: [ ],
			schemas: [NO_ERRORS_SCHEMA],
			providers: [ ]
		});
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(ServicesCatalogueComponent);
		comp = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should have services component correctly defined', () => {
		expect(comp).toBeDefined();
	});

	it('should have public attribute serviceCatalogList : array to store the service catalog data', () => {
		expect(comp.serviceCatalogList).not.toBeDefined();
	});

	it('should have public attribute selectedServiceCatalog : object to store the user selection', () => {
		expect(comp.selectedServiceCatalog).toBeDefined();
	});

	it('should have public @Output onServiceSelect : to notifiy when user select a service catalog', () => {
		expect(comp.onServiceSelect).toBeDefined();
	});

	it('should have public serviceCatalogue : to get service catalog on Init', () => {
		comp.ngOnInit();
		expect(comp.serviceCatalogue).not.toBe(null);
	});

	it('should have public selectCatalogService method : to handle when user select a catalog service', () => {
		expect(comp.selectCatalogService).not.toBe(null);
	});

	it('should have public ngOnInit method : to handle get catalog service on Init', () => {
		expect(comp.ngOnInit).toBeDefined();
	});

	it('should call serviceCatalogue and get catalog service on Init', () => {

		spyOn(comp.serviceCatalogue, 'getServiceCatalogue').and.returnValue( Observable.of({
			result: 'OK',
			records: [
				{
					id: 100,
					code: 'STD',
					name: 'Standard',
					description: 'Passenger car up to 4 Passengers',
					created_dt: '2017-01-01T12:00:00Z',
					modified_dt: '2017-01-01T12:30:00Z',
					status: 1,
					locales: [
						{
							locale: 'en-US',
							name: 'Sedan',
							description: 'Passenger car up to 4 Passengers'
						},
						{
							locale: 'en-GB',
							name: 'Standard',
							description: 'Passenger car up to 4 Passengers'
						},
						{
							locale: 'fr-fr',
							name: 'Standard',
							description: 'Passenger car up to 4 Passengers'
						}
					],
					vehicles: [
						{
							id: 1,
							supplier: 100,
							code: 'FORDS',
							name: 'Ford Standard',
							description: 'Ford Standard STD',
							make: 'Ford',
							model: 'Standard',
							passengers: 4,
							items: 2,
							image: 'service-standard.png',
							service_id: 100,
							status: 1,
							created_dt: '2017-01-01T12:00:00Z',
							modified_dt: '2017-01-01T12:30:00Z'
						}
					]
				}
			]
		}));
		comp.ngOnInit();
		fixture.detectChanges();
		fixture.whenStable().then(() => {
			expect(comp.serviceCatalogList).toBeDefined();
		});
	});

	it('should have public method selectCatalogService to emit event when user selects service', () => {

		let data = {
				car: 'Large Car',
				price: '£40.80',
				text: 'Passenger car up to 6 Passengers',
				imageCar: 'service-executive.png',
				imageCapacity: 'service-executive-capacity.png'
			};

		comp.selectCatalogService(data);
		expect(comp.selectedServiceCatalog['car']).toEqual(data.car);

		spyOn(comp.onServiceSelect, 'emit').and
			.returnValue( data );

		comp.selectCatalogService(data);
		expect(comp.onServiceSelect.emit).toHaveBeenCalledWith(data);

	});
});
