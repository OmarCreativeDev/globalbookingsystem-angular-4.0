import { browser, by, element } from 'protractor';

xdescribe('A to B - Catalogue Service Component', () => {
	it('Navigate to A to B Journey page', () => {
		browser.get('/');
		browser.sleep(1000);

		let dashboardLink = element(by.linkText('Dashboard'));
		dashboardLink.click();
		browser.sleep(1000);

		let aToBLink = element(by.linkText('BOOK A TO B'));
		aToBLink.click();
		browser.sleep(1000);
	});

	xit('should have 3 catalogue services available', () => {
		// keep waiting till
		let availableService = element.all(by.css('.service-wrapper'));

		browser.wait( () => {
			return browser.isElementPresent(availableService);
		}).then( () => {
			expect<any>(availableService.count()).toEqual(3);
		});
	});

	xit('should have Standard Car selected by default', () => {
		let selectedService = element.all(by.css('.service-wrapper h4'));
		expect<any>(selectedService.get(0).getText()).toEqual('Standard car');
		expect<any>(element(by.css('.active')).isPresent()).toBe(true);
	});

	xit('should have Executive Car selected and active class present', () => {
		let selectedService = element.all(by.css('.service-wrapper h4')).get(1);
		selectedService.click();
		expect<any>(selectedService.getText()).toEqual('Executive car');
		expect<any>(element(by.css('.active')).isPresent()).toBe(true);
	});

	xit('should have Large Car selected and active class present', () => {
		let selectedService = element.all(by.css('.service-wrapper h4')).get(2);
		selectedService.click();
		expect<any>(selectedService.getText()).toEqual('Large car');
		expect<any>(element(by.css('.active')).isPresent()).toBe(true);
	});

});
