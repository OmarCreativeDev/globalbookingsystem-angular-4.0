import { NO_ERRORS_SCHEMA, Component } from '@angular/core';
import { async, TestBed, ComponentFixture } from '@angular/core/testing';
import { AtoBModule } from '../a-to-b.module';
import { JourneyDetailsComponent } from './journey-details.component';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AppModule } from '../../app.module';
import { MOCK_CONSTANTS } from '../../../../config/mock-helpers/mock.constants';
import { ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { DashboardModule } from '../../+dashboard/dashboard.module';
describe('A to B - JourneyDetailsComponent', () => {
	let comp: JourneyDetailsComponent;
	let fixture: ComponentFixture <JourneyDetailsComponent>;
	afterEach(() => {
		TestBed.resetTestingModule();
	});
	// async beforeEach
	beforeEach(() => {
		localStorage.clear();
		TestBed.configureTestingModule({
			imports: [AppModule, AtoBModule, RouterTestingModule, BrowserAnimationsModule],
			declarations: [],
			schemas: [NO_ERRORS_SCHEMA],
			providers: []
		}).compileComponents(); // compile template and css
	});

	// synchronous beforeEach
	beforeEach(() => {
		fixture = TestBed.createComponent(JourneyDetailsComponent);
		comp = fixture.debugElement.componentInstance;
	});

	it('should have journey details component correctly defined', () => {
		expect(fixture).toBeDefined();
		expect(comp).toBeDefined();

	});

	it('should have isWidget correctly defined', () => {
		expect(comp.isWidget).toBeDefined();
		expect(comp.isWidget).toBeFalsy();
	});

	it('should have showStep correctly defined', () => {
		expect(comp.showStep).toBeDefined();
		expect(comp.showStep).toBeTruthy();
	});

	it('should have paymentMethod correctly defined', () => {
		expect(comp.atoBModel.payment).toBeDefined();
		expect(comp.atoBModel.payment).toEqual('Select');
	});

	it('should have dateOfJourney correctly defined', () => {
		expect(comp.atoBModel.journey).toBeDefined();
		expect(comp.atoBModel.journey['type']).toEqual('Select');
	});

	it('should add autocomplete listeners to pick-up and drop-off fields on Init', () => {
		spyOn(comp, 'prepareAutocompleteFields');
		comp.ngOnInit();
		expect(comp.prepareAutocompleteFields).toHaveBeenCalled();
	});

	it('when user changes journey type should emit event to update parent component', () => {
		let value = 'JOURNEY_DETAILS.DATE_OF_JOURNEY_OPTIONS.ASAP';
		comp.selectJourney(value);
		expect(comp.atoBModel.journey['type']).toEqual(value);
	});

	it('when user changes journey only allowed option is ASAP', () => {
		let value = 'JOURNEY_DETAILS.DATE_OF_JOURNEY_OPTIONS.LATER';
		comp.selectJourney(value);
		expect(comp.atoBModel.journey['type']).toEqual('Select');
	});

	it('when user changes payment type should emit event to update parent component', () => {
		let value = 'credit card';
		comp.selectPayment(value);
		expect(comp.atoBModel.payment).toBeDefined();
		expect(comp.atoBModel.payment).toEqual(value);
	});

	it('when user changes payment only allowed option is CASH', () => {
		let value = 'JOURNEY_DETAILS.PAYMENT_METHOD_OPTIONS.CARD';
		comp.selectPayment(value);
		expect(comp.atoBModel.payment).toBeDefined();
		expect(comp.atoBModel.payment).toEqual('Select');
	});

});

describe('A to B - JourneyDetailsComponent - Edit Booking', () => {

	let MockActivatedRoute = { params: Observable.of({bookingRef: '1234-test'}), snapshot: { url : [{path: 'something'}] } };

	beforeEach(async(() => {
		localStorage.clear();
		TestBed.configureTestingModule({
			imports: [
				AppModule,
				AtoBModule,
				RouterTestingModule.withRoutes([{ path: 'dashboard/a-to-b/booking-confirmation/1234-test/edit', component: JourneyDetailsComponent }]),
				BrowserAnimationsModule],
			declarations: [],
			schemas: [NO_ERRORS_SCHEMA],
			providers: [
				{ provide: ActivatedRoute, useValue: MockActivatedRoute },
			]
		}).compileComponents();
	}));

	it('should create journey details component', async(() => {
		const fixture = TestBed.createComponent(JourneyDetailsComponent);
		const comp = fixture.debugElement.componentInstance;
		expect(comp).toBeTruthy();

	}));

	xit('should handle if wrong or no data when try to populate the component fields',  async(() => {
		const fixture = TestBed.createComponent(JourneyDetailsComponent);
		fixture.detectChanges();
		const comp = fixture.debugElement.componentInstance;
		spyOn(comp, 'preFillBooking');
		fixture.whenStable().then(() => {
			expect(comp.preFillBooking).not.toHaveBeenCalled();
		});
	}));
	xit('should handle correct data when try to populate the component fields',  async(() => {
		const fixture = TestBed.createComponent(JourneyDetailsComponent);
		fixture.detectChanges();
		const comp = fixture.debugElement.componentInstance;
		let p = MOCK_CONSTANTS.BOOKING;
		comp.localStorage.set('1234-test', p);
		fixture.whenStable().then(() => {
			expect(comp.atoBModel).toBeDefined();
		});
	}));
});

describe('A to B - JourneyDetailsComponent - Get Quote From Widget', () => {

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				AppModule,
				AtoBModule,
				DashboardModule,
				RouterTestingModule.withRoutes([{ path: 'dashboard/a-to-b/quote', component: JourneyDetailsComponent }]),
				BrowserAnimationsModule],
			declarations: [],
			schemas: [NO_ERRORS_SCHEMA],
			providers: [
				// { provide: ActivatedRoute, useValue:  { snapshot: { url: [ { path: 'quote'} ] } } } ,
			]
		}).compileComponents(); // compile template and css
	}));

	it('should create journey details component', async(() => {
		const fixture = TestBed.createComponent(JourneyDetailsComponent);
		const comp = fixture.debugElement.componentInstance;
		expect(comp).toBeTruthy();

	}));

	it('should handle if wrong or no data when try to populate the component fields',  async(() => {
		const fixture = TestBed.createComponent(JourneyDetailsComponent);
		fixture.detectChanges();
		const comp = fixture.debugElement.componentInstance;
		spyOn(comp, 'preFillBooking');
		fixture.whenStable().then(() => {
			expect(comp.preFillBooking).not.toHaveBeenCalled();
		});
	}));
	it('should handle correct data when try to populate the component fields',  async(() => {
		const fixture = TestBed.createComponent(JourneyDetailsComponent);
		fixture.detectChanges();
		const comp = fixture.debugElement.componentInstance;
		let p = MOCK_CONSTANTS.BOOKING;
		comp.localStorage.set('fromWidget', p);
		fixture.whenStable().then(() => {
			expect(comp.atoBModel).toBeDefined();
		});
	}));

});
