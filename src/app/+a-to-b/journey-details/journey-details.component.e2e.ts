import { browser, by, element, protractor } from 'protractor';

xdescribe('A to B - JourneyDetailsComponent', () => {

	it('Navigate to A to B Journey page', () => {
		browser.driver.manage().window().setSize(1280, 1024);
		browser.get('/').then(() => {

			let dashboardLink = element(by.linkText('Dashboard'));
			dashboardLink.click();
			browser.sleep(1000);
			let aToBLink = element(by.linkText('BOOK A TO B'));
			aToBLink.click();
			browser.sleep(1000);
		});

		let pageHeader = element.all(by.css('h2')).get(0).getText();
		expect<any>(pageHeader).toEqual('A to B Journey');
	});

	function testGooglePlacesOnInput(inputSelector, googlePlacesSelector) {
		let input = element(by.css(inputSelector));
		let addressEntry = 'London E11, UK';
		let pageHeader = element.all(by.css('h2')).get(0);

		// send address and trigger google auto complete
		input.sendKeys(addressEntry);
		browser.sleep(2000);
		input.sendKeys(protractor.Key.ARROW_RIGHT);

		// wait for google places ui
		browser.sleep(2000).then(() => {
			let	googlePlaces = element.all(by.css(googlePlacesSelector));
			expect<any>(googlePlaces.count()).toBeGreaterThan(0);
		});
		// trigger blur event on input field in order to close google places ui
		pageHeader.click();
	}

	xit('Entering an address in the pickup field triggers google places with 5 results', () => {
		testGooglePlacesOnInput('.pickup', '.pac-container .pac-item');
	});

	xit('Entering an address in the dropoff field triggers google places with 5 results', () => {
		testGooglePlacesOnInput('.dropoff', '.pac-container:last-child .pac-item');
	});

	function testDropdownSelection(dropdownSelector, dropdownOption) {
		let dropdownBtn = element(by.css(dropdownSelector));
		let	dropdownList = element(by.cssContainingText('ul li', dropdownOption));

		dropdownBtn.click();
		dropdownList.click();

		expect<any>(dropdownBtn.getText()).toEqual(dropdownOption);
	}

	xit('Set the date of journey option in the dropdown', () => {
		testDropdownSelection('#date-of-journey-dropdown', 'Asap');
	});

	xit('Set the payment method option in the dropdown', () => {
		testDropdownSelection('#payment-method-dropdown', 'Cash');
	});
});
