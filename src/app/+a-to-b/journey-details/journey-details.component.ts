import {
	Component,
	OnInit,
	ViewChild,
	Output,
	EventEmitter,
	Input
} from '@angular/core';
import { GLOBAL_CONSTANTS } from '../../common/global.constants';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalStorage } from '../../services/local-storage/local-storage.service';
import { AtoBModel } from '../../models/a-to-b/a-to-b.model';
import { CustomInputAutocompleteComponent } from '../../components/custom-input-autocomplete/custom-input-autocomplete.component';
import { CustomDropDownComponent } from '../../components/custom-dropdown/custom-dropdown.component';
import { CustomInputBtnComponent } from '../../components/custom-input-btn/custom-input-btn.component';
import * as _ from 'lodash';
declare var google: any;

@Component({
	selector: 'journey-details',
	templateUrl: './journey-details.component.html',
	styleUrls: ['./journey-details.component.scss']
})

export class JourneyDetailsComponent implements OnInit {

	@Input() public isWidget: boolean = false;
	@Input() public showStep: boolean = true;
	@Input() public title: string;
	@Input() public waypoints: Object[];
	// emit changes to AtoB component
	@Output() public pickupEvent = new EventEmitter();
	@Output() public dropOffEvent = new EventEmitter();
	@Output() public selectJourneyType = new EventEmitter();
	@Output() public selectPaymentType = new EventEmitter();

	// waypoints
	@Output() public onResetWaypoints = new EventEmitter();
	@Output() public onChangeWaypoint = new EventEmitter();
	@Output() public onRemoveWaypoint = new EventEmitter();
	@Output() public onDragAndDrop = new EventEmitter();
	@Output() public preLoadingFinished = new EventEmitter();

	// autocomplete
	@ViewChild(CustomInputAutocompleteComponent) public pickUpAutocomplete: CustomInputAutocompleteComponent;
	@ViewChild(CustomInputBtnComponent) public dropOffAutocomplete: CustomInputAutocompleteComponent;

	// date of journey and payment method dropdowns
	@ViewChild('dateOfJourney') public dateOfJourneyEl: CustomDropDownComponent;
	@ViewChild('paymentMethod') public paymentMethodEl: CustomDropDownComponent;

	public hasLocalStorage: any;
	public defaultDropdownValue: string = GLOBAL_CONSTANTS.DEFAULT_SELECT_OPTION;
	public paymentOptions: string[] = GLOBAL_CONSTANTS.PAYMENT_TYPE_OPTIONS;
	// journey options
	public dateOfJourneyOptions: string[] = GLOBAL_CONSTANTS.JOURNEY_TYPE_OPTIONS;
	// disabled options
	public disabledOptions: string[] = GLOBAL_CONSTANTS.DISABLED_OPTIONS;

	public origin: any;
	public destination: any;
	public asap: any;

	public atoBModel: AtoBModel = new AtoBModel();

	constructor(
		private route: ActivatedRoute,
		public router: Router,
		public localStorage: LocalStorage) {
		this.atoBModel.journey = {
			type: this.defaultDropdownValue,
			time: ''
		};
		this.atoBModel['payment'] = this.defaultDropdownValue;
	}

	public ngOnInit() {
		this.prepareAutocompleteFields();
	}

	public prepareAutocompleteFields() {
		this.route.params.subscribe((value) => {
			let ref = value['bookingRef'];
			if (ref !== undefined) {
				let booking = this.localStorage.get(ref);
				this.preFillBooking(booking);
			}
		});
		this.hasLocalStorage = this.localStorage.get('fromWidget') ? true : false;
		if (this.route.snapshot.url.length > 0) {
			if (this.hasLocalStorage && !this.isWidget && this.route.snapshot.url[0].path === 'quote') {
				let fromWidget = this.localStorage.get('fromWidget');
				this.preFillBooking(fromWidget);
			}
		}
	}

	public preFillBooking(storedData) {
		this.atoBModel = storedData;
		this.pickUpAutocomplete.asyncData = this.atoBModel.origin;
		this.pickupEvent.emit(this.atoBModel.origin);
		this.dropOffAutocomplete.asyncData = this.atoBModel.destination;
		this.dropOffEvent.emit(this.atoBModel.destination);
		this.selectJourney(storedData['journey']['type']);
	};

	public updatePickup(data) {
		this.pickupEvent.emit(data);
	}
	public updateDropOff(data) {
		console.log(data);
		this.dropOffEvent.emit(data);
	}

	public removeWaypoint(data) {
		this.onRemoveWaypoint.emit(data);
	}

	public resetWaypoints(waypoints) {
		this.onResetWaypoints.emit(waypoints);
	}

	public reorderWaypoints(orderedWaypoints) {
		this.onDragAndDrop.emit(orderedWaypoints);
	}

	public selectJourney(value) {
		if (value === 'JOURNEY_DETAILS.DATE_OF_JOURNEY_OPTIONS.LATER') {
			return;
		}
		if (value === 'JOURNEY_DETAILS.DATE_OF_JOURNEY_OPTIONS.ASAP') {
			this.asap = new Date();
			this.asap.setMinutes(this.asap.getMinutes() + 20);
			this.atoBModel.journey['type'] = value;
			this.dateOfJourneyEl.updateOption(value);
			this.atoBModel.journey['time'] = this.asap;
			this.selectJourneyType.emit(this.atoBModel.journey);
		}
	}

	public selectPayment(value) {
		if (value === 'JOURNEY_DETAILS.PAYMENT_METHOD_OPTIONS.CARD') {
			return;
		}
		this.atoBModel.payment = value;
		this.paymentMethodEl.updateOption(value);
		this.selectPaymentType.emit(value);
		this.preLoadingFinished.emit();
	}

	public navigateGetQuote() {
		this.localStorage.set('fromWidget', this.atoBModel);
		this.router.navigate(['dashboard/a-to-b/quote']);
	}
}
