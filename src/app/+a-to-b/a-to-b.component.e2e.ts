import { browser, by, element } from 'protractor';

describe('A to B - AtoBComponent', () => {

	it('Navigate to A to B Journey page', () => {
		browser.driver.manage().window().setSize(1280, 1024);
		browser.get('/');
		browser.sleep(1000);

		let dashboardLink = element(by.linkText('Dashboard'));
		dashboardLink.click();
		browser.sleep(1000);

		let aToBLink = element(by.linkText('BOOK A TO B'));
		aToBLink.click();
		browser.sleep(1000);

		let pageHeader = element.all(by.css('h2')).get(0).getText();
		expect<any>(pageHeader).toEqual('A to B Journey');
	});

	it('should have 3 catalogue services available', () => {
		let carService = element.all(by.css('.service-wrapper'));

		browser.wait( () => {
			return browser.isElementPresent(carService.get(0));
		}).then( () => {
			expect<any>(carService.count()).toEqual(3);
		});
	});

	xit('should have Standard Car selected by default', () => {
		let carServiceTitle = element.all(by.css('.service-wrapper h4')).get(0);
		let carService = element.all(by.css('.service-wrapper')).get(0);

		expect<any>(carServiceTitle.getText()).toEqual('Standard car service');
		expect<any>(carService.getAttribute('class')).toMatch('active');
	});

	xit('should have Executive Car selected and active class present', () => {
		let carServiceTitle = element.all(by.css('.service-wrapper h4')).get(1);
		let carService = element.all(by.css('.service-wrapper')).get(1);
		carServiceTitle.click();

		expect<any>(carServiceTitle.getText()).toEqual('Large car service');
		expect<any>(carService.getAttribute('class')).toMatch('active');
	});

	xit('should have Executive Car selected and active class present', () => {
		let carServiceTitle = element.all(by.css('.service-wrapper h4')).get(2);
		let carService = element.all(by.css('.service-wrapper')).get(2);
		carServiceTitle.click();

		expect<any>(carServiceTitle.getText()).toEqual('Executive car service');
		expect<any>(carService.getAttribute('class')).toMatch('active');
	});

	xit('The side widget should not be visible when the page loads', () => {
		//
	});

	xit('The origin and destination fields are not informed when page loads', () => {
		//
	});

	xit('The default service catalog should be selected when page loads', () => {
		//
	});

	xit('The side widget should be visible if origin field is informed', () => {
		//
	});

	xit('The side widget should be visible when origin and destination fields are informed', () => {
		//
	});

	xit('The side widget-header should be only visible if origin, destination and journey-type fields are informed', () => {
		//
	});

	xit('The book button should be only enabled if origin, destination, service, passenger, journey and payment fields are informed', () => {
		//
	});

	xit('It should make a booking and redirect to booking-confirmation page when user click Book Now button', () => {
		//
	});
});
