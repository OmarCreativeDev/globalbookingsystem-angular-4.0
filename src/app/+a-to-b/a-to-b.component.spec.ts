import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable } from 'rxjs';
import { MOCK_CONSTANTS } from '../../../config/mock-helpers/mock.constants';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

// Load the implementations that should be tested
import { AtoBComponent } from './a-to-b.component';
import { AtoBModule } from './a-to-b.module';
import { PriceResponseModel } from '../models/price/price.response.model';
import { ServiceCatalogueModel } from '../models/service-catalogue/service-catalogue.model';
import { AppModule } from '../app.module';

describe('Components - AtoBComponent', () => {
	let comp: AtoBComponent;
	let fixture: ComponentFixture <AtoBComponent>;

	// async beforeEach
	beforeEach(() => {
		localStorage.clear();

		TestBed.configureTestingModule({
			imports: [ AppModule, AtoBModule, NoopAnimationsModule, RouterTestingModule ],
			declarations: [ ],
			schemas: [NO_ERRORS_SCHEMA],
			providers: [ ]
		});
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(AtoBComponent);
		comp = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should have A to B component correctly defined', () => {
		expect(comp).toBeDefined();
	});

	it('should have updatePickUp method correctly defined', () => {
		expect(comp.updatePickUp).toBeTruthy();
	});

	it('should have updateDropOff method correctly defined', () => {
		expect(comp.updateDropOff).toBeTruthy();
	});

	it('should have updateJourneyType method correctly defined', () => {
		expect(comp.updateJourneyType).toBeTruthy();
	});

	it('should have updatePaymentType method correctly defined', () => {
		expect(comp.updatePaymentType).toBeTruthy();
	});

	it('should have updateEstimatedTime method correctly defined', () => {
		expect(comp.updateEstimatedTime).toBeTruthy();
	});

	it('should have selectedService method correctly defined', () => {
		expect(comp.updateSelectedService).toBeTruthy();
	});

	it('should have selectedPassenger method correctly defined', () => {
		expect(comp.updateSelectedPassenger).toBeTruthy();
	});

	it('should have shouldGetQoute method correctly defined', () => {
		expect(comp.shouldGetQoute).toBeTruthy();
	});

	it('should have shouldDisplayWidget method correctly defined', () => {
		expect(comp.shouldDisplayWidget).toBeTruthy();
	});

	it('should have getQuote method defined', () => {
		expect(comp.getQuote).toBeTruthy();
	});

	it('should have preparePriceRequest method correctly defined', () => {
		expect(comp.preparePriceRequest).toBeTruthy();
	});

	it('should have bookingConfirmation method defined', () => {
		expect(comp.bookingConfirmation).toBeTruthy();
	});

	xit('when user updates passenger details should update side Widget with the correct data', () => {
		let passenger = {
			namePassenger: 'Joe Bloggs',
			emailPassenger: 'a@a.com'
		};
		comp.updateSelectedPassenger(passenger);
		fixture.detectChanges();
		expect(comp.atoBModel.passenger.namePassenger).toEqual(passenger.namePassenger);
		expect(comp.summaryWidget.atoBModel.passenger.namePassenger).toEqual(passenger.namePassenger);
	});

	xit('when user update drop off point should update side Widget with the correct data', () => {
		let place = MOCK_CONSTANTS.PLACE;
		spyOn(comp.summaryWidget, 'updateDropOff');
		spyOn(comp, 'shouldGetQoute');
		comp.updateDropOff(place);
		fixture.detectChanges();
		expect(comp.atoBModel.destination).toEqual(place);
		expect(comp.summaryWidget.updateDropOff).toHaveBeenCalled();
		expect(comp.shouldGetQoute).toHaveBeenCalled();

	});

	xit('when user update pick up point should update side Widget with the correct data', () => {
		let place = MOCK_CONSTANTS.PLACE;
		spyOn(comp.summaryWidget, 'updatePickUp');
		spyOn(comp, 'shouldDisplayWidget');
		spyOn(comp, 'shouldGetQoute');
		comp.updatePickUp(place);
		fixture.detectChanges();
		expect(comp.atoBModel.origin).toEqual(place);
		expect(comp.summaryWidget.updatePickUp).toHaveBeenCalled();
		expect(comp.shouldDisplayWidget).toHaveBeenCalled();
		expect(comp.shouldGetQoute).toHaveBeenCalled();
	});

	xit('when user update journey type should update side Widget with the correct data', () => {
		let journey = MOCK_CONSTANTS.JOURNEY;
		spyOn(comp.summaryWidget, 'updateJourneyType');
		spyOn(comp, 'shouldGetQoute');
		comp.updateJourneyType(journey);
		fixture.detectChanges();
		expect(comp.atoBModel.journey).toEqual(journey);
		expect(comp.summaryWidget.updateJourneyType).toHaveBeenCalled();
		expect(comp.shouldGetQoute).toHaveBeenCalled();
	});

	xit('when user update payment type should update side Widget with the correct data', () => {
		let payment = MOCK_CONSTANTS.PAYMENT;
		spyOn(comp.summaryWidget, 'updatePaymentType');
		spyOn(comp, 'shouldGetQoute');
		comp.updatePaymentType(payment);
		fixture.detectChanges();
		expect(comp.atoBModel.payment).toEqual(payment);
		expect(comp.summaryWidget.updatePaymentType).toHaveBeenCalled();
		expect(comp.shouldGetQoute).toHaveBeenCalled();
	});

	xit('when user update service should update side Widget with the correct data', () => {
		//
	});

	xit('should have method to get quote and  should update side Widget with the correct data', () => {
		spyOn(comp.summaryWidget, 'updatePrice');
		spyOn(comp, 'preparePriceRequest');
		spyOn(comp.priceService, 'getPriceAtoB')
			.and
			.returnValue( Observable.of( PriceResponseModel ) );

		comp.getQuote();
		fixture.detectChanges();
		expect(comp.summaryWidget.updatePrice).toHaveBeenCalled();

	});

	xit('should have method to prepare price request object', () => {
		comp.atoBModel.origin = MOCK_CONSTANTS.PLACE1;
		comp.atoBModel.destination = MOCK_CONSTANTS.PLACE1;
		comp.atoBModel.service = new ServiceCatalogueModel(MOCK_CONSTANTS.CATALOGUE_SERVICE);
		comp.atoBModel.journey = 'JOURNEY_DETAILS.DATE_OF_JOURNEY_OPTIONS.ASAP';
		let requestModel = comp.preparePriceRequest();
		fixture.detectChanges();
		expect(requestModel.locations.length === 2).toBeTruthy();

	});

	xit('should have method to update the estimated journey time', () => {
		let time = MOCK_CONSTANTS.ESTIMATED_TIME.journeyTime;
		comp.updateEstimatedTime(time);
		fixture.detectChanges();
		expect(comp.atoBModel.estimatedTime).toEqual(time);
	});

});
