import { Component, Input, OnInit } from '@angular/core';

@Component({
	selector: 'booking-confirmation-title',
	templateUrl: 'booking-confirmation-title.component.html'
})
export class BookingConfirmationTitleComponent implements OnInit {

	@Input() public classToApply: string = '';
	public cl: string = '';
	constructor() {
		//
	}

	public ngOnInit() {
		//
	}

}
