import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, TestBed, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AtoBModule } from '../a-to-b.module';
import { MOCK_CONSTANTS } from '../../../../config/mock-helpers/mock.constants';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
// Load the implementations that should be tested
import { SummaryWidgetComponent } from './summary-widget.component';
import { BookingConfirmationComponent } from './booking-confirmation.component';
import { AppModule } from '../../app.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('A to B - BookingConfirmationComponent', () => {
	let comp: BookingConfirmationComponent;
	let fixture: ComponentFixture <BookingConfirmationComponent>;
	afterEach(() => {
		// TestBed.resetTestingModule();
	});

	// async beforeEach
	beforeEach(async(() => {
		localStorage.clear();
		TestBed.configureTestingModule({
			imports: [AppModule, AtoBModule, BrowserAnimationsModule, RouterTestingModule.withRoutes([
				{ path: 'dashboard/a-to-b/booking-confirmation/1234-test', component: BookingConfirmationComponent }
			]) ],
			declarations: [],
			schemas: [NO_ERRORS_SCHEMA],
			providers: [
				{ provide: ActivatedRoute, useValue: {params: Observable.of({bookingRef: '1234-test'})} }
			]
		})
			.compileComponents(); // compile template and css
	}));

	// synchronous beforeEach
	beforeEach(() => {
		fixture = TestBed.createComponent(BookingConfirmationComponent);
		comp = fixture.debugElement.componentInstance;
	});

	it('should have booking confirmation component correctly defined ', () => {
		expect(comp).toBeTruthy();
	});

	it('should have service MapsAPILoader correctly defined', () => {
		expect(comp.mapsAPILoader).toBeTruthy();
	});

	it('should have service NgZone correctly defined', () => {
		expect(comp.ngZone).toBeTruthy();
	});

	it('should have service ActivatedRoute correctly defined', () => {
		expect(comp.route).toBeTruthy();
	});

	it('should have service LocalStorage correctly defined', () => {
		expect(comp.localStorage).toBeTruthy();
	});

	it('should have @Input icon correctly defined', () => {
		expect(comp.icon).toBe('');
	});

	it('should have @Input title correctly defined', () => {
		expect(comp.title).toBe('');
	});

	it('should have @ViewChild DirectionsMapDirective correctly defined', () => {
		expect(comp.vc).toBeTruthy();
	});

	it('should have @ViewChild DropOffComponent correctly defined', () => {
		expect(comp.dropOffComponent).not.toBeDefined();
	});

	it('should have latitude property correctly defined', () => {
		expect(comp.latitude).toBeDefined();
	});

	it('should have longitude property correctly defined', () => {
		expect(comp.longitude).toBeDefined();
	});

	it('should have zoom property correctly defined', () => {
		expect(comp.zoom).toBeDefined();
	});

	it('should have bookingData property correctly defined', () => {
		expect(comp.bookingData).toBeDefined();
	});

	xit('should retrieve booking data on init', () => {
		spyOn(comp, 'getBookingData');
		// comp.ngOnInit();
		fixture.whenStable().then(() => {
			fixture.autoDetectChanges();
			expect(comp.getBookingData).toHaveBeenCalled();
		});
	});

	xit('should retrieve booking data on init and set the model', () => {
		comp.localStorage.set('1234-test', MOCK_CONSTANTS.BOOKING);
		fixture.whenStable().then(() => {
			fixture.autoDetectChanges();
			expect(comp.bookingData).toBeDefined();
		});
	});

});
