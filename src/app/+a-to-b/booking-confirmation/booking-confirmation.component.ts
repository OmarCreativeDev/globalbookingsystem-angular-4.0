import { Component, Input, ElementRef, OnInit, NgZone, ViewChild, QueryList, ViewChildren } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorage } from '../../services/local-storage/local-storage.service';
import { MapsAPILoader } from '@agm/core';
import { DirectionsMapDirective } from '../../directives/directions.google-map.directive';
import { BookResponseModel } from '../../models/book/book.response.model';
import { DropOffComponent } from './drop-offs/drop-off.component';

declare var google: any;

@Component({
	selector: 'booking-confirmation',
	templateUrl: './booking-confirmation.component.html',
	styleUrls: ['./booking-confirmation.component.scss']

})

export class BookingConfirmationComponent implements OnInit {

	@Input() public icon: string = '';
	@Input() public title: string = '';
	@ViewChild(DirectionsMapDirective) public vc: DirectionsMapDirective;
	@ViewChildren(DropOffComponent) public dropOffComponent: QueryList<DropOffComponent>;

	public latitude: number = 57.751076;
	public longitude: number = 11.271973;
	public zoom: number = 12;
	public bookingData: BookResponseModel = new BookResponseModel();

	// pick up
	public origin: any;
	// drop off
	public destination: any;

	constructor(
		public mapsAPILoader: MapsAPILoader,
		public ngZone: NgZone,
		public route: ActivatedRoute,
		public localStorage: LocalStorage
	) { //

	}

	public ngOnInit() {
		this.getBookingData();
	}

	public getBookingData() {

		this.route.params.subscribe((value) => {
			let ref = value['bookingRef'];
			if (ref !== undefined) {
				let data = this.localStorage.get(ref);
				this.bookingData = data;
				this.origin = this.bookingData.origin;
				this.destination = this.bookingData.destination;
				this.updateMap();
			}
		});
	}

	public updateMap() {
		this.ngZone.run(() => {
			this.vc.origin = {
				longitude: this.bookingData.origin['geo'].lng,
				latitude: this.bookingData.origin['geo'].lat
			};
			this.vc.originPlaceId = this.bookingData.origin['place_id'];
			this.vc.destination = {
				longitude: this.bookingData.destination['geo'].lng,
				latitude: this.bookingData.destination['geo'].lat
			};
			this.vc.destinationPlaceId = this.bookingData.destination['place_id'];
			this.vc.waypoints = this.bookingData.waypoints;
			this.mapsAPILoader.load()
				.then(() => {
					this.vc.directionsDisplay = new google.maps.DirectionsRenderer();
				});
			this.vc.updateDirections();
		});
	}
}
