import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, TestBed, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AtoBModule } from '../../a-to-b.module';

// Load the implementations that should be tested
import { AppModule } from '../../../app.module';
import { DropOffComponent } from './drop-off.component';

describe('A to B - BookingConfirmationComponent - DropOffComponent', () => {
	let comp: DropOffComponent;
	let fixture: ComponentFixture <DropOffComponent>;
	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [AppModule, AtoBModule, RouterTestingModule],
			declarations: [],
			schemas: [NO_ERRORS_SCHEMA],
		}).compileComponents(); // compile template and css
	}));
	beforeEach(() => {
		fixture = TestBed.createComponent(DropOffComponent);
		comp = fixture.componentInstance;
		fixture.detectChanges();
	});
	it('should create the component correctly', () => {
		expect(comp).toBeTruthy();
	});
	it(`should have as title 'Drop off 4'`, () => {
		comp.index = 2;
		comp.ngOnInit();
		expect(comp.title).toEqual('Drop off 4');
	});
});
