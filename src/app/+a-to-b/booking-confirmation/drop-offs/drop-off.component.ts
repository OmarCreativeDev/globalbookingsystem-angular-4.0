
import { Component, Input, OnInit } from '@angular/core';

@Component({
	selector: 'drop-off',
	templateUrl: 'drop-off.component.html'
})
export class DropOffComponent implements OnInit {

	@Input() public model: Object = {};
	@Input() public index: number = 0;
	public title: any = '';

	constructor() {
		//
	}

	public ngOnInit() {
		let _index = this.index + 2;
		this.title = 'Drop off ' + _index;
		//
	}

}
