/**
 * Created by arfs on 26/04/2017.
 */
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'booking-confirmation-buttons',
    templateUrl: 'booking-confirmation-buttons.component.html'
})
export class BookingConfirmationActionsComponent implements OnInit {

	@Input() public bookingReference: string;
	constructor(public router: Router) {
    	//
	}

    public ngOnInit() {
		//
	}
	public cancelBooking() {
		console.log(' cancel booking ref :' + this.bookingReference);
	}

}
