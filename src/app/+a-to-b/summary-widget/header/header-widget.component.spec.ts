import { async, TestBed, ComponentFixture } from '@angular/core/testing';
import { AtoBModule } from '../../a-to-b.module';

// Load the implementations that should be tested
import { AppModule } from '../../../app.module';
import { HeaderWidgetComponent } from './header-widget.component';
import { APP_BASE_HREF } from '@angular/common';

describe('A to B - SummaryWidgetComponent - HeaderWidgetComponent', () => {
	let component: HeaderWidgetComponent;
	let fixture: ComponentFixture<HeaderWidgetComponent>;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [AppModule, AtoBModule],
			// declarations: [ HeaderWidgetComponent ],
			providers: [
				{ provide: APP_BASE_HREF, useValue: '/' },
			]
		});
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(HeaderWidgetComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should have @Input model correctly defined', () => {
		expect(component.model).toBeFalsy();
	});

	it('should have @Input step correctly defined', () => {
		expect(component.estimatedTime).toBe('');
	});

});
