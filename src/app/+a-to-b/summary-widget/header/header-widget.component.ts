
import { Component, Input, OnInit } from '@angular/core';
import { AtoBModel } from '../../../models/a-to-b/a-to-b.model';

@Component({

    selector: 'header-widget',
    templateUrl: 'header-widget.component.html',
	styleUrls: ['./header-widget.component.scss']
})
export class HeaderWidgetComponent implements OnInit {
	@Input() public model: AtoBModel;
	@Input() public estimatedTime: string = '';
    constructor() {
    	//
	}

    public ngOnInit() {
    	//
	}

}
