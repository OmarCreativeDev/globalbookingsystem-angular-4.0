import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, TestBed, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AtoBModule } from '../a-to-b.module';
import { MOCK_CONSTANTS } from '../../../../config/mock-helpers/mock.constants';
import { ActivatedRoute } from '@angular/router';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';

import { Observable } from 'rxjs/Observable';

// Load the implementations that should be tested
import { SummaryWidgetComponent } from './summary-widget.component';
import { LocalStorage } from '../../services/local-storage/local-storage.service';
import { MapsAPILoader } from '@agm/core';
import { AppModule } from '../../app.module';

describe('A to B - SummaryWidgetComponent', () => {
	let comp: SummaryWidgetComponent;
	let fixture: ComponentFixture <SummaryWidgetComponent>;
	afterEach(() => {
		 // TestBed.resetTestingModule();
	});

	// async beforeEach
	beforeEach(async(() => {
		// window['google'] = google;
		localStorage.clear();
		TestBed.configureTestingModule({
			imports: [AppModule, AtoBModule, RouterTestingModule.withRoutes([
				{ path: '', component: SummaryWidgetComponent }
			]) ],
			declarations: [],
			schemas: [NO_ERRORS_SCHEMA],
			providers: []
		})
		.compileComponents().then(() => {
			fixture = TestBed.createComponent(SummaryWidgetComponent);
			comp = fixture.debugElement.componentInstance;
			fixture.detectChanges();
		});
	}));
	it('should have SummaryWidgetComponent correctly defined', async(() => {
		fixture.detectChanges();
		fixture.whenStable().then(() => { // wait for async getQuote
			fixture.detectChanges();        // update view with quote
			expect(comp).toBeTruthy();
		});
	}));

	it('should have isSticky defined and set with correct value', () => {
		expect(comp.isSticky).toBeDefined();
		expect(comp.isSticky).toBe(false);
	});

	it('should have loading defined and set with correct value', () => {
		expect(comp.loading).toBeDefined();
		expect(comp.loading).toBe(true);
	});

	it('should have current location coordinates defined', () => {
		expect(comp.latitude).toBe(57.751076);
		expect(comp.longitude).toBe(11.271973);
	});

	it('should have a valid current location latitude coordinate', () => {
		expect(comp.latitude).toBeGreaterThan(-90);
		expect(comp.latitude).toBeLessThan(90);
	});

	it('should have a valid current location longitude coordinate', () => {
		expect(comp.longitude).toBeGreaterThan(-180);
		expect(comp.longitude).toBeLessThan(180);
	});

	it('should have a valid zoom level set', () => {
		expect(comp.zoom).toBeGreaterThan(0);
		expect(comp.zoom).toBeLessThan(51);
	});

	it('updateJourneyType() method updates the journey data', () => {
		comp.updateJourneyType({type: 'Asap'});
		expect(comp.atoBModel.journey['type']).toBe('Asap');
	});

	it('updatePaymentType() method updates the payment type', () => {
		comp.updatePaymentType('Cash');
		expect(comp.atoBModel.payment).toBe('Cash');
	});

	it('updatePrice() method updates the price', () => {
		comp.updatePrice(MOCK_CONSTANTS.BOOKING.price);
		expect(comp.atoBModel.price).not.toBe(null);
	});

	it('updatePassenger() method updates the price', () => {
		comp.updatePassenger(MOCK_CONSTANTS.BOOKING.passenger);
		expect(comp.atoBModel.passenger).not.toBe(null);
	});

	it('hideWidget() method hides the summary widget', () => {
		comp.hideWidget();
		expect(comp.el.nativeElement.style.display).toBe('none');
	});

	it('showWidget() method shows the summary widget', () => {
		comp.showWidget();
		expect(comp.el.nativeElement.style.display).toBe('block');
	});
	xit('should update drop off information and update the map', () => {
		let place = MOCK_CONSTANTS.PLACE;
		spyOn(comp, 'updateMap');
		comp.updateDropOff(place);
		expect(comp.atoBModel.destination).toEqual(place);
		expect(comp.updateMap).toHaveBeenCalled();

	});

	it('should handle update drop off when no data', () => {
		comp.updateDropOff();
		expect(comp.vc.destination).toEqual(null);
	});

	it('should update and set the duration time', () => {
		let duration = '1 min';
		comp.updateEstimatedTime(duration);
		expect(comp.duration).toEqual(duration);

	});

	xit('should update pick up information and update markers on the map', () => {
		let place = MOCK_CONSTANTS.PLACE1;
		spyOn(comp.vc, 'updateMarkers');
		comp.updatePickUp(place);
		expect(comp.atoBModel.origin).toEqual(place);
		expect(comp.vc.updateMarkers).toHaveBeenCalled();

	});

	xit('should handle update pick up when no data', () => {
		comp.updatePickUp();
		expect(comp.vc.origin).toEqual(null);
	});

	it('should handle update waypoints correctly', () => {
		comp.updateWayPoints(MOCK_CONSTANTS.BOOKING.waypoints);
		expect(comp.vc.waypoints).not.toEqual(null);
	});

	it('should handle update waypoints correctly', () => {
		comp.atoBModel.destination = MOCK_CONSTANTS.BOOKING.destination;
		comp.updateWayPoints([]);
		expect(comp.uiDestination).not.toEqual(null);
	});

	it('should handle start booking', () => {
		spyOn(comp.makeBooking, 'emit');
		expect(comp.startBooking).toBeDefined();
		comp.startBooking();
		fixture.whenStable().then(() => {
			fixture.autoDetectChanges();
			expect(comp.makeBooking.emit).toHaveBeenCalled();

		});
	});

	it('should handle amend booking', () => {
		spyOn(comp.editBooking, 'emit');
		expect(comp.amendBooking).toBeDefined();
		comp.amendBooking();
		fixture.whenStable().then(() => {
			fixture.autoDetectChanges();
			expect(comp.editBooking.emit).toHaveBeenCalled();

		});
	});

	xit('should handle update Map for Origin changes', () => {
		comp.atoBModel.origin = MOCK_CONSTANTS.PLACE1;
		comp.atoBModel.destination = MOCK_CONSTANTS.PLACE1;
		expect(comp.updateMap).toBeDefined();
		comp.updateMap('ORG');
		fixture.whenStable().then(() => {
			fixture.autoDetectChanges();
			expect(comp.vc.originPlaceId).not.toBe(null);

		});
	});

	xit('should handle update Map for Destination changes', () => {
		comp.atoBModel.origin = MOCK_CONSTANTS.PLACE1;
		comp.atoBModel.destination = MOCK_CONSTANTS.PLACE1;
		expect(comp.updateMap).toBeDefined();
		comp.updateMap('XX');
		fixture.whenStable().then(() => {
			fixture.autoDetectChanges();
			expect(comp.vc.originPlaceId).not.toBe(null);

		});
	});

	xit('should handle resolve Destination title', () => {
		comp.atoBModel.waypoints = MOCK_CONSTANTS.BOOKING.waypoints;
		expect(comp.updateMap).toBeDefined();
		comp.resolveDestination();
		fixture.whenStable().then(() => {
			fixture.autoDetectChanges();
			expect(comp.uiDestination).not.toBe(null);

		});
	});
});

describe('A to B - SummaryWidgetComponent - Edit Booking', () => {
	let comp: SummaryWidgetComponent;
	let fixture: ComponentFixture <SummaryWidgetComponent>;
	afterEach(() => {
		TestBed.resetTestingModule();
	});
	// async beforeEach
	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [AppModule, AtoBModule,
				RouterTestingModule.withRoutes([{ path: 'dashboard/a-to-b/booking-confirmation/1234-test/edit', component: SummaryWidgetComponent }]),
				BrowserAnimationsModule],
			declarations: [],
			schemas: [NO_ERRORS_SCHEMA],
			providers: [
				{ provide: ActivatedRoute, useValue: {params: Observable.of({bookingRef: '1234-test'})} },
			]
		}).compileComponents(); // compile template and css
	}));

	// synchronous beforeEach
	beforeEach(() => {
		fixture = TestBed.createComponent(SummaryWidgetComponent);
		comp = fixture.debugElement.componentInstance;
	});

	it('should have journey details component correctly defined', () => {
		expect(fixture).toBeDefined();
		expect(comp).toBeDefined();

	});

	it('should set isEdit flag on init ', () => {
		comp.localStorage.set('1234-test', MOCK_CONSTANTS.BOOKING);
		comp.ngOnInit();
		fixture.whenStable().then(() => {
			expect(comp.isEditBooking).toBe(true);
		});
	});

	it('should set waypoints array on init ', () => {
		comp.localStorage.set('1234-test', MOCK_CONSTANTS.BOOKING);
		comp.ngOnInit();
		fixture.whenStable().then(() => {
			expect(comp.atoBModel.waypoints).not.toBe(null);
		});
	});
});
