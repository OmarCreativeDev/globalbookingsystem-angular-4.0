import {
	Component, Input, ViewChild, HostListener, ElementRef, OnInit, NgZone, Output,
	EventEmitter, AfterViewInit
} from '@angular/core';
import { DirectionsMapDirective } from '../../directives/directions.google-map.directive';
import { MapsAPILoader } from '@agm/core';
import { LocalStorage } from '../../services/local-storage/local-storage.service';
import { AtoBModel } from '../../models/a-to-b/a-to-b.model';
import { ActivatedRoute } from '@angular/router';

// declare var google: any;

@Component({
	selector: 'summary-widget',
	templateUrl: './summary-widget.component.html',
	styleUrls: ['./summary-widget.component.scss']
})

export class SummaryWidgetComponent implements OnInit {
	// sticky widget
	public isSticky: boolean = false;
	public loading: boolean = true;
	public duration: string = '';
	// map init options
	public latitude: number = 57.751076;
	public longitude: number = 11.271973;
	public zoom: number = 30;
	// map directions directive
	@ViewChild(DirectionsMapDirective) public vc: DirectionsMapDirective;
	@Output() public notifyEstimatedTime = new EventEmitter();

	@Output() public makeBooking = new EventEmitter();
	@Output() public editBooking = new EventEmitter();
	// asap time date
	public asap: any;
	public isEditBooking: boolean = false;
	public atoBModel: AtoBModel = new AtoBModel();

	public uiDestination: any = '';

	public isDisplayed: boolean = true;

	constructor(
		public mapsAPILoader: MapsAPILoader,
		public el: ElementRef,
		public ngZone: NgZone,
		public route: ActivatedRoute,
		public localStorage: LocalStorage
	) {
	}

	@HostListener('window:scroll', ['$event'])
	public detectWindowPosition() {
		if (window.pageYOffset > 155) {
			this.isSticky = true;
		} else {
			this.isSticky = false;
		}
	}

	public ngOnInit() {
		this.route.params.subscribe((value) => {
			let ref = value['bookingRef'];
			if (ref !== undefined) {
				this.isEditBooking = true;
				this.atoBModel.waypoints = this.localStorage.get(ref)['waypoints'];
			}
		});
		this.hideWidget();
		this.setCurrentPosition();
	}

	public updatePickUp(data?) {
		if (!!data) {
			this.atoBModel.origin = data;
			this.ngZone.run(() => {
				this.vc.origin = data;
				this.vc.originPlaceId = this.atoBModel.origin['place_id'];
				this.isEditBooking === true ? this.isEditBooking = true : this.vc.updateMarkers();
			});
		}
	}

	public updateDropOff(data?) {
		if (data) {
			this.atoBModel.destination = data;
			this.vc.destination = data;
			this.vc.destinationPlaceId = this.atoBModel.destination['place_id'];
			this.vc.updateMarkers();
		} else {
			this.atoBModel.destination = null;
			// this.vc.removeDropOff();
			this.vc.destination = null;
			this.vc.destinationPlaceId = undefined;
			this.vc.updateMarkers();
			if (!this.atoBModel.origin) {
				this.hideWidget();
			}
			this.updateEstimatedTime(null);
		}
	}

	public updateJourneyType(journey) {
		this.atoBModel.journey = journey;
	}

	public updatePaymentType(payment) {
		this.atoBModel.payment = payment;
	}

	public updatePrice(price) {
		this.atoBModel.price = price;
	}

	public updateEstimatedTime(duration) {
		this.duration = duration;
		this.atoBModel.estimatedTime = this.duration;
	}

	public updatePassenger(passenger) {
		this.atoBModel.passenger = passenger ? passenger : null;
	}

	public updateWayPoints(waypoints) {
		this.vc.cleanMarkers()
			.then(() => {
				if (waypoints.length === 0) {
					// we need to remove every marker but origin and destination
					this.ngZone.run(() => {
						this.vc.updateMarkers();
					});
				} else {
					this.ngZone.run(() => {
						this.vc.sanitizeWaypoints(waypoints)
							.then(() => {
								this.vc.updateDirections();
							});
					});
				}
			});
		this.atoBModel.waypoints = waypoints;
	}

	public hideWidget() {
		this.isDisplayed = false;
		this.el.nativeElement.style.display = 'none';
	}

	public showWidget() {
		this.isDisplayed = true;
		this.el.nativeElement.style.display = 'block';
	}

	public startBooking() {
		this.makeBooking.emit();
	}

	public amendBooking() {
		this.editBooking.emit();
	}

	private setCurrentPosition() {
		// set current location
		if ('geolocation' in navigator) {
			navigator.geolocation.getCurrentPosition((position) => {
				this.latitude = position.coords.latitude;
				this.longitude = position.coords.longitude;
			});
		}
	}
}
