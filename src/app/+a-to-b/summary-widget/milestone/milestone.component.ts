/**
 * Created by arfs on 04/05/2017.
 */
import { Component, Input, OnInit } from '@angular/core';
import { AtoBModel } from '../../../models/a-to-b/a-to-b.model';

@Component({
	selector: 'milestone',
	templateUrl: 'milestone.component.html',
	styleUrls: ['./milestone.component.scss']
})
export class JourneyMilestoneComponent implements OnInit {

	@Input() public model: Object;
	@Input() public step: number = 0;
	@Input() public icon: string = '';
	@Input() public title: string = '';
	@Input() public skinnyIcon: boolean = false;
	public dataToRender: string = '';
	public dataExtraToRender: string = '';

	constructor() {
		//
	}

	public ngOnInit() {
		//
	}

	public handleMilestoneStatus() {
		let flag = false;
		switch (true) {
			case this.step === 1:
				flag = !!this.model['origin'] && !!this.model['destination'];
				this.dataToRender = this.model['origin'] ? this.model['origin']['formatted_address'] : '';
				this.dataExtraToRender = this.model['destination'] ? this.model['destination']['formatted_address'] : '';
				if (this.model['waypoints'] !== undefined) {
					let sanitizedWaypoints = [];
					this.model['waypoints'].map((drop) => {
						if (( drop.asyncData !== null && drop.addressLocation !== undefined ) || (drop.asyncData !== '' && drop.addressLocation !== undefined)) {
							sanitizedWaypoints.push(drop.addressLocation);
						}
					});
					let lastItem = sanitizedWaypoints[sanitizedWaypoints.length - 1];
					if (sanitizedWaypoints.length === 0) {
						this.dataExtraToRender = this.model['destination'] ? this.model['destination']['formatted_address'] : '';
					} else {
						this.dataExtraToRender = lastItem['formatted_address'];
					}
				}
				break;
			case this.step === 2:
				flag = !!this.model['service'];
				this.dataToRender = this.model['service'] ? this.model['service']['description'] : '';
				break;
			case this.step === 3:
				flag = !!this.model['passenger'];
				this.dataToRender = this.model['passenger'] ? this.model['passenger']['namePassenger'] : '';
				break;
			case this.step === 4:
				flag = !!this.model['payment'];
				this.dataToRender = this.model['payment'];
				break;
			default:
				break;
		}
		return flag;
	}
}
