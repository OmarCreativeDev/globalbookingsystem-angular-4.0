import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, TestBed, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AtoBModule } from '../../a-to-b.module';
import { MOCK_CONSTANTS } from '../../../../../config/mock-helpers/mock.constants';

// Load the implementations that should be tested
import { SummaryWidgetComponent } from './summary-widget.component';
import { LocalStorage } from '../../services/local-storage/local-storage.service';
import { AppModule } from '../../../app.module';
import { JourneyMilestoneComponent } from './milestone.component';

describe('A to B - SummaryWidgetComponent - JourneyMilestoneComponent', () => {
	let comp: JourneyMilestoneComponent;
	let fixture: ComponentFixture<JourneyMilestoneComponent>;
	afterEach(() => {
		// TestBed.resetTestingModule();
	});

	// async beforeEach
	beforeEach(async(() => {
		// window['google'] = google;
		TestBed.configureTestingModule({
			imports: [AppModule, AtoBModule, RouterTestingModule.withRoutes([
				{path: '', component: JourneyMilestoneComponent}
			])],
			declarations: [],
			schemas: [NO_ERRORS_SCHEMA],
			providers: []
		}).compileComponents().then(() => {
			fixture = TestBed.createComponent(JourneyMilestoneComponent);
			comp = fixture.debugElement.componentInstance;
		});
	}));

	it('should have SummaryWidgetComponent correctly defined', () => {
		expect(fixture).toBeDefined();
		expect(comp).toBeDefined();
	});

	it('should have dataToRender defined and set with correct value', () => {
		expect(comp.dataToRender).toBeDefined();
	});

	it('should have dataExtraToRender defined and set with correct value', () => {
		expect(comp.dataExtraToRender).toBeDefined();
	});

	it('should have @Input model correctly defined', () => {
		expect(comp.model).not.toBeDefined();
	});

	it('should have @Input step correctly defined', () => {
		expect(comp.step).toBeDefined();
	});

	it('should have @Input icon correctly defined', () => {
		expect(comp.icon).toBeDefined();
	});

	it('should have @Input title correctly defined', () => {
		expect(comp.title).toBeDefined();
	});

	it('should have @Input skinnyIcon correctly defined', () => {
		expect(comp.skinnyIcon).toBeDefined();
	});

	it('should have method to handle steps and status correctly defined', () => {
		expect(comp.handleMilestoneStatus).toBeDefined();
	});

	it('when is step 1 , it should have destination field correctly defined', () => {

		comp.model = MOCK_CONSTANTS.BOOKING;
		comp.step = 1;
		comp.ngOnInit();
		comp.handleMilestoneStatus();
		fixture.whenStable().then(() => {
			fixture.autoDetectChanges();
			expect(comp.dataToRender).not.toBe('');
			expect(comp.dataExtraToRender).not.toBe('');
		});
	});

	it('when is step 2 , it should have field correctly defined', () => {

		comp.model = MOCK_CONSTANTS.BOOKING;
		comp.step = 2;
		comp.ngOnInit();
		comp.handleMilestoneStatus();
		fixture.whenStable().then(() => {
			fixture.autoDetectChanges();
			expect(comp.dataToRender).not.toBe('');
			expect(comp.dataExtraToRender).toBe('');
		});
	});
	it('when is step 3 , it should have field correctly defined', () => {
		comp.model = MOCK_CONSTANTS.BOOKING;
		comp.step = 3;
		comp.ngOnInit();
		comp.handleMilestoneStatus();
		fixture.whenStable().then(() => {
			fixture.autoDetectChanges();
			expect(comp.dataToRender).not.toBe('');
			expect(comp.dataExtraToRender).toBe('');
		});
	});

	it('when is step 4 , it should have field correctly defined', () => {
		comp.model = MOCK_CONSTANTS.BOOKING;
		comp.step = 4;
		comp.ngOnInit();
		comp.handleMilestoneStatus();
		fixture.whenStable().then(() => {
			fixture.autoDetectChanges();
			expect(comp.dataToRender).not.toBe('');
			expect(comp.dataExtraToRender).toBe('');
		});
	});

});
