/**
 * Created by arfs on 05/05/2017.
 */
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AtoBModel } from '../../../models/a-to-b/a-to-b.model';

@Component({
    selector: 'bottom-widget',
    templateUrl: 'bottom.component.html',
	styleUrls: ['./bottom.component.scss']
})
export class BottomWidgetComponent implements OnInit {

	@Input() public model: AtoBModel;
	@Input() public isEdit: boolean;
	@Output() public amendBooking = new EventEmitter();
	@Output() public startBooking = new EventEmitter();
    constructor() {
    	//
	}
    public ngOnInit() {
    	//
	}

	public triggerAmendBooking() {
		this.amendBooking.emit();
	}

	public triggerStartBooking() {
		this.startBooking.emit();
	}
}
