import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, TestBed, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AtoBModule } from '../../a-to-b.module';

// Load the implementations that should be tested
import { AppModule } from '../../../app.module';
import { BottomWidgetComponent } from './bottom.component';
import { APP_BASE_HREF } from '@angular/common';

describe('A to B - SummaryWidgetComponent - BottomWidgetComponent', () => {
	let comp: BottomWidgetComponent;
	let fixture: ComponentFixture <BottomWidgetComponent>;
	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [AppModule, AtoBModule],
			// declarations: [ HeaderWidgetComponent ],
			providers: [
				{ provide: APP_BASE_HREF, useValue: '/' },
			]
		});
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(BottomWidgetComponent);
		comp = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should have @Input model correctly defined', () => {
		expect(comp.model).not.toBeDefined();
	});

	it('should have @Input step correctly defined', () => {
		expect(comp.isEdit).not.toBeDefined();
	});

	it('should have @Output icon correctly defined', () => {
		expect(comp.amendBooking).toBeDefined();
	});

	it('should have @Output title correctly defined', () => {
		expect(comp.startBooking).toBeDefined();
	});

	it('should have method to trigger Amend Booking correctly defined', () => {
		expect(comp.triggerAmendBooking).toBeDefined();
	});

	it('should have method to trigger Start Booking correctly defined', () => {
		expect(comp.triggerStartBooking).toBeDefined();
	});

	it('should emit event to parent to notify amend booking correctly defined', async(() => {
		spyOn(comp.amendBooking, 'emit');
		comp.triggerAmendBooking();
		fixture.detectChanges();
		fixture.whenStable().then(() => {
			expect(comp.amendBooking.emit).toHaveBeenCalled();
		});
	}));

	it('should emit event to parent to notify start booking correctly defined', async(() => {
		spyOn(comp.startBooking, 'emit');
		comp.triggerStartBooking();
		fixture.detectChanges();
		fixture.whenStable().then(() => {
			expect(comp.startBooking.emit).toHaveBeenCalled();
		});
	}));

});
