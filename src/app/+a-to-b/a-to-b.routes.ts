import { AtoBComponent } from './a-to-b.component';
import { BookingConfirmationComponent } from './booking-confirmation/booking-confirmation.component';
import { AtoBMockComponent } from '../components/a-to-b-mock/a-to-b-mock.component';

export const routes = [
		{ path: '', component: AtoBComponent, pathMatch: 'full'  },
		// { path: '', children: [
			{ path: 'booking-confirmation/:bookingRef', component: BookingConfirmationComponent },
	        { path: 'booking-confirmation/:bookingRef/edit', component: AtoBComponent },
	        { path: 'quote', component: AtoBComponent }
		// ]}

];
