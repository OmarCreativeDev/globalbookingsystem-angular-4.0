import { browser, by, element, protractor } from 'protractor';

xdescribe('A to B - Passenger Details Component', () => {
	it('Navigate to A to B Journey page', () => {
		browser.driver.manage().window().setSize(1280, 1024);
		browser.get('/');
		browser.sleep(1000);

		let dashboardLink = element(by.linkText('Dashboard'));
		dashboardLink.click();
		browser.sleep(1000);

		let aToBLink = element(by.linkText('BOOK A TO B'));
		aToBLink.click();
		browser.sleep(1000);
	});

	it('should show passenger details and enter a valid email address and no name: expect to see "Please add passenger name" error', () => {
		let showPassengerDetails = element.all(by.css('passenger-details h2'));
		let name = element(by.css('input[formControlName=name]'));
		let email = element(by.css('input[formControlName=email]'));

		showPassengerDetails.click();
		browser.sleep(2000);

		email.sendKeys('joe.bloggs@gmail.com');
		email.sendKeys(protractor.Key.TAB);

		expect<any>(element(by.css('.text-danger')).getText()).toEqual('Please add a passenger name');
	});

	it('should enter valid name and email address: expect to see no errors', () => {
		let name = element(by.css('input[formControlName=name]'));
		let email = element(by.css('input[formControlName=email]'));

		name.sendKeys('Joe Bloggs');
		email.clear();
		email.sendKeys('joe.bloggs@gmail.com');
		email.sendKeys(protractor.Key.TAB);

		expect<any>(element(by.css('.text-danger')).isPresent()).toBe(false);
	});

	function testEmailInput(setName, setEmail) {
		let pageHeader = element.all(by.css('h2')).get(0);
		let name = element(by.css('input[formControlName=name]'));
		let email = element(by.css('input[formControlName=email]'));

		name.clear();
		name.sendKeys(setName);
		email.clear();
		email.sendKeys(setEmail);
		pageHeader.click();
		expect<any>(element(by.css('.text-danger')).getText()).toEqual('Please insert a valid email');
	}

	it('should enter email address input too "joe.bloggsgmail.com" and a valid name: expect to see "Please insert a valid email" error', () => {
		testEmailInput('Joe Bloggs', 'joe.bloggsgmail.com');
	});

	it('should enter email address input too "joe.bloggs@gmail.c" and a valid name: expect to see "Please insert a valid email" error', () => {
		testEmailInput('Joe Bloggs', 'joe.bloggs@gmail.c');
	});

	it('should enter email address input too "joe.bloggs@" and a valid name: expect to see "Please insert a valid email" error', () => {
		testEmailInput('Joe Bloggs', 'joe.bloggs@');
	});

	it('should enter email address input too "joebloggs" and a valid name: expect to see "Please insert a valid email" error', () => {
		testEmailInput('Joe Bloggs', 'joebloggs');
	});

	it('should enter email address input too ".....!!!!@com" and a valid name: expect to see "Please insert a valid email" error', () => {
		testEmailInput('Joe Bloggs', '.....!!!!@com');
	});

});
