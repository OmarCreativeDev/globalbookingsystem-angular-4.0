import { Component, EventEmitter, Input, OnInit, Output, ChangeDetectionStrategy } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { GLOBAL_CONSTANTS } from '../../common/global.constants';
import { ActivatedRoute } from '@angular/router';
import { LocalStorage } from '../../services/local-storage/local-storage.service';

@Component({
	selector: 'passenger-details',
	changeDetection: ChangeDetectionStrategy.OnPush,
	templateUrl: './passenger-details.component.html'
})
export class PassengerDetailsComponent implements OnInit {

	@Input() public index: any = 0;
	public form: FormGroup;
	public passengerDetails: Object;
	public isCollapsed = false;
	public isFat = Boolean(GLOBAL_CONSTANTS.IS_FAT);
	@Output() public onValidForm = new EventEmitter();

	private emailValidator: string = GLOBAL_CONSTANTS.emailValidator;
	private mobileValidator: string = GLOBAL_CONSTANTS.mobileValidator;

	constructor(
		private formBuilder: FormBuilder,
		private route: ActivatedRoute,
		public localStorage: LocalStorage,
	) {}

	public ngOnInit() {
		this.form = this.formBuilder.group({
			name: [ '', [Validators.required, Validators.minLength(2)]],
			email: [ '', [Validators.required, Validators.pattern(this.emailValidator)]],
			mobile: [ '', [Validators.required, Validators.pattern(this.mobileValidator)]]
		});
		this.route.params.subscribe((value) => {
			let ref = value['bookingRef'];
			if (ref !== undefined) {
				let booking =  this.localStorage.get(ref);
				this.isCollapsed = false;
				this.passengerDetails = booking['passenger'];
				this.form.controls['name'].setValue(this.passengerDetails['namePassenger']);
				this.form.controls['email'].setValue(this.passengerDetails['emailPassenger']);
				this.onValidForm.emit(this.passengerDetails);
			}
		});
	}

	public checkForm() {
		let data = {
			namePassenger: this.form.controls.name.value,
			emailPassenger: this.form.controls.email.value,
			mobilePassenger: this.form.controls.mobile.value
		};

		if (this.form.valid) {
			this.onValidForm.emit(data);
		} else {
			this.onValidForm.emit();
		}
	}
}
