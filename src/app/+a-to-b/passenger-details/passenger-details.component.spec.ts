import { NO_ERRORS_SCHEMA } from '@angular/core';
import { inject, async, TestBed, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AtoBModule } from '../a-to-b.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { FormBuilder, ReactiveFormsModule } from '@angular/forms';

// Load the implementations that should be tested
import { PassengerDetailsComponent } from './passenger-details.component';
import { LocalStorage } from '../../services/local-storage/local-storage.service';
import { AppModule } from '../../app.module';

describe('A to B - PassengerDetailsComponent', () => {
	let comp: PassengerDetailsComponent;
	let fixture: ComponentFixture <PassengerDetailsComponent>;
	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [AppModule, AtoBModule, ReactiveFormsModule, NoopAnimationsModule, RouterTestingModule.withRoutes([
				{ path: '', component: PassengerDetailsComponent }
			]) ],
			declarations: [],
			schemas: [NO_ERRORS_SCHEMA],
			providers: [LocalStorage]
		}).compileComponents().then( () => {
			fixture = TestBed.createComponent(PassengerDetailsComponent);
			comp = fixture.componentInstance;
			fixture.detectChanges(); // trigger initial data binding
		});
	}));

	it('should have passenger details component correctly defined', () => {
		expect(comp).toBeTruthy();
	});

	it('form should be invalid when empty', () => {
		expect(comp.form.valid).toBeFalsy();
	});

	it('should validate email in form', () => {
		let name = comp.form.controls['email'];
		let errors = name.errors;
		expect(errors['required']).toBeTruthy();
	});
	it('should validate name in form', () => {
		let name = comp.form.controls['name'];
		let errors = name.errors;
		expect(errors['required']).toBeTruthy();
	});
	it('should validate form when correct email and correct name is informed', () => {
		comp.form.controls['name'].setValue('Joe');
		comp.form.controls['email'].setValue('Joe@bloggs.com');
		comp.form.controls['mobile'].setValue('07999555488');
		comp.checkForm();
		fixture.detectChanges();
		expect(comp.form.valid).toBeTruthy();
	});
	it('should emit event to parent when thr form is valid', () => {
		comp.form.controls['name'].setValue('Joe');
		comp.form.controls['email'].setValue('Joe@bloggs.com');
		comp.form.controls['mobile'].setValue('07999555488');
		let name = comp.form.get('name').value;
		spyOn(comp.onValidForm, 'emit').and.returnValue( name );
		comp.checkForm();
		fixture.detectChanges();
		expect(comp.onValidForm.emit).toHaveBeenCalled();
	});

});
