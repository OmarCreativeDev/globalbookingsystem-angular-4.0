import { Component, EventEmitter, OnInit, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { SummaryWidgetComponent } from './summary-widget/summary-widget.component';
import { BookingSummaryComponent } from './booking-summary/booking.summary.component';
import { LocationRequest } from '../models/price/price-location.request.model';
import { PriceRequestModel } from '../models/price/price.request.model';
import { Observable } from 'rxjs';
import { PriceService } from '../services/price/price.service';
import { PriceResponseModel } from '../models/price/price.response.model';
import { ServiceBooking } from '../services/book/book.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorage } from '../services/local-storage/local-storage.service';
import { AtoBModel } from '../models/a-to-b/a-to-b.model';
import { JourneyDetailsComponent } from './journey-details/journey-details.component';
import * as _ from 'lodash';

@Component({
	selector: 'a-to-b',
	templateUrl: './a-to-b.component.html',
	styleUrls: ['./a-to-b.component.scss'],
})
export class AtoBComponent implements OnInit {
	public waypoints: Object[] = [];
	public maxWayPoints: number = 5;
	public atoBModel: AtoBModel;
	public bookingRef: string;
	public price: PriceResponseModel;

	@ViewChild(JourneyDetailsComponent) public journeyDetails: JourneyDetailsComponent;
	@ViewChild(SummaryWidgetComponent) public summaryWidget: SummaryWidgetComponent;

	constructor(
		public priceService: PriceService,
		public serviceBooking: ServiceBooking,
		public router: Router,
		public route: ActivatedRoute,
		public localStorage: LocalStorage) {
			this.atoBModel = new AtoBModel();
			this.route.params.subscribe((value) => {
				this.bookingRef = value['bookingRef'];
			});
	}

	public ngOnInit() {
		this.shouldDisplayWidget();
		this.route.params.subscribe((value) => {
			this.bookingRef = value['bookingRef'];
			if (this.bookingRef !== undefined) {
				let booking = this.localStorage.get(this.bookingRef);
				this.atoBModel.waypoints = booking.waypoints;
				this.atoBModel.waypoints.forEach((stop) => {
					this.addOneDestination(stop);
				});
			}
		});
	}

	public updatePickUp(origin) {
		if (origin === undefined) {
			console.log('empty origin');
		}
		this.atoBModel.origin = origin;
		this.summaryWidget.updatePickUp(origin);
		this.shouldDisplayWidget();
		this.shouldGetQoute();
	}

	public updateDropOff(destination) {
		console.log(destination);
		this.atoBModel.destination = destination;
		this.summaryWidget.updateDropOff(destination);
		this.shouldGetQoute();
	}

	public updatePaymentType(payment) {
		this.atoBModel.payment = payment;
		this.summaryWidget.updatePaymentType(payment);
		this.shouldGetQoute();
	}

	public updateJourneyType(journey) {
		this.atoBModel.journey = journey;
		this.summaryWidget.updateJourneyType(journey);
		this.shouldGetQoute();
	}

	public updateEstimatedTime(time) {
		this.atoBModel.estimatedTime = time;

	}

	public updateSelectedService(selectedServiceCatalog) {
		this.atoBModel.service = selectedServiceCatalog;
		this.summaryWidget.atoBModel.service = selectedServiceCatalog;
		this.shouldGetQoute();
	}

	public updateSelectedPassenger(passenger) {
		this.atoBModel.passenger = passenger;
		this.summaryWidget.updatePassenger(passenger);
	}

	public onRemoveWaypoint(waypoint) {
		this.atoBModel.waypoints = this.atoBModel.waypoints.filter((item) => {
			return item['index'] !== waypoint.index;
		});
		this.summaryWidget.updateWayPoints(this.atoBModel.waypoints);
	}

	public onResetWaypoints(waypoints) {
		this.atoBModel.waypoints = waypoints;
		this.summaryWidget.updateWayPoints(this.atoBModel.waypoints);
	}

	public reOrderWaypoints(waypoints) {
		this.atoBModel.waypoints = waypoints;
		this.summaryWidget.updateWayPoints(this.atoBModel.waypoints);
	}

	public shouldGetQoute() {
		if (this.atoBModel.origin !== undefined && this.atoBModel.destination !== undefined && this.atoBModel.service !== undefined) {
			this.getQuote();
		}
	}

	public shouldDisplayWidget() {
		if (this.atoBModel.origin !== undefined || this.atoBModel.destination !== undefined) {
			this.summaryWidget.showWidget();
		}
	}

	public getQuote() {

		this.priceService.getPriceAtoB(this.preparePriceRequest())
			.subscribe((data) => {
				this.atoBModel.price = data;
				this.summaryWidget.updatePrice(data);
			});
	}

	public prepareData(flag) {
		if (flag === 'ORIGIN') {
			return {
				id: this.atoBModel.origin['id'],
				code: '',
				owner: {group: 'fishbowl', name: 'developer'},
				place_id: this.atoBModel.origin['place_id'],
				formatted_address: this.atoBModel.origin['formatted_address'],
				place: this.atoBModel.origin,
				geo: {lat: this.atoBModel.origin['place'].geo.lat, lng: this.atoBModel.origin['place'].geo.lng},
			};
		} else {
			return {
				id: this.atoBModel.destination['id'],
				code: '',
				owner: {group: 'fishbowl', name: 'developer'},
				place_id: this.atoBModel.destination['place_id'],
				formatted_address: this.atoBModel.destination['formatted_address'],
				place: this.atoBModel.destination,
				geo: {
					lat: this.atoBModel.destination['place'].geo.lat,
					lng: this.atoBModel.destination['place'].geo.lng
				},
			};
		}
	}

	// public prepareWaypoint(stop) {
	// 		return {
	// 			id: stop['id'],
	// 			code: '',
	// 			owner: { group: 'fishbowl', name: 'developer'},
	// 			place_id: stop['place_id'],
	// 			formatted_address: stop['formatted_address'],
	// 			place: stop,
	// 			geo: { lat: stop['place'].geo.lat(), lng: stop['place'].geo.lng()},
	// 		};
	// }

	// public prepareWaypoints() {
	// 	let wp = [];
	// 	this.atoBModel.waypoints.forEach((stop) => {
	// 		let p = this.prepareWaypoint(stop);
	// 		wp.push(p);
	// 	});
	// 	return wp;
	// }

	public preparePriceRequest(): PriceRequestModel {
		let _origin = this.prepareData('ORIGIN');
		let _destination = this.prepareData('DEST');
		let locations: LocationRequest[] = [];
		let origin = new LocationRequest(_origin);
		let destination = new LocationRequest(_destination);
		locations.push(origin);
		locations.push(destination);

		let requestQuoteModel = new PriceRequestModel();
		requestQuoteModel.locations = locations;
		if (this.atoBModel.journey === 'JOURNEY_DETAILS.DATE_OF_JOURNEY_OPTIONS.ASAP') {
			let asap = new Date();
			requestQuoteModel.pickup = asap.toDateString();
		}
		requestQuoteModel.group = '1dbde72f-56c4-4646-8c95-be162d09d949';
		requestQuoteModel.quoteType = 'journey';
		requestQuoteModel.mop = 'Invoice';
		requestQuoteModel.services = [{service: this.atoBModel.service['name']}];
		return requestQuoteModel;
	}

	public bookingConfirmation() {
		let _origin = this.prepareData('ORIGIN');
		let _destination = this.prepareData('DEST');

		let data = {
			origin: _origin,
			destination: _destination,
			passenger: this.atoBModel.passenger,
			payment: this.atoBModel.payment,
			journey: this.atoBModel.journey,
			price: this.atoBModel.price,
			service: this.atoBModel.service,
			waypoints: this.atoBModel.waypoints,
			estimatedTime: this.atoBModel.estimatedTime,
		};
		this.serviceBooking.getBookingConfirmation(data)
			.subscribe((res) => {
				// set local storage
				// let bookingData = res;
				this.localStorage.remove('fromWidget');
				this.localStorage.set(res['bookingReference'], res);
				this.router.navigate(['dashboard/a-to-b/booking-confirmation', res['bookingReference']]);
			});
	}

	public editBookingConfirmation() {
		let booking = this.localStorage.get(this.bookingRef);
		let _origin = this.prepareData('ORIGIN');
		let _destination = this.prepareData('DEST');
		booking.origin = _origin;
		booking.destination = _destination;
		booking.passenger = this.atoBModel.passenger;
		booking.payment = this.atoBModel.payment;
		booking.journey = this.atoBModel.journey;
		booking.price = this.atoBModel.price;
		booking.service = this.atoBModel.service;
		booking.waypoints = this.atoBModel.waypoints;
		booking.estimatedTime = this.atoBModel.estimatedTime;
		this.localStorage.set(booking.bookingReference, booking);
		this.router.navigate(['dashboard/a-to-b/booking-confirmation', booking.bookingReference]);
	}

	public addOneDestination(initData?: Object) {
		// Data pushed here will be passed to the newly created child
		if (this.atoBModel.waypoints.length < this.maxWayPoints) {
			this.atoBModel.waypoints.push({
				canRemove: this.waypoints.length > 0,
				data: initData || {}
			});
		}
	}
}
