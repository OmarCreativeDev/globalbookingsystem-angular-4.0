import { NgModule } from '@angular/core';
import { IndexComponent } from './index.component';
import { SharedModule } from '../+shared/shared.module';
import { routes } from './index.routes';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedComponentsModule } from '../+shared-components/shared-components.module';
import { ReactiveFormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';
import { TranslateModule } from '@ngx-translate/core';
/**
 * @Module IndexModule
 */
@NgModule({
    imports: [
    	SharedModule,
		SharedComponentsModule,
		ReactiveFormsModule,
		CommonModule,
		RouterModule.forChild(routes),
		AgmCoreModule,
		TranslateModule
	],
    declarations: [IndexComponent],
    providers: [],
})
export class IndexModule {
	public static routes = routes;
}
