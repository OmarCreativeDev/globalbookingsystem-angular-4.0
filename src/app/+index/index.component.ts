import { Component, VERSION } from '@angular/core';

@Component({
	selector: 'index',
	templateUrl: './index.component.html',
	styleUrls: ['./index.component.css']
})
/**
 * @Internal
 */
export class IndexComponent  {
	/**
	 *  angular version
	 *  @type string
	 */
	public version = VERSION.full;
}
