import { Routes } from '@angular/router';
import { LoginComponent } from './components/login';
import { NoContentComponent } from './components/no-content';
import { AirportPickupComponent } from './components/airport-pickup';

import { DataResolver } from './app.resolver';
import { IndexComponent } from './_index_/index.component';

export const ROUTES: Routes = [
	{ path: '', loadChildren: './+index#IndexModule'},
	{ path: 'login', component: LoginComponent },
	// { path: 'airport-pickup',  component: AirportPickupComponent },
	{ path: 'dashboard', loadChildren: './+dashboard#DashboardModule'},
	// { path: 'profile-list', loadChildren: './+profile-list#ProfileListModule' },
	// { path: 'profile', loadChildren: './+profile#ProfileModule' },
	{ path: 'styleGuide',  loadChildren: './+style-guide#StyleGuideModule' },
	{ path: 'a-to-b',  loadChildren: './+a-to-b#AtoBModule' },
	// { path: 'barrel', loadChildren: './+barrel#BarrelModule' },
	{ path: '**', component: NoContentComponent },
];
