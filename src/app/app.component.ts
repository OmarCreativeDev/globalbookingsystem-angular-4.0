/*
 * Angular 2 decorators and services
 */
import {
	Component,
	ViewEncapsulation, OnInit
} from '@angular/core';

import {
	Router,
	Event as RouterEvent,
	NavigationStart,
	NavigationEnd,
	NavigationCancel,
	NavigationError, ActivatedRoute, Params
} from '@angular/router';

/*
 * App Component
 * Top Level Component
 */
@Component({
	selector: 'global-app',
	encapsulation: ViewEncapsulation.None,
	styleUrls: [
		'./app.component.scss'
	],
	templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {

	public loading = false;
	public location: string;
	public show: boolean = true;
	constructor(
		public router: Router,
		public route: ActivatedRoute,
	) {
		router.events.subscribe((event: RouterEvent) => {
			this.navigationInterceptor(event);
		});
	}

	public ngOnInit() {
		// todo
		this.shouldShowHeader();
	}

	public navigationInterceptor(event: RouterEvent): void {
		if (event instanceof NavigationStart) {
			this.loading = true;
			this.location = event.url;
		}
		if (event instanceof NavigationEnd) {
			this.loading = false;
			document.body.scrollTop = 0;
		}
		// Set loading state to false in both of the below events
		// to hide the spinner in case a request fails
		if (event instanceof NavigationCancel) {
			this.loading = false;
		}
		if (event instanceof NavigationError) {
			this.loading = false;
		}
		this.shouldShowHeader();
	}

	public shouldShowHeader() {
		if (this.location === '/login' || this.location === '/') {
			this.show = false;
		} else {
			this.show = true;
		}
		return this.show;
	}
}
