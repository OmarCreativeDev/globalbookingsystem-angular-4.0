import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
    inject,
    async,
    TestBed,
    ComponentFixture
} from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppModule } from './app.module';

// Load the implementations that should be tested
import { AppComponent } from './app.component';

describe('App - Root Component', () => {
    let comp: AppComponent;
    let fixture: ComponentFixture<AppComponent>;
    afterEach(() => {
        TestBed.resetTestingModule();
    });
    // async beforeEach
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [AppModule, RouterTestingModule
            ],
            declarations: [],
            schemas: [NO_ERRORS_SCHEMA],
            providers: []
        })
            .compileComponents(); // compile template and css
    }));

    // synchronous beforeEach
    beforeEach(() => {
        fixture = TestBed.createComponent(AppComponent);
        comp = fixture.componentInstance;

        fixture.detectChanges(); // trigger initial data binding
    });

    it('should have app component correctly defined', () => {
        expect(fixture).toBeDefined();
        expect(comp).toBeDefined();
    });

    it('should have app router defined ', () => {
        expect(comp.router).not.toBeNull();
    });
    it('should have loading property defined ', () => {
        expect(comp.loading).not.toBeNull();
    });

    it('should have router navigationInterceptor defined ', () => {
        expect(comp.navigationInterceptor).toBeDefined();
    });

});
