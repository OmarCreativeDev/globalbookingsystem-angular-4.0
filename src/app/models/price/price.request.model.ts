import { LocationRequest } from './price-location.request.model';
export class PriceRequestModel {
    public group: string;
	public pickup: string;
    public quoteType: string;
    public mop: string;
	public services: Object[];
	public locations: LocationRequest[];
	public rules: Object[] = [
		{
			service_update: {
				override: true,
				attributes: [
					{
						reason: 'Client Travelling'
					},
					{
						approval: 'b163430c-e061-4803-a5f6-b23a7f4b650d'
					}
				]
			}
		}
	];
}
