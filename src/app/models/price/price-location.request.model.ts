
export class LocationRequest {
	public id: number;
	public code: string;
	public owner: Object;
	public place_id: string;
	public formatted_address: string;
	public place: Object; // gmaps object
	public geo: Object;
	public metadata: Object = {
		airport: '',
		park: '' ,
		feature: '' ,
		poi: '' ,
		url: ''
	};
	constructor(r?: Object) {
		r = r || {};
		this.id = r['id'];
		this.code = r['code'];
		this.owner =  r['owner'];
		this.place_id =  r['place_id'];
		this.formatted_address =  r['formatted_address'];
		this.place =  r;
		this.geo =  r['geo'];
	}
}
