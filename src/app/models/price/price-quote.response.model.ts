export class QuotePriceResponseModel {
	public service: string;
	public quoteId: string;
	public net: string;
	public tax: string;
	public total: string;
	public currency_code: string;
	public eta: number;
	public payment_token: string;
	public payment_url: string;
	public prices: Object[];
};

// export class QuotePriceResponseModel {
// 	public name: string;
// 	public type: string;
// 	public amount: string;
// 	public tax: string;
// 	public rate: string;
// 	public unit: string;
// 	public charge: string;
// }
