
import { LocationRequest } from './price-location.request.model';
import { QuotePriceResponseModel } from './price-quote.response.model';
export class PriceResponseModel {
    public request_id: string;
	public locations: LocationRequest[];
	public quotes: QuotePriceResponseModel[];
}
