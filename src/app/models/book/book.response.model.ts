
import { PriceResponseModel } from '../price/price.response.model';
export class BookResponseModel {
	public bookingReference: string;
	public origin: Object;
	public destination: Object;
	public waypoints: Object[] = [];
	public passenger: { namePassenger: string, emailPassenger: string };
	public payment: Object;
	public journey: Object;
	public price: PriceResponseModel;
	public service: Object;
	public estimatedTime: Object;
	public status = ['En Route', 'Arrived', ' status 3'];
	public driverNote: string;

	constructor(r?: Object) {
		r = r || {};
		this.origin = r['origin'];
		this.destination = r['destination'];
		this.waypoints = r['waypoints'] || [];
		this.passenger =  r['passenger'];
		this.journey =  r['journey'];
		this.price =  r['price'];
		this.service =  r['service'];
		this.payment =  r['payment'];
		this.estimatedTime = r['estimatedTime'];
		this.driverNote = r['driverNote'] || 0;
		this.bookingReference =  this.generateRandomBookingRef();
	}
	public generateRandomBookingRef() {
		let stringArray = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e',
				'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
				'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
				'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '!', '?'];
		let stringLength = 15;
		let rndString = '';
		for (let i = 1; i < stringLength; i++) {
			let rndNum = Math.ceil(Math.random() * stringArray.length) - 1;
			rndString = rndString + stringArray[rndNum];
		}
		return rndString;
	}
}
