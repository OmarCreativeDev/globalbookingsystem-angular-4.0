
import { ServiceCatalogLocale } from './service-catalogue-locale.model';
import { Vehicle } from './service-catalogue-vehicle.model';

export class ServiceCatalogueModel {
	public id: number;
	public code: string;
	public name: string;
	public status: number;
	public locales: ServiceCatalogLocale[];
	public vehicles: Vehicle[];

	constructor(r?: Response | Object) {
		r = r || {};
		this.id = r['id'];
		this.code = r['code'];
		this.name =  r['name'];
		this.status =  r['status'];
		this.locales =  r['locales'];
		this.vehicles =  r['vehicles'];
	}
}
