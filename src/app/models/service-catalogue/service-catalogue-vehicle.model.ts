/**
 * Created by arfs on 05/04/2017.
 */

export class Vehicle {
	public price: string;
	public id: number;
	public supplier: number;
	public code: string;
	public name: string;
	public description: string;
	public make: string;
	public model: string;
	public passengers: number;
	public items: number;
	public image: string;
	public serviceId: number;
	public status: number;
	public created: string;
	public modified: string;

}
