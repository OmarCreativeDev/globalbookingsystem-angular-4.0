
export class ServiceCatalogLocale {
	public locale: string;
	public name: string;
	public description: string;
}
