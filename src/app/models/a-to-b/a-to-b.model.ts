import { PriceResponseModel } from '../price/price.response.model';
import { ServiceCatalogueModel } from '../service-catalogue/service-catalogue.model';

export class AtoBModel {
	public origin: Object;
	public destination: Object;
	public waypoints: Object[] = [];
	public passenger: {
		namePassenger: string,
		emailPassenger: string,
		mobilePassenger: string
	};
	public payment: Object;
	public journey: Object;
	public service: ServiceCatalogueModel;
	public price: PriceResponseModel;
	public estimatedTime: Object;
	public driverNote: string;

	constructor(r?: Object) {
		r = r || {};
		this.origin = r['origin'];
		this.destination = r['destination'];
		this.waypoints = r['waypoints'] || [];
		this.passenger =  r['passenger'];
		this.journey =  r['journey'];
		this.price =  r['price'];
		this.service =  r['service'];
		this.payment =  r['payment'];
		this.estimatedTime = r['estimatedTime'];
		this.driverNote = r['driverNote'] || 0;
	}
}
