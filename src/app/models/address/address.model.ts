
import { LocationAddressModel } from './address-location.model';
export class AddressModel {
	public homeAddress: LocationAddressModel;
	public workAddress: LocationAddressModel;
	public favourites: LocationAddressModel[];

	constructor(r?: Object) {
		r = r || {};
		this.homeAddress = r['homeAddress'];
		this.workAddress = r['workAddress'];
		this.favourites =  r['favourites'] || [];
	}
}
