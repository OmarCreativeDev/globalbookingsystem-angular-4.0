/**
 * Created by arfs on 04/05/2017.
 */
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';

import { Component } from './.component';
import { HeaderGlobalComponent } from './header/header.component';
import { FooterGlobalComponent } from './footer/footer.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { CollapseModule } from 'ng2-bootstrap';

@NgModule({
    imports: [CommonModule, RouterModule, TranslateModule, CollapseModule],
    exports: [HeaderGlobalComponent, FooterGlobalComponent, RouterModule, CollapseModule],
    declarations: [HeaderGlobalComponent, FooterGlobalComponent],
    providers: [],
	schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CoreModule { }
