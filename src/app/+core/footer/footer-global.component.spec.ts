import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, TestBed, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { FormBuilder, ReactiveFormsModule } from '@angular/forms';

// Load the implementations that should be tested
import { FooterGlobalComponent } from './footer.component';
import { SharedModule } from '../../+shared/shared.module';
import { SharedComponentsModule } from '../../+shared-components/shared.module';
import { CoreModule } from '../core.module';
import { AppModule } from '../../app.module';
import { CollapseModule, Ng2BootstrapModule } from 'ng2-bootstrap';
import { TranslateModule } from '@ngx-translate/core';

@Component({
	selector: 'wrap-for-test',
	template: `<div>
		<footer-global></footer-global>
	</div>`
})
export class WrapForTestComponent {

}

describe('Components - FooterGlobalComponent', () => {
	let comp: FooterGlobalComponent;
	let fixture: ComponentFixture <FooterGlobalComponent>;
	afterEach(() => {
		TestBed.resetTestingModule();
	});
	// async beforeEach
	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [ AppModule, RouterTestingModule, TranslateModule],
			declarations: [],
			schemas: [NO_ERRORS_SCHEMA],
			providers: []
		})
			.compileComponents(); // compile template and css
	}));

	// synchronous beforeEach
	beforeEach(() => {
		fixture = TestBed.createComponent(FooterGlobalComponent);
		comp = fixture.debugElement.componentInstance;
		console.log(comp);
		// fixture.detectChanges(); // trigger initial data binding
	});

	it('should have global footer component correctly defined', () => {
		expect(fixture).toBeDefined();
		expect(comp).toBeDefined();
	});

});
