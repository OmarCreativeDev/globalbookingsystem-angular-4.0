import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, TestBed, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { FormBuilder, ReactiveFormsModule } from '@angular/forms';

// Load the implementations that should be tested
import { HeaderGlobalComponent } from './header.component';
import { SharedModule } from '../../+shared/shared.module';
import { SharedComponentsModule } from '../../+shared-components/shared.module';
import { CoreModule } from '../core.module';
import { AppModule } from '../../app.module';
import { TranslateModule } from '@ngx-translate/core';

@Component({
	selector: 'wrap-for-test',
	template: `<div>
		<header-global></header-global>
	</div>`
})
export class WrapForTestComponent {

}

describe('Components - HeaderGlobalComponent', () => {
	let comp: HeaderGlobalComponent;
	let fixture: ComponentFixture <HeaderGlobalComponent>;
	afterEach(() => {
		TestBed.resetTestingModule();
	});
	// async beforeEach
	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [AppModule, RouterTestingModule, TranslateModule],
			declarations: [],
			schemas: [NO_ERRORS_SCHEMA],
			providers: []
		})
			.compileComponents(); // compile template and css
	}));

	// synchronous beforeEach
	beforeEach(() => {
		fixture = TestBed.createComponent(HeaderGlobalComponent);
		comp = fixture.componentInstance;
		// fixture.detectChanges(); // trigger initial data binding
	});

	it('should have global header component correctly defined', () => {
		expect(fixture).toBeDefined();
		expect(comp).toBeDefined();
	});

	it('should have public attribute isCollapsed', () => {
		expect(comp.isCollapsed).toBeDefined();
	});

	it('should have public attribute isCollapsed default to true', () => {
		expect(comp.isCollapsed).toBe(true);
	});

});
