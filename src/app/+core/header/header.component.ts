/**
 * Created by arfs on 14/03/2017.
 */
import { Component } from '@angular/core';

@Component({
	selector: 'header-global',
	templateUrl: 'header.component.html',
	styleUrls: ['./header.component.scss']
})

export class HeaderGlobalComponent {
	public isCollapsed: boolean = true;

}
