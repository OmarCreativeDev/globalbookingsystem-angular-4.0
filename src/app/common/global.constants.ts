
export const GLOBAL_CONSTANTS = {

	emailValidator: `^([0-9a-zA-Z]([-_\\.+]*[0-9a-zA-Z]+)*)@([0-9a-zA-Z]([-_\\.]*[0-9a-zA-Z]+)*)[\\.]([a-zA-Z]{2,9})$`,
	mobileValidator: `^(\\+44\\s?7\\d{3}|\\(?07\\d{3}\\)?)\\s?\\d{3}\\s?\\d{3}$`,
	DEFAULT_SELECT_OPTION: 'Select',
	JOURNEY_TYPE_OPTIONS: [
		'JOURNEY_DETAILS.DATE_OF_JOURNEY_OPTIONS.ASAP',
		'JOURNEY_DETAILS.DATE_OF_JOURNEY_OPTIONS.LATER'
	],
	PAYMENT_TYPE_OPTIONS: [
		'JOURNEY_DETAILS.PAYMENT_METHOD_OPTIONS.CASH',
		'JOURNEY_DETAILS.PAYMENT_METHOD_OPTIONS.CARD'
	],
	IS_FAT: '***$styletype***',
	DISABLED_OPTIONS: [
		'JOURNEY_DETAILS.DATE_OF_JOURNEY_OPTIONS.LATER',
		'JOURNEY_DETAILS.PAYMENT_METHOD_OPTIONS.CARD'
	],
	GLOBAL_API: 'https://sandbox.api.addisonlee.com/api/v1-sandbox/',
	GEO_API_URL: 'global/geo/',
	CAR_CATALOGUE_API_URL: 'global/services/services'
};
