import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'timeFormatter'
})

export class TimeFormatterPipe implements PipeTransform {
	public transform(value: any): any {
		if (value) {
			value = Number(value);
			let h = Math.floor(value / 3600);
			let m = Math.floor(value % 3600 / 60);
			let hDisplay = h > 0 ? h + (h === 1 ? ' h ' : ' h ') : '';
			let mDisplay = m > 0 ? m + (m === 1 ? ' m ' : ' m ') : '';
			return hDisplay + mDisplay;
		}
	}
}
