// Load the implementations that should be tested
import { TimeFormatterPipe } from './time-formatter.pipe';

xdescribe('Pipes - TimeFormatterPipe', () => {
	let pipe: TimeFormatterPipe;

	beforeEach(() => {
		pipe = new TimeFormatterPipe();
	});

	it('transforms "2 hours 39 mins" to "2h 39m"', () => {
		let value: any = '2 hours 39 mins';

		expect(pipe.transform(value)).toBe('2h 39m');
	});

	it('transforms "1 hour 11 mins" to "1h 11m"', () => {
		let value: any = '1 hour 11 mins';

		expect(pipe.transform(value)).toBe('1h 11m');
	});

	it('transforms "58 mins" to "58m"', () => {
		let value: any = '58 mins';

		expect(pipe.transform(value)).toBe('58m');
	});

});
