import { DashboardHomeComponent } from './dashboard-home/dashboard.home.component';
import { DashboardComponent } from './dashboard.component';

export const routes = [
	{ path: '', component: DashboardComponent, children: [
		{ path: '', children: [
			{ path: '', component: DashboardHomeComponent },
				{ path: 'a-to-b', loadChildren: '../+a-to-b#AtoBModule'},

		]}
	]},
];
