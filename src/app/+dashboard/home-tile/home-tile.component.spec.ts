import { Component } from '@angular/core';
import { async, TestBed, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../+shared/shared.module';

// Load the implementations that should be tested
import { HomeTileComponent } from './home-tile.component';
import { AppModule } from '../../app.module';

@Component({
	selector: 'wrap-for-test',
	template: `<div>
		<home-tile></home-tile>
	</div>`
})
export class WrapForTestComponent {

}

describe('Components - HomeTile', () => {
	let comp: HomeTileComponent;
	let fixture: ComponentFixture <HomeTileComponent>;
	afterEach(() => {
		TestBed.resetTestingModule();
	});
	// async beforeEach
	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [ AppModule, RouterTestingModule, SharedModule],
			declarations: [HomeTileComponent],
			providers: []
		})
			.compileComponents(); // compile template and css

	}));

	// synchronous beforeEach
	beforeEach(() => {
		fixture = TestBed.createComponent(HomeTileComponent);
		comp = fixture.componentInstance;
		fixture.detectChanges(); // trigger initial data binding
	});

	it('should have Home Tile component correctly defined', () => {
		expect(fixture).toBeDefined();
		expect(comp).toBeDefined();
	});
});
