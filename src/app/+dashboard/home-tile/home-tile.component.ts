import { Component, Input } from '@angular/core';

@Component({
	selector: 'home-tile',
	styleUrls: ['./home-tile.component.scss'],
	templateUrl: './home-tile.component.html'
})

export class HomeTileComponent {
	@Input() public name: string;
	@Input() public route: string;
	@Input() public image: string;
}
