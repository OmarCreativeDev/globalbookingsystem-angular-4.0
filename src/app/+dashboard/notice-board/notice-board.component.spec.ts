import { Component } from '@angular/core';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, TestBed, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../+shared/shared.module';

// Load the implementations that should be tested
import { NoticeBoardComponent } from './notice-board.component';
import { AppModule } from '../../app.module';

@Component({
	selector: 'wrap-for-test',
	template: `<div>
		<notice-board></notice-board>
	</div>`
})
export class WrapForTestComponent {

}

describe('Components - Notice Board', () => {
	let comp: NoticeBoardComponent;
	let fixture: ComponentFixture <NoticeBoardComponent>;
	afterEach(() => {
		TestBed.resetTestingModule();
	});
	// async beforeEach
	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [ AppModule, ReactiveFormsModule, RouterTestingModule, SharedModule],
			declarations: [NoticeBoardComponent],
			schemas: [NO_ERRORS_SCHEMA],
			providers: []
		})
			.compileComponents(); // compile template and css
	}));

	// synchronous beforeEach
	beforeEach(() => {
		fixture = TestBed.createComponent(NoticeBoardComponent);
		comp = fixture.componentInstance;
		fixture.detectChanges(); // trigger initial data binding
	});

	it('should have Notice Board component correctly defined', () => {
		expect(fixture).toBeDefined();
		expect(comp).toBeDefined();
	});

});
