import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard.component';
import { HomeTileComponent } from './home-tile/home-tile.component';
import { NoticeBoardComponent } from './notice-board/notice-board.component';
import { RouterModule } from '@angular/router';
import { routes } from './dashboard.routes';
import { SharedModule } from '../+shared/shared.module';
import { DashboardHomeComponent } from './dashboard-home/dashboard.home.component';
import { SharedComponentsModule } from '../+shared-components/shared-components.module';

@NgModule({
	declarations: [
		// Components / Directives/ Pipes
		DashboardComponent,
		DashboardHomeComponent,
		HomeTileComponent,
		NoticeBoardComponent
	],
	imports: [
		CommonModule,
		SharedModule,
		SharedComponentsModule,
		RouterModule.forChild(routes)
	],
})
export class DashboardModule {
	public static routes = routes;
}
