import { Component } from '@angular/core';

@Component({
	selector: 'custom-date-picker',
	templateUrl: 'custom-date-picker.component.html',
	styleUrls: ['custom-date-picker.component.scss']
})

export class CustomDatePickerComponent {
	public displayDatePicker: boolean = false;
	public date: string;

	public selectedDate(date) {
		this.date = date;
		this.displayDatePicker = false;
	}
}
