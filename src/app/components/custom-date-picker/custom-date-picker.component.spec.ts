import { NO_ERRORS_SCHEMA } from '@angular/core';
import { inject, async, TestBed, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AtoBModule } from '../+a-to-b.module';
import { SharedModule } from '../../+shared';

import { FormBuilder, ReactiveFormsModule } from '@angular/forms';

// Load the implementations that should be tested
import { CustomDatePickerComponent } from './custom-date-picker.component';
import { AppModule } from '../../app.module';

describe('Components - CustomDatePickerComponent', () => {
	let comp: CustomDatePickerComponent;
	let fixture: ComponentFixture <CustomDatePickerComponent>;
	afterEach(() => {
		TestBed.resetTestingModule();
	});
	// async beforeEach
	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [AppModule, ReactiveFormsModule, SharedModule, RouterTestingModule],
			declarations: [CustomDatePickerComponent],
			schemas: [NO_ERRORS_SCHEMA],
			providers: []
		})
			.compileComponents(); // compile template and css
	}));

	// synchronous beforeEach
	beforeEach(() => {
		fixture = TestBed.createComponent(CustomDatePickerComponent);
		comp = fixture.componentInstance;
		fixture.detectChanges(); // trigger initial data binding
	});

	it('should have custom datepicker component correctly defined', () => {
		expect(fixture).toBeDefined();
		expect(comp).toBeDefined();
	});

	it('should have public attribute boolean (displayDatePicker) : to toggle date picker component display', () => {
		expect(comp.displayDatePicker).toBeDefined();
	});
	it('should have public attribute boolean (displayDatePicker) : default to false', () => {
		expect(comp.displayDatePicker).toEqual(false);
	});

	it('should have public attribute string (date) : to display data when user selects a date from date picker', () => {
		expect(comp.date).toEqual(undefined);
	});

	it('should have public method selectedDate : to handle when user selects a date from date picker', () => {
		expect(comp.selectedDate).toBeDefined();
	});

	it('should have public method selectedDate : update selected date when user selects a date from date picker', () => {
		expect(comp.date).toEqual(undefined);
		let testDate = '01-01-1978';
		comp.selectedDate(testDate);
		expect(comp.date).toEqual(testDate);
		expect(comp.displayDatePicker).toEqual(false);
	});
});
