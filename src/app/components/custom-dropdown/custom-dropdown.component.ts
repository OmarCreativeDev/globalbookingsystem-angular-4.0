import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GLOBAL_CONSTANTS } from '../../common/global.constants';

@Component({
    selector: 'custom-dropdown',
    templateUrl: 'custom-dropdown.component.html',
	styleUrls: [ 'custom-dropdown.component.scss' ],
})
export class CustomDropDownComponent implements OnInit {
	public isFat = Boolean(GLOBAL_CONSTANTS.IS_FAT);
	public model: any;
	@Input() public label: string = '';
	@Input() public initData: any;
	@Input() public icon: '';
	@Input() public showIcon: Boolean = false;
	@Input() public isFa: Boolean = false;
	@Input() public options: Object[] = [];
	@Input() public disabled: String[] = [];
	@Output() public onChangeEvent = new EventEmitter();
	public defaultSelection: string = GLOBAL_CONSTANTS.DEFAULT_SELECT_OPTION;

	public selectOption(value) {
		if ( !this.isDisabled(value) ) {
			this.updateOption(value);
			this.onChangeEvent.emit(value);
		}
	}

	public updateOption(value) {
		this.defaultSelection = value;
	}

	public isDisabled(value) {
		if ( this.disabled.indexOf(value) > -1 ) {
			return true;
		} else {
			return false;
		}
	}

	public ngOnInit() {
		this.model = this.initData || '';
	}
}
