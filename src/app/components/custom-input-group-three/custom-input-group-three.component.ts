import { Component, OnInit, Input } from '@angular/core';

import { GLOBAL_CONSTANTS } from '../../common/global.constants';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
    selector: 'custom-input-group-three',
    templateUrl: 'custom-input-group-three.component.html'
})
export class CustomInputGroupThreeComponent implements OnInit {
	public isFat = GLOBAL_CONSTANTS.IS_FAT === 'true';
	public  model: any;
	@Input() public initData: any;
	// top
	@Input() public labelTop: string = '';
	@Input() public placeholderTop: string = '';
	@Input() public iconTop: string = '';
	@Input() public showIconTop: Boolean = false;
	@Input() public isFaTop: Boolean = false;
	// bottom left
	@Input() public labelLeft: string = '';
	@Input() public placeholderLeft: string = '';
	@Input() public iconLeft: string = '';
	@Input() public showIconLeft: Boolean = false;
	@Input() public isFaLeft: Boolean = false;
	// bottom right
	@Input() public labelRight: string = '';
	@Input() public placeholderRight: string = '';
	@Input() public iconRight: string = '';
	@Input() public showIconRight: Boolean = false;
	@Input() public isFaRight: Boolean = false;

	constructor(
		public route: ActivatedRoute,
	) {
		//
	}

    public ngOnInit() {
		this.model = this.initData || '';
		this.route.queryParams.subscribe((p: Params) => {
			this.isFat = p['isFat'];
		});
	}
}
