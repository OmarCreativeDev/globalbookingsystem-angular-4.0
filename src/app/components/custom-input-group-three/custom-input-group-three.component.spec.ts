import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, TestBed, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// Load the implementations that should be tested
import { AppModule } from '../../app.module';
import { CustomInputGroupThreeComponent } from './custom-input-group-three.component';

@Component({
	selector: 'wrap-for-test',
	template: `<div>
		<custom-input-group [labelTop]="'Name'"
							[placeholderTop]="'Name'"
							[isFaTop]="false"
							[iconTop]="'icon-al-pin'"
							[showIconTop]="false"
							[labelLeft]="'Email'"
							[placeholderLeft]="'Email'"
							[isFaLeft]="false"
							[iconLeft]="'icon-al-pin'"
							[showIconLeft]="false"
							[labelRight]="'Mobile'"
							[placeholderRight]="'Mobile'"
							[isFaRight]="false"
							[iconRight]="'icon-al-pin'"
							[showIconRight]="false">
		</custom-input-group>
	</div>`
})
export class WrapForTestComponent {

}

describe('Components - CustomInputGroupComponent', () => {
	let comp: CustomInputGroupThreeComponent;
	let fixture: ComponentFixture <CustomInputGroupThreeComponent>;
	afterEach(() => {
		TestBed.resetTestingModule();
	});
	// async beforeEach
	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [ AppModule, RouterTestingModule, ReactiveFormsModule, FormsModule ],
			declarations: [CustomInputGroupThreeComponent],
			schemas: [NO_ERRORS_SCHEMA],
			providers: []
		})
			.compileComponents(); // compile template and css
	}));

	// synchronous beforeEach
	beforeEach(() => {
		fixture = TestBed.createComponent(CustomInputGroupThreeComponent);
		comp = fixture.debugElement.componentInstance;
	});

	it('should have custom input component correctly defined', () => {
		expect(fixture).toBeDefined();
		expect(comp).toBeDefined();
	});

	it('should have @Input labelTop correctly defined', () => {
		expect(comp.labelTop).toBe('');
	});

	it('should have @Input placeholderTop correctly defined', () => {
		expect(comp.placeholderTop).toBe('');
	});

	it('should have @Input iconTop correctly defined', () => {
		expect(comp.iconTop).toBe('');
	});

	it('should have @Input showIconTop correctly defined', () => {
		expect(comp.showIconTop).toBe(false);
	});

	it('should have @Input isFaTop correctly defined', () => {
		expect(comp.isFaTop).toBe(false);
	});

	it('should have @Input labelLeft correctly defined', () => {
		expect(comp.labelLeft).toBe('');
	});

	it('should have @Input placeholderLeft correctly defined', () => {
		expect(comp.placeholderLeft).toBe('');
	});

	it('should have @Input iconLeft correctly defined', () => {
		expect(comp.iconLeft).toBe('');
	});

	it('should have @Input showIconLeft correctly defined', () => {
		expect(comp.showIconLeft).toBe(false);
	});

	it('should have @Input isFaLeft correctly defined', () => {
		expect(comp.isFaLeft).toBe(false);
	});

	it('should have @Input labelRight correctly defined', () => {
		expect(comp.labelRight).toBe('');
	});

	it('should have @Input placeholderRight correctly defined', () => {
		expect(comp.placeholderRight).toBe('');
	});

	it('should have @Input iconRight correctly defined', () => {
		expect(comp.iconRight).toBe('');
	});

	it('should have @Input showIconRight correctly defined', () => {
		expect(comp.showIconRight).toBe(false);
	});

	it('should have @Input isFaRight correctly defined', () => {
		expect(comp.isFaRight).toBe(false);
	});

});
