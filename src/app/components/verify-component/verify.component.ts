import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { VerifyService } from '../../services/verify/verify.service';

@Component({
    selector: 'al-verify-component',
    templateUrl: 'verify.component.html'
})
export class VerifyComponent implements OnInit {

	@Input() public type: string = 'P'; // E(email) P(phone)
	@Input() public showVerifyForm: boolean = false;
	@Output() public notifyVerified = new EventEmitter();
	@Output() public notifyCancel = new EventEmitter();
	public form: FormGroup;
	public isSubmitted: boolean = false;
	public verified: boolean = false;

	constructor(
		public formBuilder: FormBuilder,
		public verifyService: VerifyService
	) {
    	//
		this.form = this.formBuilder.group({
			verficationCode: [
				'', [ Validators.required, Validators.minLength(6) ]
			]
		});

	}
	public ngOnInit() {
		//

	}

	public submit() {
		this.isSubmitted = true;
		if (this.form.valid) {
			this.verifyService.doVerify(this.form.controls['verficationCode'].value, this.type)
				.subscribe(() => {
					this.notifyVerified.emit();
					this.verified = true;
					setTimeout(() => {
						this.verified = false;
					}, 2000);
				});
		}
	}
	public cancelMobileVerification() {
		this.notifyCancel.emit();
	}
}
