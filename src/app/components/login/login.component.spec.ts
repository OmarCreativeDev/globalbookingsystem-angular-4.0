import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { inject, async, TestBed, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CommonConstants } from '../../common/common-constants';

import { FormBuilder, ReactiveFormsModule } from '@angular/forms';

// Load the implementations that should be tested
import { LoginComponent } from './login.component';
import { SharedModule } from '../../+shared/shared.module';
import { AppModule } from '../../app.module';

@Component({
	selector: 'wrap-for-test',
	template: `<div>
		<login></login>
	</div>`
})
export class WrapForTestComponent {

}

describe('Components - LoginComponent', () => {
	let comp: LoginComponent;
	let fixture: ComponentFixture <LoginComponent>;
	afterEach(() => {
		TestBed.resetTestingModule();
	});
	// async beforeEach
	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [ AppModule, ReactiveFormsModule, RouterTestingModule, SharedModule],
			declarations: [],
			schemas: [NO_ERRORS_SCHEMA],
			providers: []
		})
			.compileComponents(); // compile template and css
	}));

	// synchronous beforeEach
	beforeEach(() => {
		fixture = TestBed.createComponent(LoginComponent);
		comp = fixture.componentInstance;
		fixture.detectChanges(); // trigger initial data binding
	});

	it('should have login component correctly defined', () => {
		expect(fixture).toBeDefined();
		expect(comp).toBeDefined();
	});

});
