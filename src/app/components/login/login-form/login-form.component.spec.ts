import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { inject, async, TestBed, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AtoBModule } from '../a-to-b.module';

import { FormBuilder, ReactiveFormsModule } from '@angular/forms';

// Load the implementations that should be tested
import { LoginFormComponent } from './login-form.component';
import { SharedModule } from '../../../+shared/shared.module';
import { AppModule } from '../../../app.module';

@Component({
	selector: 'wrap-for-test',
	template: `<div>
		<login-form></login-form>
	</div>`
})
export class WrapForTestComponent {

}

describe('Components - LoginFormComponent', () => {
	let comp: LoginFormComponent;
	let fixture: ComponentFixture <LoginFormComponent>;
	afterEach(() => {
		TestBed.resetTestingModule();
	});
	// async beforeEach
	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [ AppModule, ReactiveFormsModule, RouterTestingModule, SharedModule],
			declarations: [],
			schemas: [NO_ERRORS_SCHEMA],
			providers: []
		})
		.compileComponents(); // compile template and css
	}));

	// synchronous beforeEach
	beforeEach(() => {
		fixture = TestBed.createComponent(LoginFormComponent);
		comp = fixture.componentInstance;
		fixture.detectChanges(); // trigger initial data binding
	});

	it('should have login form component correctly defined', () => {
		expect(fixture).toBeDefined();
		expect(comp).toBeDefined();
	});

	it('should have public method submit', () => {
		expect(comp.submit).toBeDefined();
	});

});
