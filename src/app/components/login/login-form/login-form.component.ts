import {
	Component,
	OnInit
} from '@angular/core';

import { Form, FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { GLOBAL_CONSTANTS } from '../../../common/global.constants';

@Component({
	selector: 'login-form',
	templateUrl: './login-form.component.html'
})

export class LoginFormComponent implements OnInit {

	public form: FormGroup;
	private emailValidator: string = GLOBAL_CONSTANTS.emailValidator;

	constructor( private formBuilder: FormBuilder) {}

	public ngOnInit() {
		this.form = this.formBuilder.group({
			email: ['', [Validators.required, Validators.pattern(this.emailValidator)]],
			password: [''],
		});
	}

	public submit() {
		console.log('login button clicked');
	}
}
