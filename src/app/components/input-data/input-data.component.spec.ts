import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { inject, async, TestBed, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
// Load the implementations that should be tested
import { InputDataComponent } from './input-data.component';

@Component({
	selector: 'wrap-for-test',
	template: `<div>
		<input-data
			[title]="'test'"
			[icon]="'pencil-square-o'"
			[inputValue]="'Please pick up from the front of the building'">
		</input-data>
	</div>`
})
export class WrapForTestComponent {

}

describe('Components - InputDataComponent', () => {
	let comp: InputDataComponent;
	let fixture: ComponentFixture <InputDataComponent>;
	afterEach(() => {
		TestBed.resetTestingModule();
	});
	// async beforeEach
	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [ ReactiveFormsModule, RouterTestingModule],
			declarations: [InputDataComponent],
			schemas: [NO_ERRORS_SCHEMA],
			providers: []
		})
			.compileComponents(); // compile template and css
	}));

	// synchronous beforeEach
	beforeEach(() => {
		fixture = TestBed.createComponent(InputDataComponent);
		comp = fixture.componentInstance;
		fixture.detectChanges(); // trigger initial data binding
	});

	it('should have input data component correctly defined', () => {
		expect(fixture).toBeDefined();
		expect(comp).toBeDefined();
	});

	it('should have @Input title correctly defined', () => {
		expect(comp.title).toBe('');
	});

	it('should have @Input icon correctly defined', () => {
		expect(comp.icon).toBe('');
	});

	it('should have @Input inputValue correctly defined', () => {
		expect(comp.inputValue).toBe('');
	});

});
