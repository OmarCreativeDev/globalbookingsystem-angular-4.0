
import { Component, Input, Directive, HostBinding } from '@angular/core';
import { NgModel } from '@angular/forms';

@Component({
	selector: 'input-data',
	styleUrls: ['./input-data.component.scss'],
	templateUrl: 'input-data.component.html',
})

export class InputDataComponent {

	@Input() public icon: string = '';
	@Input() public title: string = '';
	@Input() public inputValue: string = '';

}
