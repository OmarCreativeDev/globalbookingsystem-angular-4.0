import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'change-password-form',
    templateUrl: 'change-password-form.component.html'
})
export class ChangePasswordFormComponent implements OnInit {
	public form: FormGroup;
	public mockData = { email: 'user@awesome.com', password: 'something' };
    constructor(
		private formBuilder: FormBuilder,
	) {
    	//
	}

   public ngOnInit() {
    	//
	   this.form = this.formBuilder.group({
		   oldPassword: [ this.mockData.password, [Validators.required]],
		   newPassword: [ '', [Validators.required]],
		   confirmPassword: [ '', [Validators.required]],
	   });
   }

	public cancelChangePassword() {
		this.form.controls['password'].setValue(this.mockData.password);
	}

}
