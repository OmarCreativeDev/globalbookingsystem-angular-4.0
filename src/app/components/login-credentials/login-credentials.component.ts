import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GLOBAL_CONSTANTS } from '../../common/global.constants';

@Component({
    selector: 'login-credentials',
    templateUrl: 'login-credentials.component.html',
	styleUrls: [ './login-credentials.component.scss' ],
})
export class LoginCredentialsComponent implements OnInit {

	@Input() public showStep: boolean = true;
	@Input() public title: string;
	public form: FormGroup;
	public emailValidator: string = GLOBAL_CONSTANTS.emailValidator;
	// is edit mode
	public isDisabled: Boolean = true;
	public isChangePassword: Boolean = false;
	public mockData = { email: 'user@awesome.com', password: 'something' };

	public showVerify: boolean = false;
	public showSaveBtn: boolean = false;
	public hideCTA: boolean = true;
	public emailVerified: Boolean = false;

    constructor(
		private formBuilder: FormBuilder,
	) {
    	//
	}

    public ngOnInit() {
		this.form = this.formBuilder.group({
			password: [ this.mockData.password, [Validators.required]],
			email: [  this.mockData.email, [Validators.required, Validators.pattern(this.emailValidator)]],
		});
	}

	public submit() {
    	console.log('submit form');
	}

	public getVerification() {
		this.emailVerified = true;
    }

    public cancelVerification() {
		this.form.controls['email'].setValue(this.mockData.email);
		this.form.controls['password'].setValue(this.mockData.password);
		this.isDisabled = true;
		this.showVerify = false;
	}

	public toggleShowSaveBtn() {
		this.isDisabled = false;
		this.showSaveBtn = true;
	}
	public toggleShowVerify() {
		// this.isDisabled = false;
		this.showVerify = true;
		this.hideCTA = true;
	}
	public cancelEdit() {
		this.showVerify = false;
		this.isDisabled = true;
		this.showSaveBtn = false;
	}
	public changePassword() {
		if (!this.emailVerified) {
			return;
		}
		this.cancelVerification();
		this.isChangePassword = true;
	}

	public cancelChangePassword() {
		this.cancelVerification();
		this.isChangePassword = false;
	}
}
