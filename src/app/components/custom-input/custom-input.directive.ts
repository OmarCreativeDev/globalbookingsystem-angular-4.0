import { Directive, HostBinding } from '@angular/core';
import { NgModel } from '@angular/forms';

@Directive({selector: '[ngModel]'})
export class CustomInputDirective {
	constructor(public control: NgModel) {}
    @HostBinding('class.valid') get valid() { return this.control.valid; }
    @HostBinding('class.invalid') get invalid() { return this.control.invalid; }
}
