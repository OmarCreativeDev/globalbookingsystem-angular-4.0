import { Component, OnInit, Input, Output, ChangeDetectionStrategy, EventEmitter } from '@angular/core';
import { GLOBAL_CONSTANTS } from '../../common/global.constants';
import { FormGroup } from '@angular/forms';

@Component({
	selector: 'custom-input',
	changeDetection: ChangeDetectionStrategy.OnPush,
	templateUrl: 'custom-input.component.html',
	styleUrls: [ 'custom-input.component.scss' ]
})
export class CustomInputComponent implements OnInit {
	public isFat = Boolean(GLOBAL_CONSTANTS.IS_FAT);
	public model: any;
	@Input() public label: string = '';
	@Input() public initData: any;
	@Input() public icon: string = '';
	@Input() public showIcon: Boolean = false;
	@Input() public isFa: Boolean = false;
	@Input() public placeholder: string = '';
	@Input() public lightBorder: string = '';
	@Input() public transparentBorder: string = '';
	@Input() public parent: FormGroup;
	@Input() public name: string = '';
	@Output() public onBlurEvent = new EventEmitter();

    public ngOnInit() {
		this.model = this.initData || '';
	}

	public onBlur() {
		this.onBlurEvent.emit();
	}
}
