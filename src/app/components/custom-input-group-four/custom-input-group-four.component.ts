import { Component, OnInit, Input } from '@angular/core';

import { GLOBAL_CONSTANTS } from '../../common/global.constants';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
    selector: 'custom-input-group-four',
    templateUrl: 'custom-input-group-four.component.html'
})
export class CustomInputGroupFourComponent implements OnInit {
	public isFat = GLOBAL_CONSTANTS.IS_FAT === 'true';
	public  model: any;
	@Input() public initData: any;
	// top left
	@Input() public labelTopLeft: string = '';
	@Input() public placeholderTopLeft: string = '';
	@Input() public iconTopLeft: string = '';
	@Input() public showIconTopLeft: Boolean = false;
	@Input() public isFaTopLeft: Boolean = false;
	// top right
	@Input() public labelTopRight: string = '';
	@Input() public placeholderTopRight: string = '';
	@Input() public iconTopRight: string = '';
	@Input() public showIconTopRight: Boolean = false;
	@Input() public isFaTopRight: Boolean = false;
	// bottom left
	@Input() public labelBottomLeft: string = '';
	@Input() public placeholderBottomLeft: string = '';
	@Input() public iconBottomLeft: string = '';
	@Input() public showIconBottomLeft: Boolean = false;
	@Input() public isFaBottomLeft: Boolean = false;
	// bottom right
	@Input() public labelBottomRight: string = '';
	@Input() public placeholderBottomRight: string = '';
	@Input() public iconBottomRight: string = '';
	@Input() public showIconBottomRight: Boolean = false;
	@Input() public isFaBottomRight: Boolean = false;

	constructor(
		public route: ActivatedRoute,
	) {
		//
	}

    public ngOnInit() {
		this.model = this.initData || '';
		this.route.queryParams.subscribe((p: Params) => {
			this.isFat = p['isFat'];
		});
	}
}
