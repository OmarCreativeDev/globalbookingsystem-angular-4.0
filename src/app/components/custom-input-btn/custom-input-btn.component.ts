import { Component, OnInit, Input, QueryList, ViewChildren, Output, EventEmitter } from '@angular/core';

import { GLOBAL_CONSTANTS } from '../../common/global.constants';
import { ActivatedRoute, Params } from '@angular/router';
import { CustomInputChildComponent } from '../custom-input-child/custom-input-child.component';
import { FormBuilder, FormControl, FormGroup, NgModel } from '@angular/forms';
import { Observable } from 'rxjs';
import { TypeaheadMatch } from 'ng2-bootstrap';
import { GeoService } from '../../services/global-geo/geo.service';
import { DragulaService } from 'ng2-dragula';

@Component({
	selector: 'custom-input-btn',
	templateUrl: 'custom-input-btn.component.html',
	styleUrls: ['custom-input-btn.component.scss'],
	providers: [NgModel],
})
export class CustomInputBtnComponent implements OnInit {
	@Input() public label: string = '';
	@Input() public initData: any;
	@Input() public icon: string = '';
	@Input() public showIcon: Boolean = false;
	@Input() public isFa: Boolean = false;
	@Input() public placeholder: string = '';
	@Input() public lightBorder: string = '';
	@Input() public transparentBorder: string = '';
	// events
	@Output() public onAddressSelected = new EventEmitter();
	@Output() public onDragAndDrop = new EventEmitter();
	// child events
	@Output() public onRemoveWaypoint = new EventEmitter();
	@Output() public onAddWaypoint = new EventEmitter();
	@Output() public onChangeWaypoint = new EventEmitter();

	@ViewChildren(CustomInputChildComponent) public allCustomInputs: QueryList<CustomInputChildComponent>;

	public isFat = Boolean(GLOBAL_CONSTANTS.IS_FAT);
	public model: any;
	public inputs: Object[] = [];
	public maxInputs: number = 4;
	public asyncData: any = '';
	public stateCtrl: FormControl = new FormControl();
	public form: FormGroup;

	public typeaheadLoading: boolean;
	public typeaheadNoResults: boolean;
	public dataSource: Observable<any>;

	constructor(
		public geoService: GeoService,
		public formBuilder: FormBuilder,
		private dragulaService: DragulaService) {
			this.dataSource = Observable.create((observer: any) => {
				if (this.asyncData !== '') {
					this.geoService.search(this.asyncData).subscribe((result: any) => {
						observer.next(result.records);
					});
				}
			}, (err) => {
				console.log(err);
			});
			dragulaService.setOptions('third-bag', {
				// removeOnSpill: true
			});
			dragulaService.dropModel.subscribe((value) => {
				this.reorderWayoints();
			});
	}

    public ngOnInit() {
		this.asyncData = this.initData || '';
		this.form = this.formBuilder.group({
			model: this.stateCtrl,
		});
		let inputsCount = this.allCustomInputs ? this.allCustomInputs.length : 0;
		for (let i = 0; i < inputsCount; i++) {
			this.addOneInput(this.allCustomInputs[i] || {});
		}
	}

	public addOneInput(data?: Object) {
		if (this.asyncData === undefined || this.asyncData === '') {
			return;
		}
		if (this.inputs.length < this.maxInputs) {
			this.inputs.push({
				data: data || {}
			});
		}
	}

	public cleanUpPickup() {
		this.asyncData = null;
		this.onAddressSelected.emit();
	}

	public reorderWayoints() {
		let waypoints = [];
		this.allCustomInputs.map((drop) => {
			waypoints.push(drop.addressLocation);
		});
		this.onDragAndDrop.emit(this.allCustomInputs);
	}

	// bs typeahead
	public changeTypeaheadLoading(e: boolean): void {
		this.typeaheadLoading = e;
	}

	public changeTypeaheadNoResults(e: boolean): void {
		this.typeaheadNoResults = e;
	}

	public typeaheadOnSelect(e: TypeaheadMatch): void {
		this.asyncData = e.item.formatted_address;
		this.onAddressSelected.emit(e.item);
	}

	// child event
	public addWaypoint(data) {
		// we update all waypoints
		this.onAddWaypoint.emit(this.allCustomInputs);
	}
	// child event
	public removeWaypoint(index, item) {
		this.inputs.splice(item.index, 1);
		this.onRemoveWaypoint.emit(item);
	}
	// child event
	public changeWaypoint(item) {
		// we update all waypoints
		this.onChangeWaypoint.emit(this.allCustomInputs);
	}
}
