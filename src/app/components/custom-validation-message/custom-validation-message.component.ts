import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
	selector: 'validation-message',
	templateUrl: 'custom-validation-message.component.html',
	styleUrls: ['custom-validation-message.component.scss']
})

export class CustomValidationMessageComponent {

	public static getMessage(key: string, value?) {
		value = value || {};
		return {
			required: `Please enter a ${value.label}`,
			pattern: `Please enter a valid ${value.label}`,
			minlength: `Please enter a ${value.label} it must contain at least ${value.minLength} characters`,
		}[key];
	}

	@Input('label') public label: string;
	@Input('control') public control: FormControl;
	@Input('minLength') public minLength: string;

	constructor() {
		//
	}

	get errorMessage() {
		if (this.control.touched && this.control.status === 'INVALID') {
			for (let key in this.control.errors) {
				if (this.control.errors.hasOwnProperty(key) && this.control.errors[key]) {
					let params = { label: this.label, error: this.control.errors[key], minLength: this.minLength};
					return CustomValidationMessageComponent.getMessage(key, params) || '';
				}
			}
		}
		return null;
	}
}
