import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, TestBed, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// Load the implementations that should be tested
import { AppModule } from '../../app.module';
import { CustomInputChildComponent } from './custom-input-child.component';

@Component({
	selector: 'wrap-for-test',
	template: `<div>
		<custom-input-child [label]="'Drop off'"
							[placeholder]="'Enter location'"
							[isFa]="false"
							[index]="index"
							[icon]="'icon-al-pin'"
							[showIcon]="true"
							(removeMe)="removeOneInput(index)">
		</custom-input-child>
	</div>`
})
export class WrapForTestComponent {

}

describe('Components - CustomInputChildComponent', () => {
	let comp: CustomInputChildComponent;
	let fixture: ComponentFixture <CustomInputChildComponent>;
	afterEach(() => {
		TestBed.resetTestingModule();
	});
	// async beforeEach
	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [ AppModule, RouterTestingModule, ReactiveFormsModule, FormsModule ],
			declarations: [CustomInputChildComponent],
			schemas: [NO_ERRORS_SCHEMA],
			providers: []
		})
			.compileComponents(); // compile template and css
	}));

	// synchronous beforeEach
	beforeEach(() => {
		fixture = TestBed.createComponent(CustomInputChildComponent);
		comp = fixture.debugElement.componentInstance;
	});

	it('should have custom input component correctly defined', () => {
		expect(fixture).toBeDefined();
		expect(comp).toBeDefined();
	});

	it('should have @Input label correctly defined', () => {
		expect(comp.label).toBe('');
	});

	it('should have @Input icon correctly defined', () => {
		expect(comp.icon).toBe('');
	});

	it('should have @Input showIcon correctly defined', () => {
		expect(comp.showIcon).toBe(false);
	});

	it('should have @Input isFa correctly defined', () => {
		expect(comp.isFa).toBe(false);
	});

	it('should have @Input placeholder correctly defined', () => {
		expect(comp.placeholder).toBe('');
	});

	it('should have @Input index correctly defined', () => {
		expect(comp.index).toBe(0);
	});

	xit('should have public method "triggerRemoveMe" to emit event when user removes a input', () => {

		let index = 0;
		spyOn(comp.removeMe, 'emit').and.returnValue(index);
		comp.triggerRemoveMe();

		expect(comp.removeMe.emit).toHaveBeenCalledWith(index);

	});

});
