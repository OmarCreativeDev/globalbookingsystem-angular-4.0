import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

import { GLOBAL_CONSTANTS } from '../../common/global.constants';
import { ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { GeoService } from '../../services/global-geo/geo.service';
import { TypeaheadMatch } from 'ng2-bootstrap';

@Component({
    selector: 'custom-input-child',
    templateUrl: 'custom-input-child.component.html',
	styleUrls: [ 'custom-input-child.component.scss' ]
})
export class CustomInputChildComponent implements OnInit {
	public isFat = Boolean(GLOBAL_CONSTANTS.IS_FAT);
	public  model: any;
	@Input() public label: string = '';
	@Input() public initData: any;
	@Input() public icon: string = '';
	@Input() public index: number = 0;
	@Input() public showIcon: Boolean = false;
	@Input() public isFa: Boolean = false;
	@Input() public placeholder: string = '';
	@Input() public lightBorder: string = '';
	@Input() public transparentBorder: string = '';

	@Output() public onRemoveWaypoint = new EventEmitter();
	@Output() public onAddressSelected = new EventEmitter();
	@Output() public onAddressCleanUp = new EventEmitter();

	public asyncData: any;
	public stateCtrl: FormControl = new FormControl();
	public form: FormGroup;
	public addressLocation: any;

	public typeaheadLoading: boolean;
	public typeaheadNoResults: boolean;
	public dataSource: Observable<any>;

	constructor(
		public geoService: GeoService,
		public formBuilder: FormBuilder,
	) {
		this.dataSource = Observable.create((observer: any) => {
			if (this.asyncData !== '') {
				this.geoService.search(this.asyncData).subscribe((result: any) => {
					observer.next(result.records);
				});
			}
		}, (err) => {
			console.log(err);
		});

	}
    public ngOnInit() {
		this.asyncData = this.initData || null;
		this.form = this.formBuilder.group({
			model: this.stateCtrl,
		});

	}
	public removeWaypoint() {
		this.onRemoveWaypoint.emit(this);
	}
	public cleanUpAddress() {
		this.asyncData = null;
		this.onAddressCleanUp.emit(this);
	}
	public onBlur() {
		//
	}
	public changeTypeaheadLoading(e: boolean): void {
		this.typeaheadLoading = e;
	}
	public changeTypeaheadNoResults(e: boolean): void {
		this.typeaheadNoResults = e;
	}
	public typeaheadOnSelect(e: TypeaheadMatch): void {
		this.asyncData = e.item.formatted_address;
		this.addressLocation = e.item;
		this.onAddressSelected.emit(e.item);
	}
}
