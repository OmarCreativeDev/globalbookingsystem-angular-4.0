import { Component, ViewChild, Input, ElementRef } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';

@Component({
	selector: 'custom-panel',
	styleUrls: ['./custom-panel.component.scss'],
	templateUrl: 'custom-panel.component.html',
	animations: [
		trigger('collapsedState', [
			state('true' , style({
				height: '0',
				display: 'none',
				overflow: 'hidden'
			})),
			state('false', style({
				overflow: 'hidden'
			})),

			transition('* => *', animate('0.5s ease-in-out'))
		])
	]
})

export class CustomPanelComponent {

	@Input() public title: string = '';
	@Input() public step: number = 0;
	@Input() public icon: string = '';
	@Input() public canCollapse: boolean = false;
	@Input() public isCollapsed: boolean = false;
	@Input() public showStep: boolean = true;
	@Input() public lastStep: boolean = false;
	@ViewChild('customPanel') public customPanelElement: ElementRef;
	public firstTimeLoad: boolean = true;

	public togglePanel() {
		if (this.canCollapse) {
			this.isCollapsed = !this.isCollapsed;
			this.firstTimeLoad = false;
		}
	}

	public animationDone() {
		this.customPanelElement.nativeElement.style.overflow = 'visible';
	}
}
