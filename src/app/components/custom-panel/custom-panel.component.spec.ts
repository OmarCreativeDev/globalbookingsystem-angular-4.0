import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { inject, async, TestBed, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
// Load the implementations that should be tested
import { CustomPanelComponent } from './custom-panel.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AppModule } from '../../app.module';

@Component({
	selector: 'wrap-for-test',
	template: `<div>
		<custom-panel [title]="'whatever'"
			[icon]="'test'"
			[step]="2" ></custom-panel>
	</div>`
})
export class WrapForTestComponent {

}

describe('Components - CustomPanelComponent', () => {
	let comp: CustomPanelComponent;
	let fixture: ComponentFixture <CustomPanelComponent>;
	afterEach(() => {
		TestBed.resetTestingModule();
	});
	// async beforeEach
	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [ AppModule, RouterTestingModule, NoopAnimationsModule],
			declarations: [CustomPanelComponent],
			schemas: [NO_ERRORS_SCHEMA],
			providers: []
		})
			.compileComponents(); // compile template and css
	}));

	// synchronous beforeEach
	beforeEach(() => {
		fixture = TestBed.createComponent(CustomPanelComponent);
		comp = fixture.componentInstance;
		fixture.detectChanges(); // trigger initial data binding
	});

	it('should have custom panel component correctly defined', () => {
		expect(fixture).toBeDefined();
		expect(comp).toBeDefined();
	});

	it('should have @Input title correctly defined', () => {
		expect(comp.title).toBe('');
	});
	it('should have @Input icon correctly defined', () => {
		expect(comp.icon).toBe('');
	});
	it('should have @Input step correctly defined', () => {
		expect(comp.step).toBe(0);
	});

	it('should have @Input canCollapse correctly defined', () => {
		expect(comp.canCollapse).toBe(false);
	});

	it('should have @Input isCollapsed correctly defined', () => {
		expect(comp.isCollapsed).toBe(false);
	});

});
