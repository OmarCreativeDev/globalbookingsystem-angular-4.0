import { Component, OnInit, Input, EventEmitter, Output, ViewChild, ElementRef, NgZone } from '@angular/core';
import { GLOBAL_CONSTANTS } from '../../common/global.constants';
import { ActivatedRoute, Params } from '@angular/router';
import { LocationAddressModel } from '../../models/address/address-location.model';
import { GeoService } from '../../services/global-geo/geo.service';
import { Observable } from 'rxjs';
import { TypeaheadMatch } from 'ng2-bootstrap';
import { FormBuilder, FormControl, FormGroup, NgModel, ReactiveFormsModule } from '@angular/forms';

@Component({
	selector: 'custom-input-autocomplete',
	templateUrl: 'custom-input-autocomplete.component.html',
	styleUrls: [ 'custom-input-autocomplete.component.scss' ],
	providers: [NgModel],
})
export class CustomInputAutocompleteComponent implements OnInit {

	@Input() public label: string = '';
	@Input() public initData: any;
	@Input() public icon: string = '';
	@Input() public showIcon: Boolean = false;
	@Input() public showBtn: Boolean = false;
	@Input() public isFa: Boolean = false;
	@Input() public placeholder: string = '';

	@Output() public updateAddress = new EventEmitter();

	public isFat = Boolean(GLOBAL_CONSTANTS.IS_FAT);
	public asyncData: any = '';
	public stateCtrl: FormControl = new FormControl();
	public form: FormGroup;

	public typeaheadLoading: boolean;
	public typeaheadNoResults: boolean;
	public dataSource: Observable<any>;

	constructor(
		public geoService: GeoService,
		private formBuilder: FormBuilder,
		public activatedRoute: ActivatedRoute
	) {
		this.dataSource = Observable.create((observer: any) => {
			if (this.asyncData !== '') {
				this.geoService.search(this.asyncData).subscribe((result: any) => {
					observer.next(result.records);
				});
			}
		}, (err) => {
			console.log(err);
		});
	}

	public ngOnInit() {
		this.asyncData = this.initData || '';
		this.form = this.formBuilder.group({
			model: this.stateCtrl,
		});

	}
	public changeTypeaheadLoading(e: boolean): void {
		this.typeaheadLoading = e;
	}
	public changeTypeaheadNoResults(e: boolean): void {
		this.typeaheadNoResults = e;
	}
	public typeaheadOnSelect(e: TypeaheadMatch): void {
		this.asyncData = e.item.formatted_address;
		this.updateAddress.emit(e.item);
	}

	public cleanUpPickup() {
		this.asyncData = null;
		this.updateAddress.emit();
	}

	public onBlur() {

		// if (!asyncData) {
		// 	this.atoBModel.origin = {};
		// 	this.hasSelectedPickup = false;
		// 	this.pickupEvent.emit();
		// }
		// if (this.searchElementRef.nativeElement.value && this.hasSelectedPickup === false) {
		// 	this.searchElementRef.nativeElement.value = '';
		// }
	}
}
