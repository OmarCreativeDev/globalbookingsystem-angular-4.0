import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, TestBed, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// Load the implementations that should be tested
import { AppModule } from '../../app.module';
import { CustomInputAutocompleteComponent } from './custom-input-autocomplete.component';

@Component({
	selector: 'wrap-for-test',
	template: `<div>
		<custom-input-autocomplete [label]="'Pick up'"
								   [placeholder]="'Pick up address autocomplete'"
								   [isFa]="false"
								   [icon]="'icon-al-pin'"
								   [showIcon]="true">
		</custom-input-autocomplete>
	</div>`
})
export class WrapForTestComponent {

}

describe('Components - CustomInputAutocompleteComponent', () => {
	let comp: CustomInputAutocompleteComponent;
	let fixture: ComponentFixture <CustomInputAutocompleteComponent>;
	afterEach(() => {
		TestBed.resetTestingModule();
	});
	// async beforeEach
	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [ AppModule, RouterTestingModule, ReactiveFormsModule, FormsModule ],
			declarations: [CustomInputAutocompleteComponent],
			schemas: [NO_ERRORS_SCHEMA],
			providers: []
		})
			.compileComponents(); // compile template and css
	}));

	// synchronous beforeEach
	beforeEach(() => {
		fixture = TestBed.createComponent(CustomInputAutocompleteComponent);
		comp = fixture.debugElement.componentInstance;
	});

	it('should have custom input component correctly defined', () => {
		expect(fixture).toBeDefined();
		expect(comp).toBeDefined();
	});

	it('should have @Input label correctly defined', () => {
		expect(comp.label).toBe('');
	});

	it('should have @Input icon correctly defined', () => {
		expect(comp.icon).toBe('');
	});

	it('should have @Input showIcon correctly defined', () => {
		expect(comp.showIcon).toBe(false);
	});

	it('should have @Input isFa correctly defined', () => {
		expect(comp.isFa).toBe(false);
	});

	it('should have @Input placeholder correctly defined', () => {
		expect(comp.placeholder).toBe('');
	});

});
