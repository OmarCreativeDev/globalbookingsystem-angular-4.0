/**
 * Created by arfs on 02/04/2017.
 */
import { AgmMarker, GoogleMapsAPIWrapper }  from '@agm/core';
import { Directive, EventEmitter, Input, Output } from '@angular/core';
import { Marker, MarkerOptions } from '@agm/core/services/google-maps-types';
declare var google: any;
@Directive({
	selector: 'agm-map-directions'
})
export class DirectionsMapDirective {
	@Input() public origin: any;
	@Input() public destination: any;
	@Input() public originPlaceId: any;
	@Input() public destinationPlaceId: any;
	@Input() public waypoints: any = [];
	@Input() public directionsDisplay: any;
	@Input() public estimatedTime: any;
	@Input() public estimatedDistance: any;
	@Output() public notifyEstimatedTime = new EventEmitter();

	public markerOptions: MarkerOptions;
	public markers = [];

	constructor(private gmapsApi: GoogleMapsAPIWrapper) {
		//
	}
	public cleanMarkers() {
		return new Promise((resolve, reject) => {
			this.markers.forEach((marker) => {
				marker.setMap(null);
			});
			this.markers = [];
			resolve();
		});
	}

	public sanitizeWaypoints(customInputs) {
		return new Promise((resolve, reject) => {
			// if asyncData is null, we need to remove his marker
			this.directionsDisplay.setMap(null);
			let sanitizedWaypoints = [];
			customInputs.map((drop) => {
				if (drop.asyncData !== null) {
					sanitizedWaypoints.push(drop.addressLocation);
				} else {
					this.removeOneMarker(drop.addressLocation);
				}
			});
			this.waypoints = sanitizedWaypoints;
			resolve();
		});
	}

	public removeOneMarker(address) {
		this.markers.forEach((marker) => {
			if (marker.position.lat().toFixed(5)  === address.place['geo'].lat.toFixed(5) && marker.position.lng().toFixed(5)  === address.place['geo'].lng.toFixed(5)) {
				marker.setMap(null);
			}
		});
	}

	/**
	 * Google Map Directions needs
	 * 1.start(origin),
	 * 2.end(destination),
	 * 3.waypoints(extra destinations)
	 * Watch out!
	 * When waypoints.length > 0 we need to make last waypoint the end of the journey
	 * And destination is pushed as first waypoint
	 */
	public updateDirections() {

		this.gmapsApi.getNativeMap().then((map) => {
			if (!!this.directionsDisplay) {
				this.directionsDisplay.setMap(null);
			}
			if (!this.originPlaceId || !this.destinationPlaceId) {
				return;
			}
			// this.gmapsApi.setMapOptions(null);
			let me = this;
			let directionsService = new google.maps.DirectionsService();
			this.directionsDisplay = new google.maps.DirectionsRenderer();
			this.directionsDisplay.setMap(map);
			this.directionsDisplay.setOptions({
				polylineOptions: {
					strokeWeight: 4,
					strokeOpacity: 0.7,
					strokeColor: '#00468c',
					visible: true
				},
				suppressMarkers: true
			});
			let _displayWaypoints = this.waypoints;
			let lastWaypoint = null;

			// start & end of journey
			let start = new google.maps.LatLng({lat: this.origin.place['geo'].lat, lng: this.origin.place['geo'].lng});
			let end = new google.maps.LatLng({lat: this.destination.place['geo'].lat, lng: this.destination.place['geo'].lng});

			let wp = [];
			if (this.waypoints.length > 0) {
				// add destination as waypoint
				_displayWaypoints.unshift(this.destination);
				// remove last waypoint
				lastWaypoint = _displayWaypoints.pop();
				// set the end journey
				end = new google.maps.LatLng({lat: lastWaypoint.place['geo'].lat, lng: lastWaypoint.place['geo'].lng});
				_displayWaypoints.forEach((waypoint) => {
					let latLngWaypoint = new google.maps.LatLng({
						lat: waypoint.place['geo'].lat,
						lng: waypoint.place['geo'].lng
					});
					this.createMarker({position: latLngWaypoint});
					wp.push({location: latLngWaypoint, stopover: true});
				});
			}

			this.createMarker({position: start});
			this.createMarker({position: end});

			let request = {
				origin: {placeId: this.originPlaceId},
				destination: {placeId: lastWaypoint !== null ? lastWaypoint['place_id'] : this.destinationPlaceId},
				avoidHighways: true,
				optimizeWaypoints: false,
				travelMode: google.maps.DirectionsTravelMode.DRIVING,
				waypoints: wp,
			};

			// this.directionsDisplay.setDirections({routes: []});
			directionsService.route(request, (response: any, status: any) => {
				if (status === 'OK') {

					me.directionsDisplay.setDirections(response);
					me.computeTotalDistance(response);
					me.gmapsApi.triggerMapEvent('resize');

				} else {
					console.log('Directions request failed due to ' + status);
				}
			});
		});
	}

	public computeTotalDistance(result) {
		let totalDist = 0;
		let totalTime = 0;

		result.routes[0].legs.forEach((waypoint) => {
			totalDist += waypoint.distance.text;
			totalTime += waypoint.duration.value;
		});
		totalDist = totalDist / 1000;

		this.estimatedTime = totalTime;
		this.estimatedDistance = totalDist;
		this.notifyEstimatedTime.emit(this.estimatedTime);
	}

	public updateMarkers() {
		this.gmapsApi.getNativeMap().then((map) => {
			if (this.originPlaceId && this.destinationPlaceId) {
				this.cleanMarkers().then(() => {
					setTimeout(() => {
						this.updateDirections();
					}, 100);
				});
			}
			if (this.originPlaceId && !this.destinationPlaceId) {
				 if (!!this.directionsDisplay) {
					 this.directionsDisplay.setMap(null);
				 }
				 this.cleanMarkers().then(() => {
					let latLngOrigin = new google.maps.LatLng({lat: this.origin.place['geo'].lat, lng: this.origin.place['geo'].lng});
					this.gmapsApi.triggerMapEvent('resize');
					this.gmapsApi.setMapOptions({
						center: latLngOrigin,
						zoom: 12,
					});
					this.markerOptions = {position: latLngOrigin};
					this.createMarker(this.markerOptions);
				 });
			}
		});
	}

	public createMarker(markerOptions) {
		this.gmapsApi.createMarker(markerOptions).then((marker) => {
			this.markers.push(marker);
		});
	}
}
