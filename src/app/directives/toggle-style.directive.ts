import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';
import { GLOBAL_CONSTANTS } from '../common/global.constants';
@Directive({ selector: '[toggleStyles]' })
export class ToggleStylesDirective {
	public isFat = Boolean(GLOBAL_CONSTANTS.IS_FAT);

	constructor(renderer: Renderer2, el: ElementRef) {
		el.nativeElement.classstyle.backgroundColor = 'yellow';

		renderer.addClass(el.nativeElement, 'custom-theme');

	}
}
