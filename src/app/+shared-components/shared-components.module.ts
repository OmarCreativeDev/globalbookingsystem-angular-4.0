import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';
// 3rd party
import { CollapseModule } from 'ng2-bootstrap/collapse';
import { DatepickerModule } from 'ng2-bootstrap/datepicker';
import { ModalModule } from 'ng2-bootstrap/modal';
import { PaginationModule } from 'ng2-bootstrap/pagination';
import { TypeaheadModule } from 'ng2-bootstrap/typeahead';
import { DropdownModule } from 'ng2-bootstrap/dropdown/dropdown.module';
// common components
import { CustomPanelComponent } from '../components/custom-panel/custom-panel.component';
import { CustomInputComponent } from '../components/custom-input/custom-input.component';
import { CustomInputDirective } from '../components/custom-input/custom-input.directive';
import { CustomDropDownComponent } from '../components/custom-dropdown/custom-dropdown.component';
import { CustomInputAutocompleteComponent } from '../components/custom-input-autocomplete/custom-input-autocomplete.component';
import { CustomInputGroupThreeComponent } from '../components/custom-input-group-three/custom-input-group-three.component';
import { CustomInputGroupFourComponent } from '../components/custom-input-group-four/custom-input-group-four.component';
import { CustomInputBtnComponent } from '../components/custom-input-btn/custom-input-btn.component';
import { CustomInputChildComponent } from '../components/custom-input-child/custom-input-child.component';
import { CustomValidationMessageComponent } from '../components/custom-validation-message/custom-validation-message.component';
import { JourneyDetailsComponent } from '../+a-to-b/journey-details/journey-details.component';
import { InputDataComponent } from '../components/input-data/input-data.component';
import { JourneyMilestoneComponent } from '../+a-to-b/summary-widget/milestone/milestone.component';
import { BottomWidgetComponent } from '../+a-to-b/summary-widget/bottom/bottom.component';
import { HeaderWidgetComponent } from '../+a-to-b/summary-widget/header/header-widget.component';
import { TimeFormatterPipe } from '../common/pipes/time-formatter/time-formatter.pipe';
import { DropOffComponent } from '../+a-to-b/booking-confirmation/drop-offs/drop-off.component';
import { LoginCredentialsComponent } from '../components/login-credentials/login-credentials.component';
import { VerifyComponent } from '../components/verify-component/verify.component';
import { ChangePasswordFormComponent } from '../components/login-credentials/change-password-form/change-password-form.component';
import { ToggleStylesDirective } from '../directives/toggle-style.directive';
import { DragulaModule , DragulaService } from 'ng2-dragula/ng2-dragula';

@NgModule({
	declarations: [
		CustomPanelComponent,
		CustomInputComponent,
		CustomInputDirective,
		CustomDropDownComponent,
		CustomInputAutocompleteComponent,
		CustomInputGroupThreeComponent,
		CustomInputGroupFourComponent,
		CustomInputBtnComponent,
		CustomInputChildComponent,
		CustomValidationMessageComponent,
		InputDataComponent,
		JourneyDetailsComponent,
		JourneyMilestoneComponent,
		BottomWidgetComponent,
		HeaderWidgetComponent,
		DropOffComponent,
		LoginCredentialsComponent,
		VerifyComponent,
		ChangePasswordFormComponent,
		TimeFormatterPipe,
		ToggleStylesDirective
	],
	imports: [
		// angular
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		RouterModule,
		TranslateModule,
		CollapseModule,
		DatepickerModule,
		PaginationModule,
		TypeaheadModule,
		DropdownModule,
		ModalModule,
		DragulaModule,

	],
	exports: [

		// angular
		FormsModule,
		ReactiveFormsModule,
		// ng-translate
		TranslateModule,
		CollapseModule,
		DatepickerModule,
		PaginationModule,
		TypeaheadModule,
		ModalModule,
		DropdownModule,
		RouterModule,
		DragulaModule,
		// add-lee common-components
		CustomPanelComponent,
		CustomInputComponent,
		CustomInputDirective,
		CustomDropDownComponent,
		CustomInputAutocompleteComponent,
		CustomInputGroupThreeComponent,
		CustomInputGroupFourComponent,
		CustomInputBtnComponent,
		CustomInputChildComponent,
		CustomValidationMessageComponent,
		InputDataComponent,
		JourneyDetailsComponent,
		JourneyMilestoneComponent,
		BottomWidgetComponent,
		HeaderWidgetComponent,
		DropOffComponent,
		VerifyComponent,
		LoginCredentialsComponent,
		ChangePasswordFormComponent,
		TimeFormatterPipe

	],
	providers: [  ]
})

export class SharedComponentsModule {

}
