import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { HttpModule } from '@angular/http';
// 3rd party
import { CollapseModule } from 'ng2-bootstrap/collapse';
import { DatepickerModule } from 'ng2-bootstrap/datepicker';
import { ModalModule } from 'ng2-bootstrap/modal';
import { PaginationModule } from 'ng2-bootstrap/pagination';
import { TypeaheadModule } from 'ng2-bootstrap/typeahead';
import { DropdownModule } from 'ng2-bootstrap/dropdown/dropdown.module';
// common components
import { ServiceCatalogue } from '../services/service-catalogue/service-catalogue.service';

@NgModule({
	declarations: [
		//
	],
	imports: [
		// angular
		CommonModule,
		HttpModule,
		FormsModule,
		RouterModule,
		ReactiveFormsModule,
		// ng2-bootstrap
		CollapseModule,
		DatepickerModule,
		PaginationModule,
		TypeaheadModule,
		ModalModule,
		DropdownModule,
		// ng-translate
		TranslateModule,
		// maps

	],
	exports: [
		// angular
		HttpModule,
		FormsModule,
		RouterModule,
		ReactiveFormsModule,
		// translate
		TranslateModule,
		// ng-bootstrap
		CollapseModule,
		DatepickerModule,
		PaginationModule,
		TypeaheadModule,
		ModalModule,
		DropdownModule,
	],
	providers: [
		ServiceCatalogue
	]
})

export class SharedModule {

	constructor() {
		//
	}

}
