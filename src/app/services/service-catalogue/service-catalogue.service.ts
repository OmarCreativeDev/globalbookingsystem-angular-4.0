import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Response } from '@angular/http';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/share';
import { GLOBAL_CONSTANTS } from '../../common/global.constants';
import { HttpService } from '../http/http.service';

@Injectable()
export class ServiceCatalogue {
	private URL: string = GLOBAL_CONSTANTS.GLOBAL_API + GLOBAL_CONSTANTS.CAR_CATALOGUE_API_URL;

	constructor(public http: HttpService) {}

	public getServiceCatalogue(): Observable<Response> {
		return this.http.get(this.URL + '?status=1&locale=en-GB')
			.map((res: Response) => res.json())
			.catch((error: any) => Observable.throw(error.json().error || 'Server error'))
			.share();
	}
}
