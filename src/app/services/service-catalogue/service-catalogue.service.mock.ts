export const SERVICE_CATALOGUE_MOCK =
	[
		{
			car: 'Standard Car',
			price: '£25.99',
			text: 'Passenger car up to 4 Passengers',
			imageCar: 'service-standard.png',
			imageCapacity: 'service-executive-capacity.png'
		},
		{
			car: 'Executive Car',
			price: '£35.99',
			text: 'Passenger car up to 4 Passengers',
			imageCar: 'service-executive.png',
			imageCapacity: 'service-executive-capacity.png'
		},
		{
			car: 'Large Car',
			price: '£40.80',
			text: 'Passenger car up to 6 Passengers',
			imageCar: 'service-large-car.png',
			imageCapacity: 'service-executive-capacity.png'
		}
	];
