
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Http, Response } from '@angular/http';

@Injectable()
export class CardsService {
public mockCards;
constructor(public http: Http) {
// something
	this.mockCards = [
		{
			card: 'VISA',
			number: '1234'
		},
		{
			card: 'MAST',
			number: '1200'
		},
		{
			card: 'VISA',
			number: '6512'
		}];
}

public getCards(): Observable<any> {
	return Observable.of(this.mockCards);
}
	// public getAddresses(data: any): Observable<Response> {
		// 	return this.http.get('/assets/mock-data/service-catalog.mock.json')
		// 		.map((res: Response) => res.json())
		// 		.catch((error: any) => Observable.throw(error.json().error || 'Server error'))
		// 		.share();
		// }
}
