/**
 * Created by arfs on 23/03/2017.
 */
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PriceRequestModel } from '../../models/price/price.request.model';
import { PriceResponseModel } from '../../models/price/price.response.model';
import { QuotePriceResponseModel } from '../../models/price/price-quote.response.model';

@Injectable()
export class PriceService {

	public standardQuote: QuotePriceResponseModel;
	public executiveQuote: QuotePriceResponseModel;
	public largeQuote: QuotePriceResponseModel;

	constructor() {
		// create mocks
		this.createMockResponses();
	}

	public getPriceAtoB(data: PriceRequestModel): Observable<PriceResponseModel> {
		let priceResponseModel = new PriceResponseModel();
		priceResponseModel.locations = data.locations;
		switch (true) {
			case data.services[0]['service'] === 'Standard':
				priceResponseModel.quotes = [this.standardQuote];
				break;
			case data.services[0]['service'] === 'Large':
				priceResponseModel.quotes = [this.largeQuote];
				break;
			case data.services[0]['service'] === 'Executive':
				priceResponseModel.quotes = [this.executiveQuote];
				break;
			default:
				break;
		}
		return Observable.of(priceResponseModel);

	}

	public createMockResponses() {
		this.standardQuote = new QuotePriceResponseModel();
		this.standardQuote.service = 'Standard';
		this.standardQuote.quoteId = 'b9249ea4-6359-4ae8-9b91-fe35999d343b';
		this.standardQuote.net = '72.50';
		this.standardQuote.tax = '14.50';
		this.standardQuote.total = '87.00';
		this.standardQuote.currency_code = 'GBP';
		this.standardQuote.eta = 15;
		this.standardQuote.payment_token = 'abc123def456';
		this.standardQuote.payment_url = 'http=//payments.addisonlee.com/payments/id=67890&token=abc123def456';
		this.standardQuote.prices = [{
			name: 'Journey Mileage',
			type: 'BASE',
			tax: 'UK20',
			amount: 30,
			rate: 2.5,
			unit: 'miles',
			charge: 75.00
		}];
		this.executiveQuote = new QuotePriceResponseModel();
		this.executiveQuote.service = 'Executive';
		this.executiveQuote.quoteId = 'b9249ea4-6359-4ae8-9b91-fe35999d343b';
		this.executiveQuote.net = '100.00';
		this.executiveQuote.tax = '20.00';
		this.executiveQuote.total = '120.00';
		this.executiveQuote.currency_code = 'GBP';
		this.executiveQuote.eta = 15;
		this.executiveQuote.payment_token = 'abc123def456';
		this.executiveQuote.payment_url = 'http=//payments.addisonlee.com/payments/id=67890&token=abc123def456';
		this.executiveQuote.prices = [{
			name: 'Journey Mileage',
			type: 'BASE',
			tax: 'UK20',
			amount: 30,
			rate: 3,
			unit: 'miles',
			charge: 90.00
		}];
		this.largeQuote = new QuotePriceResponseModel();
		this.largeQuote.service = 'Large';
		this.largeQuote.quoteId = 'b9249ea4-6359-4ae8-9b91-fe35999d343b';
		this.largeQuote.net = '95.50';
		this.largeQuote.tax = '14.50';
		this.largeQuote.total = '98.00';
		this.largeQuote.currency_code = 'GBP';
		this.largeQuote.eta = 15;
		this.largeQuote.payment_token = 'abc123def456';
		this.largeQuote.payment_url = 'http=//payments.addisonlee.com/payments/id=67890&token=abc123def456';
		this.largeQuote.prices = [{
			name: 'Journey Mileage',
			type: 'BASE',
			tax: 'UK20',
			amount: 30,
			rate: 2.5,
			unit: 'miles',
			charge: 85.00
		}];
	}
}
