
import { Injectable } from '@angular/core';
import { Http, XHRBackend, RequestOptions, Request, RequestOptionsArgs, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class HttpService extends Http {

	constructor (backend: XHRBackend, options: RequestOptions) {
		options.headers.set('user', `df605fa22f004cba922acdddfe1ea483`);
		options.headers.set('group', 'd5cf4b7475d648a6b888ea0f7193e1Ae');
		super(backend, options);
	}

	public request(url: string|Request, options?: RequestOptionsArgs): Observable<Response> {
		// let token = localStorage.getItem('group');
		let _options = options || {headers: new Headers()};
		if (typeof url === 'string') { // meaning we have to add the token to the options, not in url
			_options.headers.set('user', `df605fa22f004cba922acdddfe1ea483`);
			_options.headers.set('group', 'd5cf4b7475d648a6b888ea0f7193e1Ae');
		}
		console.log(_options.headers['user']);
		return super.request(url, _options).catch(this.catchAuthError(this));
	}

	private catchAuthError (self: HttpService) {
		// we have to pass HttpService's own instance here as `self`
		return (res: Response) => {
			console.log(res);
			if (res.status === 401 || res.status === 403) {
				// if not authenticated
				console.log(res);
			}
			return Observable.throw(res);
		};
	}
}
