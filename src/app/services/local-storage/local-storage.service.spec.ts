// Load the implementations that should be tested
import { LocalStorage } from './local-storage.service';

describe('Services - LocalStorage', () => {
	let localStorage: LocalStorage;

	beforeEach(() => {
		localStorage = new LocalStorage();
		localStorage.clear();
	});

	it('should have clear method defined', () => {
		expect(localStorage.clear).toBeDefined();
	});

	it('should have remove method defined', () => {
		expect(localStorage.remove).toBeDefined();
	});

	it('should have set method defined', () => {
		expect(localStorage.set).toBeDefined();
	});

	it('should have get method defined', () => {
		expect(localStorage.get).toBeDefined();
	});

	it('set method adds data to local storage', () => {
		localStorage.set('name', 'joe bloggs');
		expect(localStorage.get('name')).toEqual('joe bloggs');
	});

	it('clear method clears data from local storage', () => {
		localStorage.set('skill', 'creative');
		localStorage.clear();
		expect(localStorage.get('skill')).toBeNull();
	});

	it('remove method removes data from local storage', () => {
		localStorage.set('job', 'engineer');
		localStorage.remove('job');
		expect(localStorage.get('job')).toBeNull();
	});

	it('get method retrieves data from local storage if the key exists', () => {
		localStorage.set('hobby', 'gaming');
		expect(localStorage.get('hobby')).toEqual('gaming');
	});

});
