import { Injectable } from '@angular/core';

@Injectable()
export class LocalStorage {

	private model: Object;
	constructor() {
		this.model = {};
	}
	public clear() {
		this.model = {};
		localStorage.clear();
	}
	public remove (key) {
		delete this.model[key];
		localStorage.removeItem(key);
	}
	public set(key, value) {
		this.model[key] = value;
		localStorage.setItem(key, JSON.stringify(value));
	}
	public get(key) {
		if (this.model[key] === undefined) {
			let rawData = localStorage.getItem(key);
			if (rawData === '[object Object]') {
				throw new Error('Corrupted localStorage data for key: ' + key);
			}
			this.model[key] = JSON.parse(rawData) || null;
		}
		return this.model[key];
	}
}
