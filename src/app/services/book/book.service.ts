
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Http, Response } from '@angular/http';
import { SERVICE_CATALOGUE_MOCK } from './service-catalogue.service.mock';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/share';
import { forEach } from '@angular/router/src/utils/collection';
import { BookResponseModel } from '../../models/book/book.response.model';

@Injectable()
export class ServiceBooking {

	constructor(public http: Http) {
		// something
	}

	public getBookingConfirmation(data: any): Observable<BookResponseModel> {
		let res = new BookResponseModel(data);
		console.log(res);
		return Observable.of(res);
	}

	// public getBookingConfirmation(data: any): Observable<Response> {
	// 	return this.http.get('/assets/mock-data/service-catalog.mock.json')
	// 		.map((res: Response) => res.json())
	// 		.catch((error: any) => Observable.throw(error.json().error || 'Server error'))
	// 		.share();
	// }
}
