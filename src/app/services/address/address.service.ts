
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Http, Response } from '@angular/http';
import { SERVICE_CATALOGUE_MOCK } from './service-catalogue.service.mock';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/share';
import { AddressModel } from '../../models/address/address.model';
import { MOCK_CONSTANTS } from '../../../../config/mock-helpers/mock.constants';
import { LocationAddressModel } from '../../models/address/address-location.model';

@Injectable()
export class AddressService {

constructor(public http: Http) {
// something
}

public getAddresses(): Observable<AddressModel> {
	let mockAddress = new AddressModel();
	mockAddress.homeAddress = new LocationAddressModel(MOCK_CONSTANTS.PLACE);
	mockAddress.workAddress = new LocationAddressModel(MOCK_CONSTANTS.PLACE2);
	mockAddress.favourites = [];
	MOCK_CONSTANTS.BOOKING.waypoints.forEach((wp) => {
		let fav = new LocationAddressModel(wp);
		mockAddress.favourites.push(fav);
	});
	return Observable.of(mockAddress);
}
	// public getAddresses(data: any): Observable<Response> {
		// 	return this.http.get('/assets/mock-data/service-catalog.mock.json')
		// 		.map((res: Response) => res.json())
		// 		.catch((error: any) => Observable.throw(error.json().error || 'Server error'))
		// 		.share();
		// }
}
