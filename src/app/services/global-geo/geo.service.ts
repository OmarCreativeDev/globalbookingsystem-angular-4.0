
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Response } from '@angular/http';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/share';
import { GLOBAL_CONSTANTS } from '../../common/global.constants';
import { HttpService } from '../http/http.service';

@Injectable()
export class GeoService {

	private URL: string = GLOBAL_CONSTANTS.GLOBAL_API + GLOBAL_CONSTANTS.GEO_API_URL;

	constructor(public http: HttpService) {
		// something
	}
	public search(data: any): Observable<Response> {
		return this.http.get( this.URL + 'location/search?address=' + data) // + '&lng=-0.13981&lat=51.52742&radius=5000' )
			.map((res: Response) => res.json())
			.catch((error: any) => Observable.throw(error.json().error || 'Server error'))
			.share();
	}
}
