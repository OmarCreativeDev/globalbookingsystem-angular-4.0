import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { MOCK_CONSTANTS } from '../../../../config/mock-helpers/mock.constants';

@Injectable()
export class PersonalInfoService {

	public model: Object;
	constructor() {
		this.model = {};
	}

	public get() {
		console.log('PersonalInfoService.get() fired');
		// TODO - temp mock data till backend is ready
		this.model = MOCK_CONSTANTS.PERSONAL_INFO;
		return Observable.of(this.model);
	}

	public update(data) {
		console.log('PersonalInfoService.update() fired', data);
		this.model = data;
		return Observable.of(this.model);
	}

}
