'use strict';

module.exports = {
	app : {
		src: 'src/app/',
		destination: 'src/assets/',
		styleIsFat:  process.env.STYLE_IS_FAT || false
	}
};
