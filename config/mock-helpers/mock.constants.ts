export const MOCK_CONSTANTS = {
	PLACE1: {
		address_components: [
			{
				long_name: 'Heathorw Airprot',
				short_name: 'HA',
				types: ['administrative_area_level_1', 'political']
			}, {
				long_name: 'United Kingdom',
				short_name: 'UK',
				types: ['country', 'political']
			}
		],
		adr_address: '',
		formatted_address: 'United Kingdom, UK',
		geometry: {
			location: {
				lat: () => { return 1112.334; },
				lng: () => { return -12.334; },
			},
			viewport: {
				south: -35.1843199,
				west: 112.92214179999996,
				north: -13.6897104,
				east: 129.00140970000007
			}
		},
		icon: '',
		id: '123',
		name: 'United Kingdom',
		place_id: 'ChIJ0YTziS4qOSoRmaMAMt9KDm4',
		reference: '',
		scope: 'GOOGLE',
		types: ['administrative_area_level_1', 'political'],
		url: '',
		utc_offset: 480,
		html_attributions: []
	},
	PLACE: {
		id: '239eeea3c3d759cec107b5ed9d8dca9c70740387',
		code: '',
		owner: {
			group: 'fishbowl',
			name: 'developer'
		},
		place_id: 'ChIJ1Qb35NA3dkgR0BrZSvPnBi4',
		formatted_address: 'Airport Way, Luton LU2 9LY, UK',
		place: {
			address_components: [
				{
					long_name: 'Airport Way',
					short_name: 'Airport Way',
					types: [
						'route'
					]
				},
				{
					long_name: 'Luton',
					short_name: 'Luton',
					types: [
						'locality',
						'political'
					]
				},
				{
					long_name: 'Luton',
					short_name: 'Luton',
					types: [
						'postal_town'
					]
				},
				{
					long_name: 'Luton',
					short_name: 'Luton',
					types: [
						'administrative_area_level_2',
						'political'
					]
				},
				{
					long_name: 'England',
					short_name: 'England',
					types: [
						'administrative_area_level_1',
						'political'
					]
				},
				{
					long_name: 'United Kingdom',
					short_name: 'GB',
					types: [
						'country',
						'political'
					]
				},
				{
					long_name: 'LU2 9LY',
					short_name: 'LU2 9LY',
					types: [
						'postal_code'
					]
				}
			],
			adr_address: '<span class="street-address">Airport Way</span>, <span class="locality">Luton</span> <span class="postal-code">LU2 9LY</span>, <span class="country-name">UK</span>',
			formatted_address: 'Airport Way, Luton LU2 9LY, UK',
			formatted_phone_number: '01582 405100',
			geometry: {
				location: {
					lat: 51.87626460000001,
					lng: -0.3717470999999932
				},
				viewport: {
					south: 51.8747214,
					west: -0.3827086500000405,
					north: 51.8808942,
					east: -0.36809325000001536
				}
			},
			icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/airport-71.png',
			id: '239eeea3c3d759cec107b5ed9d8dca9c70740387',
			international_phone_number: '+44 1582 405100',
			name: 'London Luton Airport',
			photos: [
				{
					height: 3024,
					html_attributions: [
						'<a href="https://maps.google.com/maps/contrib/102524656751104378129/photos">Stefan Keyte</a>'
					],
					width: 4032
				},
				{
					height: 720,
					html_attributions: [
						'<a href="https://maps.google.com/maps/contrib/107698311982185311845/photos">Adam Nogly</a>'
					],
					width: 1280
				},
				{
					height: 1836,
					html_attributions: [
						'<a href="https://maps.google.com/maps/contrib/109005656841629657439/photos">Suman Saku</a>'
					],
					width: 3264
				},
				{
					height: 1936,
					html_attributions: [
						'<a href="https://maps.google.com/maps/contrib/116421373267115457012/photos">Łukasz Langer</a>'
					],
					width: 2592
				},
				{
					height: 2160,
					html_attributions: [
						'<a href="https://maps.google.com/maps/contrib/113154668748794474274/photos">Georgi Georgiev</a>'
					],
					width: 3840
				},
				{
					height: 2828,
					html_attributions: [
						'<a href="https://maps.google.com/maps/contrib/117006259515368791119/photos">360 Degree Tours</a>'
					],
					width: 5656
				},
				{
					height: 1440,
					html_attributions: [
						'<a href="https://maps.google.com/maps/contrib/110867677264652795062/photos">Leon Halpern</a>'
					],
					width: 2560
				},
				{
					height: 2304,
					html_attributions: [
						'<a href="https://maps.google.com/maps/contrib/101290781179116358064/photos">Mihai Andrei</a>'
					],
					width: 3456
				},
				{
					height: 3024,
					html_attributions: [
						'<a href="https://maps.google.com/maps/contrib/107122921620709397982/photos">Yoav Yaari</a>'
					],
					width: 4032
				},
				{
					height: 3024,
					html_attributions: [
						'<a href="https://maps.google.com/maps/contrib/111002916855246537797/photos">Dryden Davies</a>'
					],
					width: 4032
				}
			],
			place_id: 'ChIJ1Qb35NA3dkgR0BrZSvPnBi4',
			rating: 2.8,
			reference: 'CmRRAAAAP4nsfddbiy1jc6Hm3pwJw_LJwx6L7GWGJTDzmy3tE_orKiBIH6CdQAHIYR6-bULQajwwPZrSKshVT0dy1hE2k4I7fIgeaJRLPPIoXiqEIBldTgB0sCuQFZHmpn49z8U1EhCTBnaAm2qgpWfyJ5bqZFK9GhTVX7y-4OipFUE3rczfkd7PjdKa9Q',
			reviews: [
				{
					aspects: [
						{
							rating: 0,
							type: 'overall'
						}
					],
					author_name: 'Conor Clancy',
					author_url: 'https://www.google.com/maps/contrib/113347440373811471273/reviews',
					language: 'en',
					profile_photo_url: 'https://lh5.googleusercontent.com/-B7FAfxjDE_A/AAAAAAAAAAI/AAAAAAAAABM/G359LJNGtcM/s128-c0x00000000-cc-rp-mo/photo.jpg',
					rating: 1,
					relative_time_description: '3 weeks ago',
					text: '',
					time: 1492157580
				},
				{
					aspects: [
						{
							rating: 0,
							type: 'overall'
						}
					],
					author_name: 'Steve Jaybury',
					author_url: 'https://www.google.com/maps/contrib/108213535188025931482/reviews',
					language: 'en',
					profile_photo_url: 'https://lh4.googleusercontent.com/-y4A3KkvIgv0/AAAAAAAAAAI/AAAAAAAAAAA/AHalGhpd59XArc4n-ELUNV54dFCcDOjC3g/s128-c0x00000000-cc-rp-mo/photo.jpg',
					rating: 1,
					relative_time_description: 'in the last week',
					text: '',
					time: 1494311331
				},
				{
					aspects: [
						{
							rating: 0,
							type: 'overall'
						}
					],
					author_name: 'Elizabeth Machado',
					author_url: 'https://www.google.com/maps/contrib/110494753646891723876/reviews',
					language: 'en',
					profile_photo_url: 'https://lh6.googleusercontent.com/-hVnviqnNxd8/AAAAAAAAAAI/AAAAAAAAABM/X57lDT2T-os/s128-c0x00000000-cc-rp-mo/photo.jpg',
					rating: 1,
					relative_time_description: 'in the last week',
					text: '',
					time: 1494182851
				},
				{
					aspects: [
						{
							rating: 0,
							type: 'overall'
						}
					],
					author_name: 'Leah Park',
					author_url: 'https://www.google.com/maps/contrib/111394651533468530315/reviews',
					language: 'en',
					profile_photo_url: 'https://lh4.googleusercontent.com/-3MAh94iqnlA/AAAAAAAAAAI/AAAAAAAAAGw/ISB2Nc2RkDc/s128-c0x00000000-cc-rp-mo/photo.jpg',
					rating: 1,
					relative_time_description: 'a month ago',
					text: '',
					time: 1491572341
				},
				{
					aspects: [
						{
							rating: 0,
							type: 'overall'
						}
					],
					author_name: 'Dhaya Online',
					author_url: 'https://www.google.com/maps/contrib/110699388809872596087/reviews',
					language: 'en',
					profile_photo_url: 'https://lh4.googleusercontent.com/-CF5ye-3asPA/AAAAAAAAAAI/AAAAAAAAAO0/QDWwI253IbM/s128-c0x00000000-cc-rp-mo-ba1/photo.jpg',
					rating: 2,
					relative_time_description: 'a week ago',
					text: '',
					time: 1493327812
				}
			],
			scope: 'GOOGLE',
			types: [
				'airport',
				'point_of_interest',
				'establishment'
			],
			url: 'https://maps.google.com/?cid=3316593207723760336',
			utc_offset: 60,
			vicinity: 'Airport Way, Luton',
			website: 'http://www.london-luton.co.uk/',
			html_attributions: []
		},
		geo: {
			lat: 51.87626460000001,
			lng: -0.3717470999999932
		}
	},

	PLACE2: {
		address_components: [
			{
				long_name: 'NW10 7NZ',
				short_name: 'NW10 7NZ',
				types: [
					'postal_code'
				]
			},
			{
				long_name: 'Premier Park Road',
				short_name: 'Premier Park Rd',
				types: [
					'route'
				]
			},
			{
				long_name: 'London',
				short_name: 'London',
				types: [
					'locality',
					'political'
				]
			},
			{
				long_name: 'London',
				short_name: 'London',
				types: [
					'postal_town'
				]
			},
			{
				long_name: 'Greater London',
				short_name: 'Greater London',
				types: [
					'administrative_area_level_2',
					'political'
				]
			},
			{
				long_name: 'England',
				short_name: 'England',
				types: [
					'administrative_area_level_1',
					'political'
				]
			},
			{
				long_name: 'United Kingdom',
				short_name: 'GB',
				types: [
					'country',
					'political'
				]
			}
		],
		adr_address: '<span class="street-address">Premier Park Rd</span>, <span class="locality">London</span> <span class="postal-code">NW10 7NZ</span>, <span class="country-name">UK</span>',
		formatted_address: 'Premier Park Rd, London NW10 7NZ, UK',
		geometry: {
			location: {
				lat: 51.5366472,
				lng: -0.2716547999999648
			},
			viewport: {
				south: 51.5350493,
				west: -0.27425189999996746,
				north: 51.5393862,
				east: -0.2680708999999979
			}
		},
		icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/geocode-71.png',
		id: '418ff36079b2c7fe59f78772fe3f50ba7d4f607d',
		name: 'NW10 7NZ',
		place_id: 'ChIJ5ziwOo0RdkgR09ydenFoYoo',
		reference: 'CmRbAAAAIdLX1bDqnvWrCu0NkkuX6F2rnEyV50aUQQBXpKP29aKcO83HjkKjehU5KrbrdRmx4-8Gt3UQNc7Uw9LQcwdQaTrBStaFSJCnb1InMGNRXKGlG1s_rqa-6qYyLOcjHLqPEhARd_PfotWlhStcVaPiqvzzGhREq4E2-pdD1QqGLMNYiDW6hiOFCQ',
		scope: 'GOOGLE',
		types: [
			'postal_code'
		],
		url: 'https://maps.google.com/?q=NW10+7NZ&ftid=0x4876118d3ab038e7:0x8a6268717a9ddcd3',
		utc_offset: 60,
		vicinity: 'London',
		html_attributions: []
	},

	QUOTE: {
		service: 'Executive',
		quoteId: 'b9249ea4-6359-4ae8-9b91-fe35999d343b',
		net: '100.00',
		tax: '20.00',
		total: '120.00',
		currency_code: 'GBP',
		eta: 15,
		payment_token: 'abc123def456gh',
		payment_url: '',
		prices: [{
			name: 'Journey Mileage',
			type: 'BASE',
			tax: 'UK20',
			amount: 30,
			rate: 3,
			unit: 'miles',
			charge: 90
		}]
	},

	CATALOGUE_SERVICE: {
		id: 101,
		code: 'EXC',
		name: 'Executive',
		description: 'Passenger car up to 4 Passengers',
		status: 1,
		locales: [{
			locale: 'en-US',
			name: 'Sedan',
			description: 'Passenger car up to 4 Passengers'
		}, {
			locale: 'en-GB',
			name: 'Standard',
			description: 'Passenger car up to 4 Passengers'
		}, {
			locale: 'fr-fr',
			name: 'Standard',
			description: 'Passenger car up to 4 Passengers'
		}],
		vehicles: [{
			id: 2,
			supplier: 100,
			code: 'EC400',
			name: 'Mercedes E-Class',
			description: 'Mercedes E-400',
			make: 'Mercedes',
			model: 'E-400',
			passengers: 4,
			items: 2,
			image: 'service-executive.png',
			service_id: 100,
			status: 1,
		}]
	},

	PAYMENT: {
		CASH: 'Cash',
		CARD: 'Card'
	},

	JOURNEY: {
		ASAP: 'Asap',
		LATER: 'Later'
	},

	ESTIMATED_TIME: {
		journeyTime: '30mins',
		journeyType: 'ASAP'
	},
	BOOKING : {
		waypoints: [
			{
				id: '239eeea3c3d759cec107b5ed9d8dca9c70740387',
				code: '',
				owner: {
					group: 'fishbowl',
					name: 'developer'
				},
				place_id: 'ChIJ1Qb35NA3dkgR0BrZSvPnBi4',
				formatted_address: 'Airport Way, Luton LU2 9LY, UK',
				place: {
					address_components: [
						{
							long_name: 'Airport Way',
							short_name: 'Airport Way',
							types: [
								'route'
							]
						},
						{
							long_name: 'Luton',
							short_name: 'Luton',
							types: [
								'locality',
								'political'
							]
						},
						{
							long_name: 'Luton',
							short_name: 'Luton',
							types: [
								'postal_town'
							]
						},
						{
							long_name: 'Luton',
							short_name: 'Luton',
							types: [
								'administrative_area_level_2',
								'political'
							]
						},
						{
							long_name: 'England',
							short_name: 'England',
							types: [
								'administrative_area_level_1',
								'political'
							]
						},
						{
							long_name: 'United Kingdom',
							short_name: 'GB',
							types: [
								'country',
								'political'
							]
						},
						{
							long_name: 'LU2 9LY',
							short_name: 'LU2 9LY',
							types: [
								'postal_code'
							]
						}
					],
					adr_address: '<span class="street-address">Airport Way</span>, <span class="locality">Luton</span> <span class="postal-code">LU2 9LY</span>, <span class="country-name">UK</span>',
					formatted_address: 'Airport Way, Luton LU2 9LY, UK',
					formatted_phone_number: '01582 405100',
					geometry: {
						location: {
							lat: 51.87626460000001,
							lng: -0.3717470999999932
						},
						viewport: {
							south: 51.8747214,
							west: -0.3827086500000405,
							north: 51.8808942,
							east: -0.36809325000001536
						}
					},
					icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/airport-71.png',
					id: '239eeea3c3d759cec107b5ed9d8dca9c70740387',
					international_phone_number: '+44 1582 405100',
					name: 'London Luton Airport',
					photos: [
						{
							height: 3024,
							html_attributions: [
								'<a href="https://maps.google.com/maps/contrib/102524656751104378129/photos">Stefan Keyte</a>'
							],
							width: 4032
						},
						{
							height: 720,
							html_attributions: [
								'<a href="https://maps.google.com/maps/contrib/107698311982185311845/photos">Adam Nogly</a>'
							],
							width: 1280
						},
						{
							height: 1836,
							html_attributions: [
								'<a href="https://maps.google.com/maps/contrib/109005656841629657439/photos">Suman Saku</a>'
							],
							width: 3264
						},
						{
							height: 1936,
							html_attributions: [
								'<a href="https://maps.google.com/maps/contrib/116421373267115457012/photos">Łukasz Langer</a>'
							],
							width: 2592
						},
						{
							height: 2160,
							html_attributions: [
								'<a href="https://maps.google.com/maps/contrib/113154668748794474274/photos">Georgi Georgiev</a>'
							],
							width: 3840
						},
						{
							height: 2828,
							html_attributions: [
								'<a href="https://maps.google.com/maps/contrib/117006259515368791119/photos">360 Degree Tours</a>'
							],
							width: 5656
						},
						{
							height: 1440,
							html_attributions: [
								'<a href="https://maps.google.com/maps/contrib/110867677264652795062/photos">Leon Halpern</a>'
							],
							width: 2560
						},
						{
							height: 2304,
							html_attributions: [
								'<a href="https://maps.google.com/maps/contrib/101290781179116358064/photos">Mihai Andrei</a>'
							],
							width: 3456
						},
						{
							height: 3024,
							html_attributions: [
								'<a href="https://maps.google.com/maps/contrib/107122921620709397982/photos">Yoav Yaari</a>'
							],
							width: 4032
						},
						{
							height: 3024,
							html_attributions: [
								'<a href="https://maps.google.com/maps/contrib/111002916855246537797/photos">Dryden Davies</a>'
							],
							width: 4032
						}
					],
					place_id: 'ChIJ1Qb35NA3dkgR0BrZSvPnBi4',
					rating: 2.8,
					reference: 'CmRRAAAAP4nsfddbiy1jc6Hm3pwJw_LJwx6L7GWGJTDzmy3tE_orKiBIH6CdQAHIYR6-bULQajwwPZrSKshVT0dy1hE2k4I7fIgeaJRLPPIoXiqEIBldTgB0sCuQFZHmpn49z8U1EhCTBnaAm2qgpWfyJ5bqZFK9GhTVX7y-4OipFUE3rczfkd7PjdKa9Q',
					reviews: [
						{
							aspects: [
								{
									rating: 0,
									type: 'overall'
								}
							],
							author_name: 'Conor Clancy',
							author_url: 'https://www.google.com/maps/contrib/113347440373811471273/reviews',
							language: 'en',
							profile_photo_url: 'https://lh5.googleusercontent.com/-B7FAfxjDE_A/AAAAAAAAAAI/AAAAAAAAABM/G359LJNGtcM/s128-c0x00000000-cc-rp-mo/photo.jpg',
							rating: 1,
							relative_time_description: '3 weeks ago',
							time: 1492157580
						},
						{
							aspects: [
								{
									rating: 0,
									type: 'overall'
								}
							],
							author_name: 'Steve Jaybury',
							author_url: 'https://www.google.com/maps/contrib/108213535188025931482/reviews',
							language: 'en',
							profile_photo_url: 'https://lh4.googleusercontent.com/-y4A3KkvIgv0/AAAAAAAAAAI/AAAAAAAAAAA/AHalGhpd59XArc4n-ELUNV54dFCcDOjC3g/s128-c0x00000000-cc-rp-mo/photo.jpg',
							rating: 1,
							relative_time_description: 'in the last week',
							time: 1494311331
						},
						{
							aspects: [
								{
									rating: 0,
									type: 'overall'
								}
							],
							author_name: 'Elizabeth Machado',
							author_url: 'https://www.google.com/maps/contrib/110494753646891723876/reviews',
							language: 'en',
							profile_photo_url: 'https://lh6.googleusercontent.com/-hVnviqnNxd8/AAAAAAAAAAI/AAAAAAAAABM/X57lDT2T-os/s128-c0x00000000-cc-rp-mo/photo.jpg',
							rating: 1,
							relative_time_description: 'in the last week',
							time: 1494182851
						},
						{
							aspects: [
								{
									rating: 0,
									type: 'overall'
								}
							],
							author_name: 'Leah Park',
							author_url: 'https://www.google.com/maps/contrib/111394651533468530315/reviews',
							language: 'en',
							profile_photo_url: 'https://lh4.googleusercontent.com/-3MAh94iqnlA/AAAAAAAAAAI/AAAAAAAAAGw/ISB2Nc2RkDc/s128-c0x00000000-cc-rp-mo/photo.jpg',
							rating: 1,
							relative_time_description: 'a month ago',
							time: 1491572341
						},
						{
							aspects: [
								{
									rating: 0,
									type: 'overall'
								}
							],
							author_name: 'Dhaya Online',
							author_url: 'https://www.google.com/maps/contrib/110699388809872596087/reviews',
							language: 'en',
							profile_photo_url: 'https://lh4.googleusercontent.com/-CF5ye-3asPA/AAAAAAAAAAI/AAAAAAAAAO0/QDWwI253IbM/s128-c0x00000000-cc-rp-mo-ba1/photo.jpg',
							rating: 2,
							relative_time_description: 'a week ago',
							time: 1493327812
						}
					],
					scope: 'GOOGLE',
					types: [
						'airport',
						'point_of_interest',
						'establishment'
					],
					url: 'https://maps.google.com/?cid=3316593207723760336',
					utc_offset: 60,
					vicinity: 'Airport Way, Luton',
					website: 'http://www.london-luton.co.uk/',
					html_attributions: []
				},
				geo: {
					lat: 51.87626460000001,
					lng: -0.3717470999999932
				}
			},
			{
				id: '418ff36079b2c7fe59f78772fe3f50ba7d4f607d',
				code: '',
				owner: {
					group: 'fishbowl',
					name: 'developer'
				},
				place_id: 'ChIJ5ziwOo0RdkgR09ydenFoYoo',
				formatted_address: 'Premier Park Rd, London NW10 7NZ, UK',
				place: {
					address_components: [
						{
							long_name: 'NW10 7NZ',
							short_name: 'NW10 7NZ',
							types: [
								'postal_code'
							]
						},
						{
							long_name: 'Premier Park Road',
							short_name: 'Premier Park Rd',
							types: [
								'route'
							]
						},
						{
							long_name: 'London',
							short_name: 'London',
							types: [
								'locality',
								'political'
							]
						},
						{
							long_name: 'London',
							short_name: 'London',
							types: [
								'postal_town'
							]
						},
						{
							long_name: 'Greater London',
							short_name: 'Greater London',
							types: [
								'administrative_area_level_2',
								'political'
							]
						},
						{
							long_name: 'England',
							short_name: 'England',
							types: [
								'administrative_area_level_1',
								'political'
							]
						},
						{
							long_name: 'United Kingdom',
							short_name: 'GB',
							types: [
								'country',
								'political'
							]
						}
					],
					adr_address: '<span class="street-address">Premier Park Rd</span>, <span class="locality">London</span> <span class="postal-code">NW10 7NZ</span>, <span class="country-name">UK</span>',
					formatted_address: 'Premier Park Rd, London NW10 7NZ, UK',
					geometry: {
						location: {
							lat: 51.5366472,
							lng: -0.2716547999999648
						},
						viewport: {
							south: 51.5350493,
							west: -0.27425189999996746,
							north: 51.5393862,
							east: -0.2680708999999979
						}
					},
					icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/geocode-71.png',
					id: '418ff36079b2c7fe59f78772fe3f50ba7d4f607d',
					name: 'NW10 7NZ',
					place_id: 'ChIJ5ziwOo0RdkgR09ydenFoYoo',
					reference: 'CmRbAAAAIdLX1bDqnvWrCu0NkkuX6F2rnEyV50aUQQBXpKP29aKcO83HjkKjehU5KrbrdRmx4-8Gt3UQNc7Uw9LQcwdQaTrBStaFSJCnb1InMGNRXKGlG1s_rqa-6qYyLOcjHLqPEhARd_PfotWlhStcVaPiqvzzGhREq4E2-pdD1QqGLMNYiDW6hiOFCQ',
					scope: 'GOOGLE',
					types: [
						'postal_code'
					],
					url: 'https://maps.google.com/?q=NW10+7NZ&ftid=0x4876118d3ab038e7:0x8a6268717a9ddcd3',
					utc_offset: 60,
					vicinity: 'London',
					html_attributions: []
				},
				geo: {
					lat: 51.5366472,
					lng: -0.2716547999999648
				}
			}
		],
		status: [
			'En Route',
			'Arrived',
			' status 3'
		],
		origin: {
			code: '',
			owner: {
				group: 'fishbowl',
				name: 'developer'
			},
			place_id: 'ChIJS6B4Ms1zdkgRcBghsBDPRXc',
			formatted_address: 'Heathrow Airport Terminal 3, Heathrow Airport (LHR), Longford, Hounslow TW6 1SX, UK',
			place: {
				address_components: [
					{
						long_name: 'Heathrow Airport Terminal 3',
						short_name: 'Heathrow Airport Terminal 3',
						types: [
							'bus_station',
							'establishment',
							'point_of_interest',
							'transit_station'
						]
					},
					{
						long_name: 'Longford',
						short_name: 'Longford',
						types: [
							'locality',
							'political'
						]
					},
					{
						long_name: 'Hounslow',
						short_name: 'Hounslow',
						types: [
							'postal_town'
						]
					},
					{
						long_name: 'Greater London',
						short_name: 'Greater London',
						types: [
							'administrative_area_level_2',
							'political'
						]
					},
					{
						long_name: 'England',
						short_name: 'England',
						types: [
							'administrative_area_level_1',
							'political'
						]
					},
					{
						long_name: 'United Kingdom',
						short_name: 'GB',
						types: [
							'country',
							'political'
						]
					},
					{
						long_name: 'TW6 1SX',
						short_name: 'TW6 1SX',
						types: [
							'postal_code'
						]
					}
				],
				formatted_address: 'Heathrow Airport Terminal 3, Heathrow Airport (LHR), Longford, Hounslow TW6 1SX, UK',
				geometry: {
					location: {
						lat: 51.47189299999999,
						lng: -0.4567289999999957
					},
					location_type: 'APPROXIMATE',
					viewport: {
						south: 51.4705440197085,
						west: -0.4580779802914776,
						north: 51.4732419802915,
						east: -0.45538001970851383
					}
				},
				place_id: 'ChIJS6B4Ms1zdkgRcBghsBDPRXc',
				types: [
					'bus_station',
					'establishment',
					'point_of_interest',
					'transit_station'
				]
			},
			geo: {}
		},
		destination: {
			code: '',
			owner: {
				group: 'fishbowl',
				name: 'developer'
			},
			place_id: 'ChIJ8_KpN4sadkgR7MI_0LgVCUs',
			formatted_address: 'Hampstead, London NW3 2QG, UK',
			place: {
				address_components: [
					{
						long_name: 'NW3 2QG',
						short_name: 'NW3 2QG',
						types: [
							'postal_code'
						]
					},
					{
						long_name: 'Hampstead',
						short_name: 'Hampstead',
						types: [
							'neighborhood',
							'political'
						]
					},
					{
						long_name: 'London',
						short_name: 'London',
						types: [
							'postal_town'
						]
					},
					{
						long_name: 'Greater London',
						short_name: 'Greater London',
						types: [
							'administrative_area_level_2',
							'political'
						]
					},
					{
						long_name: 'England',
						short_name: 'England',
						types: [
							'administrative_area_level_1',
							'political'
						]
					},
					{
						long_name: 'United Kingdom',
						short_name: 'GB',
						types: [
							'country',
							'political'
						]
					}
				],
				formatted_address: 'Hampstead, London NW3 2QG, UK',
				geometry: {
					bounds: {
						south: 51.5524932,
						west: -0.16662480000002233,
						north: 51.5536201,
						east: -0.16428180000002612
					},
					location: {
						lat: 51.5532242,
						lng: -0.16532359999996515
					},
					location_type: 'APPROXIMATE',
					viewport: {
						south: 51.5517076697085,
						west: -0.16680228029144928,
						north: 51.5544056302915,
						east: -0.1641043197084855
					}
				},
				place_id: 'ChIJ8_KpN4sadkgR7MI_0LgVCUs',
				types: [
					'postal_code'
				]
			},
			geo: {}
		},
		passenger: {
			namePassenger: 'sadadsa',
			emailPassenger: 'asdasdas@dsaas.asdds'
		},
		journey: {
			type: 'JOURNEY_DETAILS.DATE_OF_JOURNEY_OPTIONS.ASAP',
			time: '2017-05-09T14:25:11.695Z'
		},
		price: {
			locations: [
				{
					metadata: {
						airport: '',
						park: '',
						feature: '',
						poi: '',
						url: ''
					},
					code: '',
					owner: {
						group: 'fishbowl',
						name: 'developer'
					},
					place_id: 'ChIJS6B4Ms1zdkgRcBghsBDPRXc',
					formatted_address: 'Heathrow Airport Terminal 3, Heathrow Airport (LHR), Longford, Hounslow TW6 1SX, UK',
					place: {
						code: '',
						owner: {
							group: 'fishbowl',
							name: 'developer'
						},
						place_id: 'ChIJS6B4Ms1zdkgRcBghsBDPRXc',
						formatted_address: 'Heathrow Airport Terminal 3, Heathrow Airport (LHR), Longford, Hounslow TW6 1SX, UK',
						place: {
							address_components: [
								{
									long_name: 'Heathrow Airport Terminal 3',
									short_name: 'Heathrow Airport Terminal 3',
									types: [
										'bus_station',
										'establishment',
										'point_of_interest',
										'transit_station'
									]
								},
								{
									long_name: 'Longford',
									short_name: 'Longford',
									types: [
										'locality',
										'political'
									]
								},
								{
									long_name: 'Hounslow',
									short_name: 'Hounslow',
									types: [
										'postal_town'
									]
								},
								{
									long_name: 'Greater London',
									short_name: 'Greater London',
									types: [
										'administrative_area_level_2',
										'political'
									]
								},
								{
									long_name: 'England',
									short_name: 'England',
									types: [
										'administrative_area_level_1',
										'political'
									]
								},
								{
									long_name: 'United Kingdom',
									short_name: 'GB',
									types: [
										'country',
										'political'
									]
								},
								{
									long_name: 'TW6 1SX',
									short_name: 'TW6 1SX',
									types: [
										'postal_code'
									]
								}
							],
							formatted_address: 'Heathrow Airport Terminal 3, Heathrow Airport (LHR), Longford, Hounslow TW6 1SX, UK',
							geometry: {
								location: {
									lat: 51.47189299999999,
									lng: -0.4567289999999957
								},
								location_type: 'APPROXIMATE',
								viewport: {
									south: 51.4705440197085,
									west: -0.4580779802914776,
									north: 51.4732419802915,
									east: -0.45538001970851383
								}
							},
							place_id: 'ChIJS6B4Ms1zdkgRcBghsBDPRXc',
							types: [
								'bus_station',
								'establishment',
								'point_of_interest',
								'transit_station'
							]
						},
						geo: {}
					},
					geo: {}
				},
				{
					metadata: {
						airport: '',
						park: '',
						feature: '',
						poi: '',
						url: ''
					},
					code: '',
					owner: {
						group: 'fishbowl',
						name: 'developer'
					},
					place_id: 'ChIJ8_KpN4sadkgR7MI_0LgVCUs',
					formatted_address: 'Hampstead, London NW3 2QG, UK',
					place: {
						code: '',
						owner: {
							group: 'fishbowl',
							name: 'developer'
						},
						place_id: 'ChIJ8_KpN4sadkgR7MI_0LgVCUs',
						formatted_address: 'Hampstead, London NW3 2QG, UK',
						place: {
							address_components: [
								{
									long_name: 'NW3 2QG',
									short_name: 'NW3 2QG',
									types: [
										'postal_code'
									]
								},
								{
									long_name: 'Hampstead',
									short_name: 'Hampstead',
									types: [
										'neighborhood',
										'political'
									]
								},
								{
									long_name: 'London',
									short_name: 'London',
									types: [
										'postal_town'
									]
								},
								{
									long_name: 'Greater London',
									short_name: 'Greater London',
									types: [
										'administrative_area_level_2',
										'political'
									]
								},
								{
									long_name: 'England',
									short_name: 'England',
									types: [
										'administrative_area_level_1',
										'political'
									]
								},
								{
									long_name: 'United Kingdom',
									short_name: 'GB',
									types: [
										'country',
										'political'
									]
								}
							],
							formatted_address: 'Hampstead, London NW3 2QG, UK',
							geometry: {
								bounds: {
									south: 51.5524932,
									west: -0.16662480000002233,
									north: 51.5536201,
									east: -0.16428180000002612
								},
								location: {
									lat: 51.5532242,
									lng: -0.16532359999996515
								},
								location_type: 'APPROXIMATE',
								viewport: {
									south: 51.5517076697085,
									west: -0.16680228029144928,
									north: 51.5544056302915,
									east: -0.1641043197084855
								}
							},
							place_id: 'ChIJ8_KpN4sadkgR7MI_0LgVCUs',
							types: [
								'postal_code'
							]
						},
						geo: {}
					},
					geo: {}
				}
			],
			quotes: [
				{
					service: 'Standard',
					quoteId: 'b9249ea4-6359-4ae8-9b91-fe35999d343b',
					net: '72.50',
					tax: '14.50',
					total: '87.00',
					currency_code: 'GBP',
					eta: 15,
					payment_token: 'abc123def456',
					payment_url: 'http=//payments.addisonlee.com/payments/id=67890&token=abc123def456',
					prices: [
						{
							name: 'Journey Mileage',
							type: 'BASE',
							tax: 'UK20',
							amount: 30,
							rate: 2.5,
							unit: 'miles',
							charge: 75
						}
					]
				}
			]
		},
		service: {
			id: 100,
			code: 'STD',
			name: 'Standard',
			description: 'Passenger car up to 4 Passengers',
			created_dt: '2017-01-01T12:00:00Z',
			modified_dt: '2017-01-01T12:30:00Z',
			status: 1,
			locales: [
				{
					locale: 'en-US',
					name: 'Sedan',
					description: 'Passenger car up to 4 Passengers'
				},
				{
					locale: 'en-GB',
					name: 'Standard',
					description: 'Passenger car up to 4 Passengers'
				},
				{
					locale: 'fr-fr',
					name: 'Standard',
					description: 'Passenger car up to 4 Passengers'
				}
			],
			vehicles: [
				{
					id: 1,
					supplier: 100,
					code: 'FORDS',
					name: 'Ford Standard',
					description: 'Ford Standard STD',
					make: 'Ford',
					model: 'Standard',
					passengers: 4,
					items: 2,
					image: 'service-standard.png',
					service_id: 100,
					status: 1,
					created_dt: '2017-01-01T12:00:00Z',
					modified_dt: '2017-01-01T12:30:00Z'
				}
			]
		},
		payment: 'JOURNEY_DETAILS.PAYMENT_METHOD_OPTIONS.CASH',
		driverNote: 0,
		bookingReference: 'OQ7mCu7lM8hpB3'
	},

	PERSONAL_INFO: {
		firstName: 'Bob',
		lastName: 'Marley',
		mobileNo: '07999 555 888'
	}
};
