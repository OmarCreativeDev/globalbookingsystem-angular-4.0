#!groovy

def git_repo = 'github.com/addleeplc/global' // repo url
def aws_dev = '54.71.206.189' // dev machine
def aws_uat = '35.166.184.252' // uat machine
def aws_user = 'ubuntu' // aws user
def scp_dir = 'jenkins-production-builds' // where we place the prod.zip
def prod_dir = 'www/addison-lee/production/' // nginx /dist folder

def notifySuccess() {
  // send to Slack
  slackSend (color: '#009933', message: "NEW BUILD SUCCESS! : [${env.BUILD_NUMBER}]' ( new TAG : v1.0.${env.BUILD_ID} )")
}
def notifyFailed() {
  slackSend (color: '#FF0000', message: "OOOPSSS BUILD FAILED! : [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
}

node{
    currentBuild.result = "SUCCESS"

    print "INI : POLARIS DEV PIPELINE CONFIG ======================================================================== "
    print " \t\t\t \u27A1 GIT URL     : ${git_repo}"
    print " \t\t\t \u27A1 DEV MACHINE : ${aws_dev}"
    print " \t\t\t \u27A1 UAT MACHINE : ${aws_uat}"
    print " \t\t\t \u27A1 DEPLOY DIR  : /home/ubuntu/${scp_dir}"
    print " \t\t\t \u27A1 NGINX DIR   : /home/ubuntu/${prod_dir}dist"
    print "END : POLARIS DEV PIPELINE CONFIG ======================================================================== "

    try {
			stage ('\u2780 Checkout'){
				print "===================================================================================================== Checkout code ..."
				checkout scm
				print "\u2713 Checkout Success"
			}
			stage ('\u2781 Npm'){
				print "===================================================================================================== Environment check ..."
				sh 'node -v; npm -v;'
                print "\u2713 Environment Check Success"
				print "===================================================================================================== Installing npm deps ..."
				sh 'rm -rf node_modules'
				sh 'npm cache clean'
				sh 'npm install'
				sh 'typings install dt~google.maps --global'
				print "\u2713 Npm Install Success"
			}
			stage ('\u2782 Lint'){
				print "===================================================================================================== Linting code ..."
				sh 'npm run lint'
				print "\u2713 Lint Success"
			}
			stage ('\u2783 Brand Tests'){
				print "===================================================================================================== Test branding strategy ..."
				sh 'grunt dev'
                print "\u2713 Brand Success"
				sh 'grunt test-branding'
				print "\u2713 Brand Tests Success"
			}
			stage ('\u2784 Unit Tests'){
				print "===================================================================================================== Unit test app ..."
				ansiColor('xterm') {
				    sh '''
						npm run test || exit1
					'''
				}
				print "\u2713 Unit Tests Success"
				publishHTML(target: [
					allowMissing: false,
					alwaysLinkToLastBuild: true,
					keepAll: false,
					reportDir: 'coverage/html',
					reportFiles: 'index.html',
					reportName: 'HTML Coverage Report'
				])
				print "\u2713 Tests Reports Generated Success"
			}
			stage ('\u2785 Build'){
				print "===================================================================================================== Build production app ...  "
				sh 'npm run build:aot:prod'
				print "\u2713 AOT build Success"
			}
			stage ('\u2786 E2E Tests'){
				print "===================================================================================================== Unit test app ..."
				ansiColor('xterm') {
					sh '''
						Xvfb -ac :99 -screen 0 1280x1024x16 &
						export DISPLAY=:99
						npm run e2e
					'''
				}
				print "\u2713 E2E Tests Success"
			}
			stage ('\u2787 Tag'){
				print "===================================================================================================== Tag the code ..."
				withCredentials([usernamePassword(credentialsId: '5b86bc2b-32b4-4f24-a647-d430cc0e0892', passwordVariable: 'password', usernameVariable: 'username')]) {
					sh 'git tag -d $(git tag -l)'
					sh "git tag -a v1.0.${env.BUILD_ID} -m \"Jenkins created RC for UAT\""
					sh "git push https://${env.username}:${env.password}@${git_repo} --tags"
				}
				print "\u2713 Tag Code Success"
            }
			stage ('\u2788 DEV'){
				print "===================================================================================================== Deploying app in dev ..."
				sh 'zip -r prod.zip dist'
				print "\u2713 Zip Production Build Success"
				sh "ssh -i ~/.ssh/id_rsa ubuntu@54.71.206.189 \"cd jenkins-production-builds; rm -rf prod.zip\"; exit;"
				print "\u2713 Clean destination folder Success"
				sh 'scp -i ~/.ssh/id_rsa prod.zip ubuntu@54.71.206.189:/home/ubuntu/jenkins-production-builds; exit;'
				print "\u2713 Send Zip over ssh Success"
				sh "ssh -i ~/.ssh/id_rsa ubuntu@54.71.206.189 \"cd jenkins-production-builds; rm -rf dist; unzip prod.zip; cd ..; rm -rf www/addison-lee/production/dist; cp -R jenkins-production-builds/dist www/addison-lee/production;\" exit;"
				print "\u2713 Deploy Dev Success"
  			    //notifySuccess();
			}
			stage ('\u2789 UAT'){
				print "===================================================================================================== Deploying app in uat ..."
				sh "ssh -i ~/.ssh/id_rsa ubuntu@35.166.184.252 \"cd jenkins-production-builds; rm -rf prod.zip\"; exit;"
				print "\u2713 Clean destination folder Success"
				sh 'scp -i ~/.ssh/id_rsa prod.zip ubuntu@35.166.184.252:/home/ubuntu/jenkins-production-builds; exit;'
				print "\u2713 Send Zip over ssh Success"
				sh "ssh -i ~/.ssh/id_rsa ubuntu@35.166.184.252 \"cd jenkins-production-builds; rm -rf dist; unzip prod.zip; cd ..; rm -rf www/addison-lee/production/dist; cp -R jenkins-production-builds/dist www/addison-lee/production;\" exit;"
				print "\u2713 Deploy UAT Success"
				//notifySuccess();
			}
    }
    catch (err) {
        currentBuild.result = "FAILURE"
            print 'error'
            echo "\u2717 Error in Pipeline"
            //notifyFailed()
        throw err
    }
}
