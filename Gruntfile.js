'use strict';

var utils = require('./config/config-brand');
var chalk = require('chalk');

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

	if (process.env.NODE_ENV === undefined) {
		process.env.NODE_ENV = 'development';
	}

	// A JIT(Just In Time) plugin loader for Grunt.
	require('jit-grunt')(grunt);

	// default brand addisonlee
	var branding = process.env.brand !== undefined ? process.env.brand:'addison-lee';

	// Define the configuration for all the tasks
	grunt.initConfig({

		// empty and prep translations, images and icons folder
		clean: [
			utils.app.destination + 'i18n/',
			utils.app.destination + 'img/',
			'src/styles/brand-variables/',
			utils.app.destination + 'icon/'
		],

		// merge json translations in to correct folder
		'merge-json': {
			'en': {
				src: [ utils.app.src + '**/*.i18n.en.json' ],
				dest: utils.app.destination + 'i18n/en.json'
			}
		},

		copy: {
			images: {
				files: [
					{
						cwd:'config/branding/images/' +branding+ '/',
						expand: true,
						src: '**',
						dest: 'src/assets/img/',
						filter: 'isFile'
					}
				],
			},
			sass: {
				files: [
					{
						cwd:'config/branding/scss/' +branding+ '/',
						expand: true,
						src: '**',
						dest: 'src/styles/brand-variables/',
						filter: 'isFile'
					}
				],
			},
			icon: {
				files: [
					{
						cwd:'config/branding/icon/' +branding+ '/',
						expand: true,
						src: '**',
						dest: 'src/assets/icon/',
						filter: 'isFile'
					}
				],
			}
		},
        mochaTest: {
            test: {
                options: {
                    reporter: 'spec',
                    captureFile: 'results.txt', // Optionally capture the reporter output to a file
                    quiet: false, // Optionally suppress output to standard out (defaults to false)
                    clearRequireCache: false, // Optionally clear the require cache before running tests (defaults to false)
                    noFail: false // Optionally set to not fail on failed tests (will still fail on other errors)
                },
                src: ['tests-branding/**/*.js']
            }
        },
		cloc: {
			all: {
				options: ["--quiet --exclude-lang=Markdown,XML,YAML,CSS,JavaScript,JSON", "--exclude-dir=node_modules,typings,dist,compiled,dll,coverage"],
				src: ["."]
			},
		},
		sloccount: {
			options: {
				reportPath: 'coverage/sloc/sloc.sc'
			},
			src: [  'src/app/*.ts',
					'src/app/**/*.ts',
					'src/app/**/**/*.ts',
					'src/app/*.scss',
					'src/app/**/*.scss',
					'src/app/**/**/*.scss',
					'src/app/*.html',
					'src/app/**/*.html',
					'src/app/**/**/*.html',
				]
		}

	});

	grunt.registerTask('dev', [
		'clean',
		'merge-json',
		'copy'
	]);

	grunt.registerTask('test-branding', ['mochaTest','finished']);

	grunt.registerTask('finished',function(){
		console.log(chalk.magenta('-------------------------------------------------------------------------------------------'));
		console.log(chalk.magenta('ADDISON LEE - GRUNT BRANDING FINISHED'));
		console.log(chalk.magenta('-------------------------------------------------------------------------------------------'));
		console.log(chalk.red('Environment  :\t\t\t' + process.env.NODE_ENV));
		console.log(chalk.yellow('App Branded  :\t\t\t' + branding));
		console.log(chalk.blue('Json Merged  :\t\t\t' + utils.app.destination + 'i18n/en.json'));
		console.log(chalk.magenta('-------------------------------------------------------------------------------------------'));
	});

	grunt.registerTask('default', ['dev','test-branding','cloc:all','sloccount']);

};
